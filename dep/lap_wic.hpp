﻿///---------------------------------------------------------------------------------------------------------------------
///															CONFIG
///---------------------------------------------------------------------------------------------------------------------
//define the next line to suppress OpenGL tokens and core function extension loading and management (do this if you want to use another extension loading library)
//#define LAP_WIC_SUPPRESS_GL_EXTENSIONS
//define the next line to suppress definition of Vulkan function
//#define LAP_WIC_SUPPRESS_VULKAN

///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP Window Input Context (WIC)
/// LAP WIC is a ease-of-use micro window toolkit for OpenGL, which wraps a modified version of GLFW 3.2 (http://www.glfw.org/) 
/// and a modified version of the GLAD extension loader (https://github.com/Dav1dde/glad). Compared to the original libraries LAP WIC
/// makes using GLFW and GLAD functionality as simple as including a .h and a .cpp file into your project. Therefore, the user controls 
/// the compiler, the compiler settings, and the entire deployment process. Test examples are provided with the library. 
/// LAPWIC focuses on minimizing user effort by offering an object oriented structure with modern callback capabilities (member functions, 
/// lambdas, etc), minimal threading restrictions (create many windows from different threads and run them concurrently - but don't forget
/// to synchronize OpenGL access), the option of using events instead of callbacks, background multi-context multi-thread OpenGL extension
/// loading and management and a streamlined window creation process based on requested properties.
///
/// @par Configuration:
/// OpenGL token and core functions extension loading can be disabled by defining :
/// #define LAP_WIC_SUPPRESS_GL_EXTENSIONS
/// in the config section of lap_wic.hpp(line 5 of lap_wic.hpp).
/// This must be used when lap_wic is used with another OpenGL extension loading library.
/// Vulkan functions can be disabled by defining :
/// #define LAP_WIC_SUPPRESS_VULKAN
/// in the config section of lap_wic.hpp(line 7 of lap_wic.hpp).
/// This is useful when not working with Vulkan.
///
/// @par Design and Threading Restrictions:
/// In singlethreaded mode lap_wic works exactly like any other windowing toolkit, a WICSystem object has to be created for the entire
/// program (singleton), which initializes the necessary system queries. After this windows can be created, controlled and used for rendering.
/// In multithreaded mode lap_wic works as a client-server architecture with the program-wide WICSYstem object acting as server and 
/// WIndow objects created outside of the main thread acting as clients. This design is required because there are several platform limitations 
/// which are impossible to bypass in a clean multi-threaded manner. Not all window commands require this type of protocol, the ones working
/// in this manner are documented. Other designs are possible : single threaded only or taking control of the main thread, like (free)glut, but
/// leaves the maximum programmer flexbility. The consequences of this design are the following:
/// - When the WICSystem object is destroyed, it WAITS for the destructors of all the registered windows, thus Window objects have to go out of 
/// scope during the lifetime of WICSystem, in a standard OOP manner.
/// - commands will only be executed after the server has finished processing them. This processing is done by continuously calling the 
/// WicSystem.processProgramEvents() function from the main thread. DON'T BLOCK THE MAIN THREAD WHILE USING WIC.
/// - WICSystem is a singleton, it will assert if the user tries to create more than one instance.
/// - WICSystem.processProgramEvents(), WICSystem.WICSystem(), WICSystem.~WICSystem() can only be called form the main thread (assert otherwise)
///
///
/// @par OpenGL :
/// IMPORTANT: The lap_wic OpenGL function extension loader only loads functions and tokens which are included in the compatibility profie or are
///	ubiquitous.Lap_wic contains functions which enable manual function extension loading.
/// The lap_wic library is by default ran in a thread-safe manner but OpenGL access is NOT thread safe and NEEDS manual synchronization.
/// Therefore all context operations (window.swapBuffers(), window.setSwapInterval(), window.bindContext() and window.unbindContext())
/// and OpenGL commands (glCreateShader, glBufferData, etc) require synchronized access, if rendering is performed concurrently from multiple
/// threads. The context functions are properly documented.
/// For more information consult the specification, section 2.3.: https://www.opengl.org/registry/doc/glspec45.core.pdf
/// Quick list of important concepts:
/// - in OpenGL there can be only one context per thread
/// - an OpenGL command issued on a contextless thread results in undefined behavior
/// - context switches are costly as they save/restore ALL state and cause a GPU flush. On some platforms even the function pointers have to be reloaded!.
/// - your application must ensure that two tasks that access the same context are not allowed to execute concurrently. This is valid on both CPU
/// where context are binded/unbinded per thread (with explicit synchronization, e.g.: locks, lockless) and on the GPU (where objects shared between
/// multiple contexts have to be synchronized with sync/fances).
/// - if issueing OpenGL commands concurrently from multiple threads, each with their own context, it is very probable that the GPU driver
/// will perform a hidden synchronization, as internally OpenGL drivers are generally single threaded. Thus it is usually most efficient to have a 
/// single OpenGL thread. Multithreaded rendering can be performed by using non immediate contexts which hold queues of OpenGL commands, and 
/// collecting the commands with a single OpenGL rendering thread (this technique emilates DirectX11)
///
///
///@par Novel aspects:
///While there are other adequate windowing toolkits availble for free, this toolkit offers several new features:\n
/// 1. absolutely no deployment issues, besides linking with the system libraries (and vulkan if necessary/possible). LAPWIC is plug play,
/// just insert the lap_wic.hpp, lap_wic_dep.hpp and lap_wic.cpp into your build system. No libs, no compilers, no dlls, no libs, no exports.
/// 2. minimal threading restrictions. Most windowing toolkit libraries limit the window creation and event processing to the main thread,
/// usually due to windowing system limitations. LAPWIC asks only for a single function to be constantly called from the main thread, 
/// (WICSystem.processProgramEvents()), besides this function and WICSystem creation and destruction any function can be used from everywhere.
/// The threading restricted functions are documented, and perform internal system initializations and application wide input processing.
/// For a more clear program structure see the examples shown in this file or test.cpp.
/// LAPWIC in multithreading is dependent on the WICSystem.processProgramEvents() function, thus the user has to guarantee that this function
/// gets executed. E.g.: the user creates a window from a secondary thread, but has to guarantee that the main thread is running when the
/// secondary thread is creating the window.
/// 3. integrated multi-context multi-thread extension loading. LAPWIC can be used from many threads with different OpenGL contexts, 
/// in comparison to the majority of other extension loader libraries. The extension are handled in a cached manner, thus a change
/// of context only implies a change of function pointers and a pipeline flush. Thus a context switch is expensive (the pipeline flush) but 
/// not prohibitively expensive.
/// 4. a flexibile object oriented input and windows event system. The events can be handled through modern callbacks, which accept lambdas, 
/// delegates, functors, basically anything storable in a std::function, or through events, which come with window information, input state
/// information and a timestamp.
/// 5. LAPWIC makes it extremely easy to decouple a rendering thread with window control from an input processing thread.
/// 6. each window can run its own main function, completely decoupled from other windows.
/// Besides the novel features presented here LAPWIC provides almost all the features from both GLFW and GLAD, along with minor bugfixes\n
///
/// @version 1.05
/// @par Properties:
///	- c++11
///	- works on Windows, *nix and MACOS
///	- object oriented
///	- window, input, monitors, context creation and vulkan access and handling through GLFW (http://www.glfw.org/)
///	- OpenGL extensions loading through GLAD (https://github.com/Dav1dde/glad)
/// 	- OpenGL extensions can be exported as macros or as functions (by default global namespace functions). This can be changed by changing gl_as_functions from True to False in dep_gen/generator.py and then running the script)
///		- Macros are a C-style solution, they can be undefined, lead to more coupled code, can't be shadowed (e.g. preprocessor will transform struct.glCullFace into struct.loader_glCullFace). They are used by:
///			- glad (https://github.com/Dav1dde/glad)
///			- glsdk (http://glsdk.sourceforge.net/docs/html/index.html) if using c mode
///		- Functions are a CPP-style solution, they support intelligent code completion, can be used as "extern" from other files without including the interface, lead to increased code decoupling, can be shadowed (but don't compile with -Wshadow). They are used by:
///			- glew 2.0 (http://glew.sourceforge.net/)
///			- glbinding (https://github.com/cginternals/glbinding) , under gl namespace.
///			- gl3w (https://github.com/skaslev/gl3w)
///			- glsdk (http://glsdk.sourceforge.net/docs/html/index.html) if using cpp mode, under the gl namespace
///	- minimal size
///	- minimal deployment, just plug and play.
///	- minimal threading restrictions, besides calling WICSystem.processProgramEvents(), WICSystem.WICSystem and WICSystem.~WICSystem() from the main thread there are no other restrictions.
///	- RAII window creation from any thread.
///	- window events and input processing with either callbacks or events
///	- modern callbacks
///		- delegates / member functions
///		- lambdas
///		- free functions
///		- functors 
///		- anything captureable with std::function
///	- cached multi-context multi-thread OpenGL extension loading and management.
///	- input remapper (keys, mouse buttons, controller buttons)
///	- each window has it's own main and can be controlled independently
///	- streamlined window creation process : windows are created through requested property structs:
///		- WindowProperties : requested properties of the system window such as decorated, focused, visible, size, position, etc.
///		- FramebufferProperties : channel bit depths, double buffering, accumulation buffers for legacy OpenGL, etc
///		- ContextProperties : type of context, debug options, requested version, etc
///		- InputProperties : cursor enabled? sticky keys/cursor? etc
///	- the event system provides direct access to the window input state
///	- all keys and mouse buttons offer access to the last pressed timestamp
///	- lapwic can be used as a windowing toolkit without OpenGL
///	- examples are provided in test.cpp \n
///
/// @par Compiling and linking:
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///		- also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder\n
///
/// @par Limitations and Observations:
///	- the main thread MUST always run a main program loop, like: 
///		while (WICSystem.isAnyWindowOpened()) {
///			WICSystem.processProgramEvents();
///		}
///	- windows from non-main threads use a blocking type of communication with the main thread, which is hidden from the library user,
///	thus the main thread should NEVER block inside the loop.
///	- the almost lack of threading limitations has the positive aspect of potentially specializing the main thread for input handling
///	- as LAPWIC depends on GLFW, please read http://www.glfw.org/faq.html and https://github.com/glfw/glfw/issues if you are encountering unexpected difficulties.
///	- do not include Windows.h or other platform specific headers unless you plan on using those APIs directly, if you do need to include such headers, do it before including the GLFW.\n
///		
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Usage:
///	- please check test.cpp as it contains in-depth commented examples (including advanced topics such as multi-context, multi-threaded, multi-window opengl)
///	- simple usage
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
///		...
///		while (window.isOpened()) {
///			//DO WORK HERE
///			window.swapBuffers();
///			while (const Event* e = window.processEvent()) {
///				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window.close();
///				if (e->type == Event::Type::MouseMove) std::cout << "Mouse move at " << e->MouseMove.position_x << " " << e->MouseMove.position_y << std::endl;
///			}
///			WICSystem.processProgramEvents();
///		}
///		@endcode
///
///	- callbacks
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
///		window.setCallbackMouseScroll([](const Window& caller, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState&, uint64_t timestamp) {
///			std::cout << "[Lambda Callback] Window " << caller.getWindowProperties().title << " Mouse scroll " << scrolly << std::endl;
///		});
///		...
///		while (window.isOpened()) {
///			//DO WORK HERE
///			window.swapBuffers();
///			window.processEvents();
///			WICSystem.processProgramEvents();
///		}
///		@endcode
///
///	- multiple windows
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		WindowProperties wp1; wp1.title = "first window";
///		Window window1 = Window(wp1, FramebufferProperties(), ContextProperties(), InputProperties());
///		WindowProperties wp2; wp2.title = "seconds window";
///		Window window2 = Window(wp2, FramebufferProperties(), ContextProperties(), InputProperties());
///		...
///		while (WICSystem.isAnyWindowOpened()) {
///			if(window1.isOpened()){
///				//DO WORK HERE
///				window1.swapBuffers();
///				while (const Event* e = window1.processEvent()) {
///					if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window1.close();
///				}
///			}
///			if(window2.isOpened()){
///				//DO WORK HERE
///				window2.swapBuffers();
///				while (const Event* e = window2.processEvent()) {
///					if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window2.close();
///				}
///			}
///			WICSystem.processProgramEvents();
///		}
///		@endcode
///
///	- shared opengl context
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		WindowProperties wp1; wp1.title = "first window";
///		Window window1 = Window(wp1, FramebufferProperties(), ContextProperties(), InputProperties());
///		WindowProperties wp2; wp2.title = "seconds window";
///		Window window2 = Window(wp2, FramebufferProperties(), ContextProperties(), InputProperties(), nullptr, &window1);
///		...
///		window1.bindContext();
///		//DO OPENGL WORK HERE ON CONTEXT 1
///		...
///		window2.bindContext(); //shared lists with window 1
///		while (window2.isOpened()) {
///			//DO OPENGL WORK HERE ON CONTEXT 2 USING WORK FROM CONTEXT 1 (shared)
///			window2.swapBuffers();
///			while (const Event* e = window2.processEvent()) {
///				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window2.close();
///			}
///			WICSystem.processProgramEvents();
///		}
///		@endcode
///
///	- toggle fullscreen
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		Window window = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
///		...
///		while (window.isOpened()) {
///			//DO WORK HERE
///			window.swapBuffers();
///			while (const Event* e = window.processEvent()) {
///				if (e->type == Event::Type::KeyPress) {
///					if (e->KeyPress.key == Key::F1) window.setFullscreen();
///					if (e->KeyPress.key == Key::F2) window.setWindowed(400, 400, 100, 100);
///				}
///			}
///			WICSystem.processProgramEvents();
///		}
///		@endcode
///
///	- manual extension loading (lapwic only loads the OpenGL core / compatibility functions and the ubiquitous extensions)
///		@code
///		#include "lap_wic.hpp"
///		...
///		using namespace lap::wic;
///		WICSystem wicsystem(&std::cout);
///		Window window1 = Window(WindowProperties(), FramebufferProperties(), ContextProperties(), InputProperties());
///		...
///		supported = window.isOpenGLExtensionSupported("GL_ARB_sparse_buffer");
///		std::cout << " GL_arb_sparse_buffer support = " << std::boolalpha << supported << std::endl;
///		if (supported) {
///			typedef void (APIENTRYP PFNGLBUFFERPAGECOMMITMENTARBPROC)(GLenum target, GLintptr offset, GLsizeiptr size, GLboolean commit);
///			PFNGLBUFFERPAGECOMMITMENTARBPROC my_glBufferPageCommitmentARB = (PFNGLBUFFERPAGECOMMITMENTARBPROC)window.loadOpenGLExtensionFunction("glBufferPageCommitmentARB");
///			// do work here but don't forget this function pointer is unmanaged and might be (usually isn't) valid only for this context
///		}
///		...
///		@endcode
///------------------------------------------------------------------------------------------------



#ifndef LAP_WIC_H_
#define LAP_WIC_H_

#include <vector>
#include <list>
#include <functional>
#include <iostream>
#include <string>
#include <map>
#include <chrono>
#include <thread>
#include <mutex>

//this guard is used by lap_wic.cpp (the VK related exports are done by GLFW too)
#ifndef LAP_WIC_INTERNAL_GUARD
#if (defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__MINGW64__)) && !(defined(__MINGW32__) || defined (__MINGW64__))
	#define NOMINMAX
	#include "lap_wic_dep.hpp"
	#undef NOMINMAX
#else
	#include "lap_wic_dep.hpp"
#endif
#ifndef LAP_WIC_SUPPRESS_VULKAN
#define VK_NULL_HANDLE 0

typedef void* VkInstance;
typedef void* VkPhysicalDevice;
typedef uint64_t VkSurfaceKHR;
typedef uint32_t VkFlags;
typedef uint32_t VkBool32;

typedef enum VkStructureType
{
	VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR = 1000004000,
	VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR = 1000005000,
	VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR = 1000006000,
	VK_STRUCTURE_TYPE_MIR_SURFACE_CREATE_INFO_KHR = 1000007000,
	VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR = 1000009000,
	VK_STRUCTURE_TYPE_MAX_ENUM = 0x7FFFFFFF
} VkStructureType;

typedef enum VkResult
{
	VK_SUCCESS = 0,
	VK_NOT_READY = 1,
	VK_TIMEOUT = 2,
	VK_EVENT_SET = 3,
	VK_EVENT_RESET = 4,
	VK_INCOMPLETE = 5,
	VK_ERROR_OUT_OF_HOST_MEMORY = -1,
	VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
	VK_ERROR_INITIALIZATION_FAILED = -3,
	VK_ERROR_DEVICE_LOST = -4,
	VK_ERROR_MEMORY_MAP_FAILED = -5,
	VK_ERROR_LAYER_NOT_PRESENT = -6,
	VK_ERROR_EXTENSION_NOT_PRESENT = -7,
	VK_ERROR_FEATURE_NOT_PRESENT = -8,
	VK_ERROR_INCOMPATIBLE_DRIVER = -9,
	VK_ERROR_TOO_MANY_OBJECTS = -10,
	VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
	VK_ERROR_SURFACE_LOST_KHR = -1000000000,
	VK_SUBOPTIMAL_KHR = 1000001003,
	VK_ERROR_OUT_OF_DATE_KHR = -1000001004,
	VK_ERROR_INCOMPATIBLE_DISPLAY_KHR = -1000003001,
	VK_ERROR_NATIVE_WINDOW_IN_USE_KHR = -1000000001,
	VK_ERROR_VALIDATION_FAILED_EXT = -1000011001,
	VK_RESULT_MAX_ENUM = 0x7FFFFFFF
} VkResult;

typedef struct VkAllocationCallbacks VkAllocationCallbacks;

typedef struct VkExtensionProperties
{
	char            extensionName[256];
	uint32_t        specVersion;
} VkExtensionProperties;

#ifndef APIENTRY
#define APIENTRY
#endif
typedef void (APIENTRY * PFN_vkVoidFunction)(void);
typedef PFN_vkVoidFunction(APIENTRY * PFN_vkGetInstanceProcAddr)(VkInstance, const char*);
typedef VkResult(APIENTRY * PFN_vkEnumerateInstanceExtensionProperties)(const char*, uint32_t*, VkExtensionProperties*);
#endif
#endif

namespace lap {
	namespace wic {

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Keyboard Keys
		enum class Key : int {
			UNKNOWN = -1,		///< a non-standard key
			SPACE = 32,			///< the space key
			APOSTROPHE = 39,	///< the ' key
			COMMA = 44,			///< the , key
			MINUS = 45,			///< the - key
			PERIOD = 46,		///< the . key
			SLASH = 47,			///< the / key
			NUM0 = 48,			///< the 0 key
			NUM1 = 49,			///< the 1 key
			NUM2 = 50,			///< the 2 key
			NUM3 = 51,			///< the 3 key
			NUM4 = 52,			///< the 4 key
			NUM5 = 53,			///< the 5 key
			NUM6 = 54,			///< the 6 key
			NUM7 = 55,			///< the 7 key
			NUM8 = 56,			///< the 8 key
			NUM9 = 57,			///< the 9 key
			SEMICOLON = 59,		///< the ; key
			EQUAL = 61,			///< the = key
			A = 65,				///< the A key
			B = 66,				///< the B key
			C = 67,				///< the C key
			D = 68,				///< the D key
			E = 69,				///< the E key
			F = 70,				///< the F key
			G = 71,				///< the G key
			H = 72,				///< the H key
			I = 73,				///< the I key
			J = 74,				///< the J key
			K = 75,				///< the K key
			L = 76,				///< the L key
			M = 77,				///< the M key
			N = 78,				///< the N key
			O = 79,				///< the O key
			P = 80,				///< the P key
			Q = 81,				///< the Q key
			R = 82,				///< the R key
			S = 83,				///< the S key
			T = 84,				///< the T key
			U = 85,				///< the U key
			V = 86,				///< the V key
			W = 87,				///< the W key
			X = 88,				///< the X key
			Y = 89,				///< the Y key
			Z = 90,				///< the Z key
			LEFT_BRACKET = 91,	///< the [ key
			BACKSLASH = 92,		///< the "\" key
			RIGHT_BRACKET = 93, ///< the ] key
			GRAVE_ACCENT = 96,	///< the ` key
			WORLD_1 = 161,		///< non - US #1
			WORLD_2 = 162,		///< non - US #2
			ESCAPE = 256,		///< the escape key
			ENTER = 257,		///< the enter key
			TAB = 258,			///< the tab key
			BACKSPACE = 25,		///< the backspace key
			INSERT = 260,		///< the insert key
			DEL = 261,			///< the delete key
			RIGHT = 262,		///< the right key
			LEFT = 263,			///< the left key
			DOWN = 264,			///< the down key
			UP = 265,			///< the up key
			PAGE_UP = 266,		///< the page up key
			PAGE_DOWN = 267,	///< the page down key
			HOME = 268,			///< the home key
			END = 269,			///< the end key
			CAPS_LOCK = 280,	///< the caps lock key
			SCROLL_LOCK = 281,	///< the scroll lock key
			NUM_LOCK = 282,		///< the num lock key
			PRINT_SCREEN = 283,	///< the print screen key
			PAUSE = 284,		///< the pause key
			F1 = 290,			///< the F1 key
			F2 = 291,			///< the F2 key
			F3 = 292,			///< the F3 key
			F4 = 293,			///< the F4 key
			F5 = 294,			///< the F5 key
			F6 = 295,			///< the F6 key
			F7 = 296,			///< the F7 key
			F8 = 297,			///< the F8 key
			F9 = 298,			///< the F9 key
			F10 = 299,			///< the F10 key
			F11 = 300,			///< the F11 key
			F12 = 301,			///< the F12 key
			F13 = 302,			///< the F13 key
			F14 = 303,			///< the F14 key
			F15 = 304,			///< the F15 key
			F16 = 305,			///< the F16 key
			F17 = 306,			///< the F17 key
			F18 = 307,			///< the F18 key
			F19 = 308,			///< the F19 key
			F20 = 309,			///< the F20 key
			F21 = 310,			///< the F21 key
			F22 = 311,			///< the F22 key
			F23 = 312,			///< the F23 key
			F24 = 313,			///< the F24 key
			F25 = 314,			///< the F25 key
			NUMPAD0 = 320,		///< the numpad 0 key
			NUMPAD1 = 321,		///< the numpad 1 key
			NUMPAD2 = 322,		///< the numpad 2 key
			NUMPAD3 = 323,		///< the numpad 3 key
			NUMPAD4 = 324,		///< the numpad 4 key
			NUMPAD5 = 325,		///< the numpad 5 key
			NUMPAD6 = 326,		///< the numpad 6 key
			NUMPAD7 = 327,		///< the numpad 7 key
			NUMPAD8 = 328,		///< the numpad 8 key
			NUMPAD9 = 329,		///< the numpad 9 key
			NUMPAD_DECIMAL = 330,	///< the numpad decimal key
			NUMPAD_DIVIDE = 331,	///< the numpad divide key
			NUMPAD_MULTIPLY = 332,	///< the numpad multiply key
			NUMPAD_SUBTRACT = 333,	///< the numpad subtract key
			NUMPAD_ADD = 334,		///< the numpad add key
			NUMPAD_ENTER = 335,		///< the numpad enter key
			NUMPAD_EQUAL = 336,		///< the numpad equal key
			LEFT_SHIFT = 340,		///< the numpad shift key
			LEFT_CONTROL = 341,		///< the numpad control key
			LEFT_ALT = 342,			///< the numpad alt key
			LEFT_SYSTEM = 343,		///< the numpad super/system key
			LEFT_SUPER = LEFT_SYSTEM,///< the numpad super/system key
			RIGHT_SHIFT = 344,		///< the numpad right shift key
			RIGHT_CONTROL = 345,	///< the numpad right control key
			RIGHT_ALT = 346,		///< the numpad right alt key
			RIGHT_SYSTEM = 347,		///< the numpad right super/system key
			RIGHT_SUPER = RIGHT_SYSTEM, ///< the numpad right super/system key
			MENU = 348,				///< the numpad menu key
			LAST = MENU,			///< the last/max key number
			MAX = LAST				///< the last/max key number
		};
		
		/// Mouse Buttons
		enum class MouseButton : int {
			LEFT = 0,				///< mouse left button
			RIGHT = 1,				///< mouse right button
			MIDDLE = 2,				///< mouse middle button
			EXTRA1 = 3,				///< mouse 4th button
			EXTRA2 = 4,				///< mouse 5th button
			EXTRA3 = 5,				///< mouse 6th button
			EXTRA4 = 6,				///< mouse 7th button
			EXTRA5 = 7,				///< mouse 8th button
			LAST = EXTRA5,			///< mouse last/max button
			MAX = LAST				///< mouse last/max button
		};
		
		///Controller (joysticks, gamepads, wheels, etc)
		enum class Controller : int {
			DEVICE_1 = 0,				///< the 1st controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_2 = 1,				///< the 2nd controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_3 = 2,				///< the 3rd controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_4 = 3,				///< the 4th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_5 = 4,				///< the 5th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_6 = 5,				///< the 6th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_7 = 6,				///< the 7th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_8 = 7,				///< the 8th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_9 = 8,				///< the 9th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_10 = 9,				///< the 10th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_11 = 10,				///< the 11th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_12 = 11,				///< the 12th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_13 = 12,				///< the 13th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_14 = 13,				///< the 14th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_15 = 14,				///< the 15th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			DEVICE_16 = 15,				///< the 16th controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			LAST = DEVICE_16,			///< the last/max controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
			MAX = LAST,					///< the last/max controller ( gamepad / joystick / trackball / throttle / steering wheel / etc )
		};
		
		/// Controller Buttons
		enum class ControllerButton : int{
			BUTTON_1 = 0,				///< the 1st controller button
			BUTTON_2 = 1,				///< the 2nd controller button
			BUTTON_3 = 2,				///< the 3rd controller button
			BUTTON_4 = 3,				///< the 4th controller button
			BUTTON_5 = 4,				///< the 5th controller button
			BUTTON_6 = 5,				///< the 6th controller button
			BUTTON_7 = 6,				///< the 7th controller button
			BUTTON_8 = 7,				///< the 8th controller button
			BUTTON_9 = 8,				///< the 9th controller button
			BUTTON_10 = 9,				///< the 10th controller button
			BUTTON_11 = 10,				///< the 11th controller button
			BUTTON_12 = 11,				///< the 12th controller button
			BUTTON_13 = 12,				///< the 13th controller button
			BUTTON_14 = 13,				///< the 14th controller button
			BUTTON_15 = 14,				///< the 15th controller button
			BUTTON_16 = 15,				///< the 16th controller button
			BUTTON_17 = 16,				///< the 17th controller button
			BUTTON_18 = 17,				///< the 18th controller button
			BUTTON_19 = 18,				///< the 19th controller button
			BUTTON_20 = 19,				///< the 20th controller button
			BUTTON_21 = 20,				///< the 21th controller button
			BUTTON_22 = 21,				///< the 22th controller button
			BUTTON_23 = 22,				///< the 23th controller button
			BUTTON_24 = 23,				///< the 24th controller button
			LAST = BUTTON_24,			///< the last/max controller button
			MAX = LAST					///< the last/max controller button
		};

		//consts
		const unsigned int JOYSTICK_MAX_AXES = 6;				///< the maximum number of joystick axes
		const unsigned int EVENT_BUFFER_START_SIZE = 250;		///< the maximum number of events per frame

		/// InputState
		class InputState {
		public:
			InputState();
			InputState(const InputState&) = delete;
			InputState(InputState&&) = default;
			InputState& operator=(const InputState&) = delete;
			InputState& operator=(InputState&&) = default;
			~InputState() = default;
		public:
			bool keys[(int)Key::MAX];						///< key state for the owner window
			uint64_t keys_timestamp[(int)Key::MAX];///< the timestamp (the duration since start of program) of when the key was last pressed (useful for key combinations over time)
			bool mod_alt;										///< the ALT modifier state
			bool mod_control;									///< the control modifier state
			bool mod_shift;										///< the shift modifier state
			bool mod_system;									///< the system modifier state
			struct {
				unsigned int position_x;						///< the current mouse x position
				unsigned int position_y;						///< the current mouse y position
				unsigned int prev_position_x;				///< the previous mouse x position
				unsigned int prev_position_y;				///< the previous mouse y position
				float last_scroll_x;								///< the last scroll on the x axis
				float last_scroll_y;								///< the last scrool on the y axis
				bool buttons[(int)MouseButton::MAX];	///< state of the mouse buttons
				uint64_t buttons_timestamp[(int)MouseButton::MAX];	///< the timestamp (the duration since start of program) of when the button was last pressed (useful for mouse button combinations over time)
			}mouse;
			struct {
				bool connected;									///< does the this controller entry correspond to a connected controller?
				std::string name;								///< the name of the controller
				float axes[JOYSTICK_MAX_AXES];		///< the axes values
				bool buttons[(int)ControllerButton::MAX];///< the state of the controller buttons
			}controller[(int)Controller::MAX];
			std::string clipboard_text;						///< the text on the clipboard
		};




		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// MonitorGammaRamp
		struct MonitorGammaRamp {
			MonitorGammaRamp() = default;
			MonitorGammaRamp(const MonitorGammaRamp&) = default;
			MonitorGammaRamp(MonitorGammaRamp&&) = default;
			MonitorGammaRamp& operator=(const MonitorGammaRamp&) = default;
			MonitorGammaRamp& operator=(MonitorGammaRamp&&) = default;
			~MonitorGammaRamp() = default;
			std::vector<unsigned int> red;				///< array with the gammma ramp for the red channel
			std::vector<unsigned int> green;			///< array with the gammma ramp for the green channel
			std::vector<unsigned int> blue;				///< array with the gammma ramp for the blue channel
			unsigned int size;							///< the size of the red / green / blue arrays
		};
		/// MonitorMode
		struct MonitorMode {
			MonitorMode() = default;
			MonitorMode(const MonitorMode&) = default;
			MonitorMode(MonitorMode&&) = default;
			MonitorMode& operator=(const MonitorMode&) = default;
			MonitorMode& operator=(MonitorMode&&) = default;
			~MonitorMode() = default;
			unsigned int width;							///< the width of the monitor mode
			unsigned int height;						///< the width of the monitor mode
			unsigned int redbits;						///< the number of bits of the red channel
			unsigned int greenbits;						///< the number of green of the red channel
			unsigned int bluebits;						///< the number of blue of the red channel
			unsigned int refreshrate;					///< the refresh rate in Hz
		};
		/// Monitor
		struct Monitor {
			Monitor() = default;
			Monitor(const Monitor&) = default;
			Monitor& operator=(const Monitor&) = default;
			Monitor(Monitor&&) = default;
			Monitor& operator=(Monitor&&) = default;
			~Monitor() = default;
			bool connected;								///< is the monitor connected?
			unsigned int position_x;					///< the x position of the monitor (on the virtual desktop space, in screen coordinates)
			unsigned int position_y;					///< the y position of the monitor (on the virtual desktop space, in screen coordinates)
			unsigned int physical_sizex;				///< the physical width of the monitor
			unsigned int physical_sizey;				///< the physical width of the monitor
			std::string name;							///< the name of the monitor
			MonitorGammaRamp gamma_ramp;				///< the gamma ramp
			MonitorMode mode_current;					///< the current working mode (e.g. 1600x900@60Hz)
			std::vector<MonitorMode> modes;				///< the available working modes
			void *handle;								///< raw handle
		};


		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Event 
		struct Event {
			/// Event Types
			enum class Type{
				MonitorConnect,							///< signals that a monitor was connected (data in MonitorConnect)
				MonitorDisconnect,						///< signals that a monitor was disconnected (data in MonitorDiconnect)
				ControllerConnect,						///< signals that a (game) controller was connected (data in ControllerConnect)
				ControllerDisconnect,					///< signals that a (game) controller was disconnected (data in ControllerConnect)
				ControllerButtonPress,				///< signals that a (game) controller's button was pressed
				ControllerButtonRelease,			///< signals that a (game) controller's button was released
				ControllerAxisMove,					///< signals that a (game) controller's axis was moved
				FilesDrop,									///< files were dragged and dropped on this window	(data in FilesDrop)
				WindowMove,							///< this window has moved (data in WindowMove)
				WindowResize,							///< this window was resized, the size includes decorations (data in WindowResize)
				WindowRefresh,							///< this window needs refreshing (no other data)
				WindowClose,							///< generated immediately before closing the window, coming from click on the x button, alt-f4, ctrl-d, sigkill, etc, (no other data).
				WindowFocusGain,						///< this window has gained focus (no other data)
				WindowFocusLose,						///< this window has lost focus (no other data)
				WindowMinimize,						///< this window was minimized(no other data)
				WindowMaximize,						///< this window was maximized(no other data)
				WindowRestore,							///< this window was restored (no other data)
				FramebufferResize,					///< the framebuffer of this window was resized, the size does not include decorations (data in FramebufferResize)
				KeyPress,									///< a key was pressed (data in KeyPress)
				KeyRelease,								///< a kay was released (data in KeyRelease)
				KeyRepeat,								///< a key was repeated (data in KeyRepeat)
				UnicodeChar,								///< an unicode char (one or a sequence of keys) was entered (without the use of system/control/alt/shift keys) (data in UnicodeChar)
				UnicodeCharMods,						///< an unicode char (one or a sequence of keys) was entered (includes characters generated with system/control/alt/shift) (data in UnicodeCharMods)
				MouseMove,								///< the mouse was moved (data in MouseMove)
				MouseDrag,								///< the mouse was dragged (data in MouseDrag)
				MousePress,								///< a mouse button was pressed (data in MousePressed)
				MouseRelease,							///< a mouse button was released (data in MouseReleased)
				MouseScroll,								///< the mouse wheel was scrolled (data in MouseScroll)
				MouseEnter,								///< the mouse has entered the window space (no other data)
				MouseLeave								///< the mouse has left the window space (no other data)
			};
			Event();
			Event(const Event&) = default;
			Event(Event&&) = default;
			Event& operator=(const Event&) = default;
			Event& operator=(Event&&) = default;
			~Event();
		private:
			struct MonitorConnectEvent {
				Monitor *monitor = nullptr;				///< the connected/disconnected monitor
			};
			struct MonitorDisconnectEvent {
				Monitor *monitor = nullptr;				///< the connected/disconnected monitor
			};
			struct ControllerConnectEvent {
				int id;									///< the id of the connected controller
			};
			struct ControllerDisconnectEvent {
				int id;									///< the id of the connected controller
			};
			struct ControllerButtonPressEvent {
				int id;									///< the id of the controller
				ControllerButton button;				///< the id of the pressed button
			};
			struct ControllerButtonReleaseEvent {
				int id;									///< the id of the controller
				ControllerButton button;				///< the id of the released button
				
			};
			struct ControllerAxisMoveEvent {
				int id;									///< the id of the controller
				float axes[JOYSTICK_MAX_AXES];			///< the controller axes
			};
			struct FilesDropEvent {
				std::vector<std::string>* paths;		///< the paths dropped on the window
			};
			struct WindowMoveEvent{
				unsigned int position_x;				///< the window position on the x axis
				unsigned int position_y;				///< the window position on the y axis
			};
			struct WindowResizeEvent {
				unsigned int width;						///< the window width
				unsigned int height;					///< the window height
			};
			struct FramebufferResizeEvent {
				unsigned int width;						///< the framebuffer width
				unsigned int height;					///< the framebuffer height
			};
			struct KeyPressEvent{
				Key key;								///< the pressed key
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct KeyReleaseEvent {
				Key key;								///< the released key
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct KeyRepeatEvent {
				Key key;								///< the repeated key
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct UnicodeCharEvent {
				unsigned int codepoint;					///< the codepoint for the unicode char
			};
			struct UnicodeCharModsEvent {
				unsigned int codepoint;					///< the codepoint for the unicode char
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct MouseMoveEvent {
				unsigned int position_x;				///< the mouse position on the x axis
				unsigned int position_y;				///< the mouse position on the y axis
			};
			struct MousePressEvent {
				MouseButton button;						///< the pressed mouse button
				unsigned int position_x;				///< the mouse position on the x axis
				unsigned int position_y;				///< the mouse position on the y axis
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct MouseReleaseEvent {
				MouseButton button;						///< the pressed mouse button
				unsigned int position_x;				///< the mouse position on the x axis
				unsigned int position_y;				///< the mouse position on the y axis
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct MouseDragEvent {
				MouseButton button;
				unsigned int position_x;
				unsigned int position_y;
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
			struct MouseScrollEvent {
				float scroll_x;							///< the amount of x scroll 
				float scroll_y;							///< the amount of y scroll (a simple mouse is vertical) (positive is up, negative is down)
				int16_t position_x;						///< the mouse position on the x axis
				int16_t position_y;						///< the mouse position on the y axis
				bool alt;								///< is the alt key pressed?
				bool control;							///< is the control key pressed?
				bool shift;								///< is the shift key pressed?
				bool system;							///< is the system key pressed?
			};
		public:
			Type type;									///< type of event
			uint64_t timestamp;							///< timestamp (the duration since start of program)
			union {
				MonitorConnectEvent MonitorConnect;		///< monitor connect event data
				MonitorDisconnectEvent MonitorDisconnect;///< monitor disconnect event data
				ControllerConnectEvent ControllerConnect;///< controller connect event data
				ControllerDisconnectEvent ControllerDisconnect;		///< controller disconnect event data
				ControllerButtonPressEvent ControllerButtonPress;	///< controller button press event data
				ControllerButtonReleaseEvent ControllerButtonRelease;///< controller button release event data
				ControllerAxisMoveEvent ControllerAxisMove;			///< controller axis move event data
				FilesDropEvent FilesDrop;				///< files dropped on window event data
				WindowMoveEvent WindowMove;				///< window moved event data
				WindowResizeEvent WindowResize;			///< window resized event data
				FramebufferResizeEvent FramebufferResize;///< framebuffer resized event data
				KeyPressEvent KeyPress;					///< key press event data
				KeyReleaseEvent KeyRelease;				///< key release event data
				KeyRepeatEvent KeyRepeat;				///< key repeat event data
				UnicodeCharEvent UnicodeChar;			///< unicode char was entered event data
				UnicodeCharModsEvent UnicodeCharMods;	///< unicode char was entered with modifiers event data
				MouseMoveEvent MouseMove;				///< mouse moved event data
				MousePressEvent MousePress;				///< mouse pressed event data
				MouseReleaseEvent MouseRelease;			///< mouse released event data
				MouseDragEvent MouseDrag;				///< mouse dragged event data
				MouseScrollEvent MouseScroll;			///< mouse scrolled event data
			};
		};

		
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Input Remapper
		/// This can be used by each window to remap the received input.
		/// NOTE: the Input Remapper does not map multiple keys/mouse buttons/controller buttons or between keys, mouse buttons and controller buttons.
		///		  A more complex InputRemapper, which mapped groups of keys/mouse buttons/controller buttons to groups of keys/mouse buttons/controller buttons
		///		  in any possible combination, was originally written but it was too computationally costly for a general purpose windowing toolkit. Furthermore,
		///		  a true general remapper would include support for double clicks / right clicks and other similiar maps. For these reasons the simple and very 
		///		  fast remapper was considered more practical.
		class InputRemapper {
			friend class Window;
		public:
			/// by default the input remapper maps each key/mouse button/controller button to itself.
			InputRemapper();
			InputRemapper(const InputRemapper&) = default;
			InputRemapper(InputRemapper&&) = default;
			InputRemapper& operator=(const InputRemapper&) = default;
			InputRemapper& operator=(InputRemapper&&) = default;
			~InputRemapper() = default;

			/// maps a key to another key, e.g. A->G
			/// @param from		the original key
			/// @param to		the target key
			void add(Key from, Key to);
			/// maps a mouse button to another mouse button, e.g. LMB->RMB
			/// @param from		the original mouse button
			/// @param to		the target mouse button
			void add(MouseButton from, MouseButton to);
			/// maps a controller button to another controller button, e.g. B1->B2
			/// @param from		the original controller button
			/// @param to		the target controller button
			void add(ControllerButton from, ControllerButton to);
			/// swaps two keys. overwrites previous remaps (it swaps the keys not their current mappings)
			/// @param first	the first key
			/// @param second	the second key
			void swap(Key first, Key second);
			/// swaps two mouse buttons. overwrites previous remaps (it swaps the keys not their current mappings)
			/// @param first	the first mouse button
			/// @param second	the second mouse button
			void swap(MouseButton first, MouseButton second);
			/// swaps two controller buttons. overwrites previous remaps (it swaps the keys not their current mappings)
			/// @param first	the first controller button
			/// @param second	the second controller button
			void swap(ControllerButton first, ControllerButton second);

			/// resets a key mapping, mapping it to itself
			/// @param from		the key
			void reset(Key from);
			/// resets a mouse button mapping, mapping it to itself
			/// @param from		the mouse button
			void reset(MouseButton from);
			/// resets a controller mapping, mapping it to itself
			/// @param from		the controller button
			void reset(ControllerButton from);

			/// resets all mappings, mapping every key/mouse button/controller button to itself
			void reset();
		private:
			///internal usage
			void internalRemap(std::vector<Event>& events);
			std::vector<int> keys;
			std::vector<int> mouse_buttons;
			std::vector<int> controller_buttons;
		};



		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Window Properties
		struct WindowProperties {
			unsigned int width = 800;					///< the width of the window (but not necessarily the framebuffer, that depends on border and decoration)
			unsigned int height = 600;					///< the height of the window (but not necessarily the framebuffer, that depends on border and decoration)
			unsigned int position_x = 100;			///< the position x of the window relative to monitor space (starting from upper left corner)
			unsigned int position_y = 100;			///< the position y of the window relative to monitor space (starting from upper left corner)
			std::string title = "WIC";						///< the window title
			bool opened = false;							///< is the window opened? (ONLY QUERYABLE)
			bool resizable = true;						///< is the window resizable/reshapable ? ignored for fullscreen windows
			bool visible = true;							///< is the window visible? ignored for fullscreen windows
			bool decorated = true;						///< is the window decorated? ignored for fullscreen windows
			bool focused = true;							///< is the window focused? ignored for fullscreen windows
			bool minimized = false;						///< is the window minimized? (ONLY QUERYABLE)
			bool maximized = false;					///< is the window maximized? ignored for fullscreen windows
			bool auto_restore_mode_on_minimize = false;	///< minimize and restore the previous video mode on input focus loss? ignored for windowed windows
			bool floating = false;							///< is the window always on top? ignored for fullscreen windows
			bool fullscreen = false;						///< is the window fullscreen? 
			unsigned int refresh_rate = 60;			///< the referesh rate (the target monitor has to support this rate)
			unsigned char* icon_pixels = nullptr;	///< the icon image (32x32, rgba), if this is left nullptr than the default window icon is used. [Does not work on OSX, will use same icon as the app bundle]
		};

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Framebuffer Properties
		struct FramebufferProperties {
			unsigned int width = 800;					///< the width of the framebuffer (but not necessarily the framebuffer, that depends on border and decoration)
			unsigned int height = 600;					///< the height of the framebuffer (but not necessarily the framebuffer, that depends on border and decoration)
			unsigned int red_bits = 8;					///< the number of bits in the red channel
			unsigned int green_bits = 8;				///< the number of bits in the green channel
			unsigned int blue_bits = 8;					///< the number of bits in the blue channel
			unsigned int alpha_bits = 8;				///< the number of bits in the alpha channel
			unsigned int depth_bits = 24;				///< the number of bits in the depth channel
			unsigned int stencil_bits = 8;				///< the number of bits in the stencil channel
			unsigned int accum_red_bits = 0;		///< the number of bits in the red accumulation channel (this is useful for OLD OpenGL, do not request for modern OpenGL)
			unsigned int accum_blue_bits = 0;		///< the number of bits in the green accumulation channel (this is useful for OLD OpenGL, do not request for modern OpenGL)
			unsigned int accum_green_bits = 0;	///< the number of bits in the blue accumulation channel (this is useful for OLD OpenGL, do not request for modern OpenGL)
			unsigned int accum_alpha_bits = 0;		///< the number of bits in the alpha accumulation channel (this is useful for OLD OpenGL, do not request for modern OpenGL)
			unsigned int aux_buffers = 0;				///< the number of auxiliary buffers (this is useful for OLD OpenGL, do not request for modern OpenGL)
			bool stereo = false;							///< performing stereoscopic rendering ? (requires hardware support!)
			bool doublebuffer = true;					///< is the framebuffer double buffered ? 
			unsigned int samples_per_pixel = 1;	///< what is the number of samples per pixel (used in multisampling), the default is 1 (single sample per pixel)
			bool srgb = false;								///< is the default framebuffer srgba (GL_ARB_framebuffer_sRGB or GL_EXT_framebuffer_sRGB)
		};

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Context Properties
		struct ContextProperties {
			std::thread::id thread_binded;			///< bound to which thread? (ONLY QUERYABLE)
			/// Context Api Type
			enum class ApiType : int {
				NONE,											///< no api, use this if using Vulkan without OpenGL
				OPENGL,										///< OpenGL api
			};
			ApiType api = ApiType::OPENGL;		///< by default the context will use the OpenGL api
			unsigned int version_major = 4;			///< the major version of OpenGL, if unspecified it will default to 4.5
			unsigned int version_minor = 5;			///< the minor version of OpenGL, if unspecified it will default to 4.5
			bool debug_context = false;				///< is this a debug context? (ignored if OpenGL ES is used)
			bool profile_core = false;					///< is this context core ? = is all deprecated functionality is removed?
			bool release_behavior_flush = true;	///< when the context is released is the pipeline flushed?
			/// Context Robustness Type
			enum class RobustnessType : int {
				NONE,											///< no robustness
				NO_RESET_NOTIFICATION,			///< the implementation will never deliver notification of reset events, and GetGraphicsResetStatusARB will always return NO_ERROR (https://www.opengl.org/registry/specs/ARB/robustness.txt)
				LOSE_CONTEXT_ON_RESET			///< a graphics reset will result in the loss of all context state, requiring the recreation of all associated objects.In this case GetGraphicsResetStatusARB may return GUILTY_CONTEXT_RESET_ARB, 
																	///< INNOCENT_CONTEXT_RESET_ARB, UNKNOWN_CONTEXT_RESET_ARB https://www.opengl.org/registry/specs/ARB/robustness.txt)
			};
			RobustnessType robustness = RobustnessType::NONE;	///< robustness type
			/// also known as vertical synchronization, vsync or vertical retrace, it represents the minimum number of frames to be displayed before swap buffers. 
			/// Note: some GPU drivers do not honor the requested swap interval.
			/// This value will be given to the platform dependent OpenGL api (wglSwapIntervalEXT, glXSwapIntervalEXT)
			/// Some useful values:
			///	0  - never performs vsync
			///	1  - always perform vsync : swap buffers at window refresh rate (e.g. 60FPS for a 60Hz video mode)
			///	     this can lead to poor performance when used on frames longer than the refresh rate, e.g. a 18ms frame would last 32ms on 60 Hz mode.
			/// -1 - perfrom vsync adaptively : swap buffers at window refresh rate only if the frame duration is shorter than the refresh rate, requires hardware support.
			int swap_interval = 0;
			std::string driver_version = "";			///< the OpenGL version exposed by the driver	(ONLY QUERYABLE)
			std::string driver_vendor = "";			///< the OpenGL vendor of the driver	(ONLY QUERYABLE)
			std::string driver_renderer = "";			///< the OpenGL renderer used by the driver (ONLY QUERYABLE)
			std::string driver_glsl_version = "";		///< the GLSL version exposed by the driver	(ONLY QUERYABLE)

			void *detail;										///platform dependent hidden detail
		};

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Input Properties
		struct InputProperties {
			bool sticky_keys = false;					///< are sticky keys enabled?
			bool sticky_mouse = false;				///< is sticky mouse enabled?
			/// Cursor Type
			enum class CursorType : int {
				ARROW = 0x00036001,					///< The regular arrow cursor
				IBEAM = 0x00036002,					///< The text input I-beam cursor shape
				CROSSHAIR = 0x00036003,			///< The crosshair shape
				HAND = 0x00036004,						///< The hand shape
				HRESIZE = 0x00036005,				///< The horizontal resize arrow shape
				VRESIZE = 0x00036006					///< The horizontal resize arrow shape
			};
			CursorType cursor_type = CursorType::ARROW;	///< what cursor type is used
			bool cursor_visible = true;					///< is the cursor visible or hidden?
			bool cursor_enabled = true;				///< an enabled cursor works like any normal cursor, a disabled cursor is repositioned in the middle of the window after each mouse movement (useful for FPS cameras)
		};

		namespace detail {
			struct WindowBlockingCommand;
		}
		
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Window
		class Window {
			friend class WICSystem;
		public:
			/// creates a window and registers it to the WICSystem. 
			/// NOTE: This function requires the existence of a WICSystem (asserts otherwise) throughout its entire existence.
			/// NOTE: if an OpenGL context was requested it will binded on the calling thread.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param windowprop		describes the required window, it is taken as a HINT, thus if parameters are incompatible/unavailable defaults will be used
			/// @param framebufferprop	describes the required framebuffer, it is taken as a HINT, thus if parameters are incompatible/unavailable defaults will be used
			/// @param contextprop		describes the required OpenGL context, it is taken as a HINT, thus if parameters are incompatible/unavailable defaults will be used
			/// @param inputprop		describes the required input details, it is taken as a HINT, thus if parameters are incompatible/unavailable defaults will be used
			/// @param monitor			[optional] the monitor on which to create the window, by default the function uses is the primary monitory
			/// @param shared_window	[optional] another window, with which to share the OpenGL context data (VBOs, Textures, etc), by default none
			Window(const WindowProperties& windowprop, const FramebufferProperties& framebufferprop, const ContextProperties& contextprop, const InputProperties& inputprop, const Monitor* monitor = nullptr, const Window* shared_window = nullptr);
			Window() = delete;
			Window(const Window&) = delete;
			Window& operator=(const Window&) = delete;
			Window(Window&&) = default;
			Window& operator=(Window&&) = default;
			/// destroys the Window object and the system window (if one still exists and was not destroyed by event)
			/// NOTE: if the system window is alive and this function is called from outside the main thread, then this call finishes ONLY after main thread has executed 
			///		  WICSystem.processProgramEvents() or during WICSystem destruction.
			~Window();

			/// is the window still opened?
			/// @return window state
			bool isOpened() const;
			/// returns the window properties
			/// @return window properties
			const WindowProperties& getWindowProperties() const;
			/// returns the framebuffer properties
			/// @return framebuffer properties
			const FramebufferProperties& getFramebufferProperties() const;
			/// returns the context properties
			/// @return context properties
			const ContextProperties& getContextProperties() const;
			/// returns the input properties
			/// @return input properties
			const InputProperties& getInputProperties() const;
			/// returns the input remapper
			/// @return the input remapper
			const InputRemapper& getInputRemapper() const;
			/// returns the input remapper
			/// @return the input remapper
			InputRemapper& getInputRemapper();
			/// returns the input state
			/// @return input state
			const InputState& getInputState() const;
			/// returns the monitor on which this window is displayed
			/// @return the monitor on which this window is displayed
			const Monitor* getMonitor() const;


			/// bind the context to the current thread and load the OpenGL function pointers specific to this context
			/// NOTE: this function can only be called on an opened window with an OpenGL context (see ContextProperties). Asserts otherwise.
			/// NOTE: this function is necessary for OpenGL rendering & processing.
			/// NOTE: This operation is EXPENSIVE for rendering purposes.
			///       - it introduces a pipeline flush (OpenGL is serial!)
			///		  - it switches the entire OpenGL context
			///		  - because of the context differences (pixel format, requested version, profile, debugging data, ICD, etc) this function
			///		  changes (not loads) all the glFUNCNAME function pointers. While this is faster than an extension loading lib init call
			///		  it still has a cost.
			///		  As the OpenGL function pointers vary per context (different version, debug, etc), bindContext is intended to be used 
			///		  each time before performing OpenGL work, per OpenGL using thread. Thus it is inefficient to have more than one OpenGL
			///		  thread per application (of course, there are exceptions)
			/// NOTE: this function does NOT assure OpenGL synchronization, this is done through the on the GPU through the OpenGL api (glFence, etc) 
			///       and on the CPU through standard synchronization mechanisms
			void bindContext();
			/// unbinds the context associated with this window from the current thread
			/// NOTE: this MUST be used if sharing the context between multiple threads (OBS: this is generally a bad idea)
			/// NOTE: this function does NOT assure OpenGL synchronization, this is done through the on the GPU through the OpenGL api (glFence, etc) 
			///       and on the CPU through standard synchronization mechanisms
			void unbindContext();
			/// sets the vertical synchronization duration
			/// also known as vsync or vertical retrace, it represents the minimum number of frames to be displayed before swap buffers. 
			/// NOTE: this function can only be called on an opened window with an OpenGL context (see ContextProperties). The context has to be binded to the calling thread. Asserts otherwise.
			/// Note: some GPU drivers do not honor the requested swap interval.
			/// @param interval	 the vertical synchronization minimum number of frames
			///			This value will be given to the platform dependent OpenGL api (wglSwapIntervalEXT, glXSwapIntervalEXT)
			///			Some useful values:
			///			0  - never performs vsync
			///			1  - always perform vsync : swap buffers at window refresh rate (e.g. 60FPS for a 60Hz video mode)
			///			this can lead to poor performance when used on frames longer than the refresh rate, e.g. a 18ms frame would last 32ms on 60 Hz mode.
			///			-1 - perfrom vsync adaptively : swap buffers at window refresh rate only if the frame duration is shorter than the refresh rate, requires hardware support.
			/// NOTE: this function does NOT assure OpenGL synchronization, this is done through the on the GPU through the OpenGL api (glFence, etc) 
			///       and on the CPU through standard synchronization mechanisms
			void setSwapInterval(int interval);
			/// swaps the buffers. 
			/// NOTE: this function can only be called on an opened window with an OpenGL context (see ContextProperties) and a doublebuffered framebuffer. The context has to be binded to the calling thread. Asserts otherwise.
			/// NOTE: this function does NOT assure OpenGL synchronization, this is done through the on the GPU through the OpenGL api (glFence, etc) 
			///       and on the CPU through standard synchronization mechanisms
			void swapBuffers();


			/// process all existing events by calling the registered on-event callbacks in order
			void processEvents();
			/// process a single event
			/// @return	the next event or nullptr if there are no more events
			/// NOTE: this function is useful for processing all the events with knowledge of their order, all in single function 
			/// E.G.:
			/// while(const Event* e=processEvent()){
			///		switch(e->type){
			///			//event types
			///		}
			/// }
			const Event* processEvent();

			/// go into fullscreen mode
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: this function can be used to migrate the window from the current monitor to another monitor
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param mode		the fullscreen mode (resolution, refresh rate). If mode is nullptr the current monitor mode from the currently set monitor is used.
			/// @param monitor	migrate to this monitor. If monitor is nullptr or the current monitor no action is taken.
			void setFullscreen(const MonitorMode* mode = nullptr, const Monitor* monitor = nullptr);
			/// go into windowed mode
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: this function positions the window in client window space and can be used to migrate the window from the current monitor to another monitor
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param width	the width of the window
			/// @param height	the width of the height
			/// @param position_x	the position on the x axis in monitor space (starting in upper left corner)
			/// @param position_y	the position on the y axis in monitor space (starting in upper left corner)
			/// @param monitor	migrate to this monitor. If monitor is nullptr or the current monitor no action is taken.
			void setWindowed(unsigned int width, unsigned int height, unsigned int position_x, unsigned int position_y, const Monitor* monitor = nullptr);
			/// set the size of the window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: for fullscreen windows this function only updates the desired video mode to the closest supported mode. It does not affect the window context or the framebuffer bit depths.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param width  the width of the window
			/// @param height the height of the window
			void setSize(unsigned int width, unsigned int height);
			/// positions the window relative to the currently 
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: for fullscreen windows this function only updates the desired video mode to the closest supported mode. It does not affect the window context or the framebuffer bit depths.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param position_x the position of the window on the x axis
			/// @param position_y the position of the window on the y axis
			void setPosition(unsigned int position_x, unsigned int position_y);
			/// sets the title of the window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param title the new title of the window
			void setTitle(const std::string& title);
			/// sets the input mode
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// @param cursor_visible	is the cursor visible?
			/// @param cursor_enabled	is the cursor enabled?
			/// @param cursor_type		what is the cursor type?
			/// @param sticky_keys		are the keys sticky?
			/// @param sticky_mouse		is the mouse sticky?
			void setInputProperties(bool cursor_visible, bool cursor_enabled, InputProperties::CursorType& cursor_type, bool sticky_keys, bool sticky_mouse);
			/// sets the input remapper
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			void setInputRemapper(const InputRemapper& remapper);
			/// sets the clipboard string
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			void setClipboardString(const std::string& text);


			/// minimizes/iconifies this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void minimize();
			/// restores this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void restore();
			/// maximizes this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			/// NOTE : ignored for fullscreen windows
			void maximize();
			/// focuses this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE : stealing focus from other windows might lead to problems on some platforms. Use this function ONLY if necessary.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void focus();
			/// hides this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void hide();
			/// shows this window
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void show();
			/// closes this window
			/// NOTE: this function calls the close callback
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: if used from outside the main thread this call finishes ONLY after the main thread has executed WICSystem.processProgramEvents()
			void close();
			

			/// set the callback for the monitor connect event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param monitor		the connected monitor
			/// @param timestamp	the duration since start of program
			void setCallbackMonitorConnect(const std::function<void(const Monitor& monitor, uint64_t timestamp)>& callback);
			/// set the callback for the monitor disconnect event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param monitor		the disconnected monitor
			/// @param timestamp	the duration since start of program
			void setCallbackMonitorDisconnect(const std::function<void(const Monitor& monitor, uint64_t timestamp)>& callback);
			/// set the callback for the controller connect event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param controllerid	the connected controller id
			/// @param timestamp	the duration since start of program
			void setCallbackControllerConnect(const std::function<void(unsigned int controllerid, uint64_t timestamp)>& callback);
			/// set the callback for the controller disconnect event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param controllerid	the disconnected controller id
			/// @param timestamp	the duration since start of program
			void setCallbackControllerDisconnect(const std::function<void(unsigned int controllerid, uint64_t timestamp)>& callback);
			/// set the callback for the controller button press event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param controllerid	the controller id
			/// @param button		the pressed button
			/// @param timestamp	the duration since start of program
			void setCallbackControllerButtonPress(const std::function<void(unsigned int controllerid, ControllerButton button, uint64_t timestamp)>& callback);
			/// set the callback for the controller button release event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param controllerid	the controller id
			/// @param button		the released button
			/// @param timestamp	the duration since start of program
			void setCallbackControllerButtonRelease(const std::function<void(unsigned int controllerid, ControllerButton button, uint64_t timestamp)>& callback);
			/// set the callback for the controller axis move event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param controllerid	the controller id
			/// @param axes			the controller axes
			/// @param timestamp	the duration since start of program
			void setCallbackControllerAxisMove(const std::function<void(unsigned int controllerid, float (&axes)[6], uint64_t timestamp)>& callback);
			/// sets the callback for framebuffer resizing.
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE: this callback can produce different results from the WindowResize callback/event 
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param width		the new width of the framebuffer
			/// @param height		the new height of the framebuffer
			/// @param timestamp	the duration since start of program
			void setCallbackFramebufferResize(const std::function<void(const Window& caller, unsigned int width, unsigned int height, uint64_t timestamp)>& callback);
			/// set the callback for the window close event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// NOTE : this function is called just before closing the system window. Immediatly after the callback is executed this Window object will then have its state updated (WindowProperties().opened = false, width=height=0, no context, etc)
			/// The lifetime of this Window object is controlled by the programmer, but the system window is permanently destroyed.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowClose(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window move event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param position_x	the new position on the x axis of the window in monitor space (starting upper left)
			/// @param position_y	the new position on the y axis of the window in monitor space (starting upper left)
			/// @param timestamp	the duration since start of program
			void setCallbackWindowMove(const std::function<void(const Window& caller, unsigned int position_x, unsigned int position_y, uint64_t timestamp)>& callback);
			/// set the callback for the window resize event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param width		the new width of the window
			/// @param height		the new height of the window
			/// @param timestamp	the duration since start of program
			void setCallbackWindowResize(const std::function<void(const Window& caller, unsigned int width, unsigned int height, uint64_t timestamp)>& callback);
			/// set the callback for the window minimize event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowMinimize(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window maximize event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowMaximize(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window restore event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowRestore(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window focus gain event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowFocusGain(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window focus lose event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowFocusLose(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window refresh event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackWindowRefresh(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the window file drop event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param paths		a vector which contains the paths of the dropped files
			/// @param timestamp	the duration since start of program
			void setCallbackFilesDrop(const std::function<void(const Window& caller, const std::vector<std::string>& paths, uint64_t timestamp)>& callback);

			/// set the callback for the key press event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param key			the pressed key
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackKeyPress(const std::function<void(const Window& caller, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the key release event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param key			the released key
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackKeyRelease(const std::function<void(const Window& caller, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the key release event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param key			the repeated key
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackKeyRepeat(const std::function<void(const Window& caller, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the unicode char event, without modifiers
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param codepoint	the unicode codepoint (one or more keys entered to produce one)
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			/// NOTE: the character callback behaves as system text input normally does and will not be called if modifier keys are held down that would prevent normal text input on that platform, for example a Super (Command) key on OS X or Alt key on Windows, InputState contains the entire state of the input
			void setCallbackUnicodeChar(const std::function<void(const Window& caller, unsigned int codepoint, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the unicode char event, with modifiers
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param codepoint	the unicode codepoint (one or more keys entered to produce one)
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			/// NOTE: the character callback behaves as system text input normally does and IS called if modifier keys are held down that would prevent normal text input on that platform, for example a Super (Command) key on OS X or Alt key on Windows
			void setCallbackUnicodeCharMods(const std::function<void(const Window& caller, unsigned int codepoint, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the mouse move event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param position_x	the mouse position x in screen space (starts upper left)
			/// @param position_y	the mouse position y in screen space (starts upper left)
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackMouseMove(const std::function<void(const Window& caller, unsigned int position_x, unsigned int position_y, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the mouse enter window event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackMouseEnter(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the mouse leave window event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param timestamp	the duration since start of program
			void setCallbackMouseLeave(const std::function<void(const Window& caller, uint64_t timestamp)>& callback);
			/// set the callback for the mouse button press event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param buttton		the pressed mouse button
			/// @param position_x	the mouse position x in screen space (starts upper left)
			/// @param position_y	the mouse position y in screen space (starts upper left)
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackMousePress(const std::function<void(const Window& caller, MouseButton button, unsigned int position_x, unsigned int position_y, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the mouse button release event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param buttton		the released mouse button
			/// @param position_x	the mouse position x in screen space (starts upper left)
			/// @param position_y	the mouse position y in screen space (starts upper left)
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackMouseRelease(const std::function<void(const Window& caller, MouseButton button, unsigned int position_x, unsigned int position_y, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the mouse button drag event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param buttton		the pressed mouse button
			/// @param position_x	the mouse position x in screen space (starts upper left)
			/// @param position_y	the mouse position y in screen space (starts upper left)
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackMouseDrag(const std::function<void(const Window& caller, MouseButton button, unsigned int position_x, unsigned int position_y, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
			/// set the callback for the mouse scroll event
			/// NOTE: this function can only be called on an opened window. Asserts otherwise.
			/// @param callback		the event callback
			/// @param window		the caller window (when the callback will be executed it will be a pointer to this object). Useful for distinguishing between different windows.
			/// @param scroll_x		the amount of scroll on the x axis (horizontal scrolling)
			/// @param scroll_y		the amount of scroll on the y axis (vertical scrolling, NOTE: MOST MICE HAVE VERTICAL SCROLLING)
			/// @param position_x	the mouse position x in screen space (starts upper left)
			/// @param position_y	the mouse position y in screen space (starts upper left)
			/// @param alt			is the alt key down?
			/// @param control		is the control key down?
			/// @param shift		is the shift key down?
			/// @param system		is the system key down?
			/// @param state		a const reference to the current input state
			/// @param timestamp	the duration since start of program
			void setCallbackMouseScroll(const std::function<void(const Window& caller, float scroll_x, float scroll_y, unsigned int position_x, unsigned int position_y, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp)>& callback);
		
			/// returns true if the OpenGL extension is supported
			/// @param name the name of the queried OpenGL extension (e.g. name = GL_arb_sparse_buffer)
			/// @return true if the OpenGL extension is supported
			bool isOpenGLExtensionSupported(const std::string& name);
			/// returns address of an OpenGL extension function
			/// because of platform dependent OpenGL driver implementation designs the result is valid only on a per-context level (this window's context)
			/// thus if multiple contexts are to be used the result should be managed/cached per context. Causes of this behavior: 
			/// - multiple GPUs have different function signatures, even if they both support an extension/function.
			/// - some platforms associate a function pointer with the context on which it was created, using the function pointer outside of the
			///   scope of the parent context can produce undefined results (depends on platform and OS)
			/// - some platforms use multiple different libraries from which function signatures for the same context are loaded
			/// @param funcname the name of the queried OpenGL extension (e.g. funcname = glBufferPageCommitmentARB)
			/// @return the address of the OpenGL extension function
			void(*loadOpenGLExtensionFunction(const std::string& funcname))(void);
		private:
			/// called internally on window destroy (cleans all entries, callbacks, etc)
			void internalCleanup();
		private:
			WindowProperties window_properties;
			FramebufferProperties framebuffer_properties;
			ContextProperties context_properties;
			InputProperties input_properties;
			InputRemapper input_remapper;
			InputState input_state;
			
			std::vector<Event> events;
			unsigned int event_current;

			const Monitor *monitor;
			void* handle_monitor;
			void* handle_window;
			void* handle_window_shared;

			std::function<void(unsigned int, uint64_t)> callback_controller_connect;
			std::function<void(unsigned int, uint64_t)> callback_controller_disconnect;
			std::function<void(unsigned int, ControllerButton, uint64_t)> callback_controller_button_press;
			std::function<void(unsigned int, ControllerButton, uint64_t)> callback_controller_button_release;
			std::function<void(unsigned int, float (&)[6], uint64_t)> callback_controller_axes_move;
			std::function<void(const Monitor&, uint64_t)> callback_monitor_connect;
			std::function<void(const Monitor&, uint64_t)> callback_monitor_disconnect;
			std::function<void(const Window& caller, const std::vector<std::string>&, uint64_t)> callback_window_filesdrop;
			std::function<void(const Window& caller, uint64_t)> callback_window_close;
			std::function<void(const Window& caller, unsigned int, unsigned int, uint64_t)> callback_window_move;
			std::function<void(const Window& caller, unsigned int, unsigned int, uint64_t)> callback_window_resize;
			std::function<void(const Window& caller, uint64_t)> callback_window_minimize;
			std::function<void(const Window& caller, uint64_t)> callback_window_maximize;
			std::function<void(const Window& caller, uint64_t)> callback_window_restore;
			std::function<void(const Window& caller, uint64_t)> callback_window_focus_gain;
			std::function<void(const Window& caller, uint64_t)> callback_window_focus_lose;
			std::function<void(const Window& caller, uint64_t)> callback_window_refresh;
			std::function<void(const Window& caller, unsigned int, unsigned int, uint64_t)> callback_framebuffer_resize;
			std::function<void(const Window& caller, Key, bool, bool, bool, bool, const InputState&, uint64_t)> callback_key_press;
			std::function<void(const Window& caller, Key, bool, bool, bool, bool, const InputState&, uint64_t)> callback_key_release;
			std::function<void(const Window& caller, Key, bool, bool, bool, bool, const InputState&, uint64_t)> callback_key_repeat;
			std::function<void(const Window& caller, unsigned int, const InputState&, uint64_t)> callback_unicode_char;
			std::function<void(const Window& caller, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)> callback_unicode_char_mods;
			std::function<void(const Window& caller, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)> callback_mouse_press;
			std::function<void(const Window& caller, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)> callback_mouse_release;
			std::function<void(const Window& caller, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)> callback_mouse_drag;
			std::function<void(const Window& caller, unsigned int, unsigned int, const InputState&, uint64_t)> callback_mouse_move;
			std::function<void(const Window& caller, uint64_t)> callback_mouse_enter;
			std::function<void(const Window& caller, uint64_t)> callback_mouse_leave;
			std::function<void(const Window& caller, float, float, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)> callback_mouse_scroll;
		};
		
		namespace detail {
			class WICSystemState {
			public:
				struct WindowState {
					std::vector<Event> events;
					bool cursor_enabled = true;
					bool maximized = false;
					std::string title;
				};
			public:
				WICSystemState();
				~WICSystemState();
				void init(std::ostream* logger, bool multithreaded);
				void terminate();
				WICSystemState(const WICSystemState&) = delete;
				WICSystemState& operator=(const WICSystemState&) = delete;
				WICSystemState(WICSystemState&&) = delete;
				WICSystemState& operator=(WICSystemState&&) = delete;
				
			public:
				std::thread::id mainthread;
				std::mutex mutex;
				bool multithreaded;
				bool initialized;
				std::chrono::time_point<std::chrono::high_resolution_clock> timestamp_program;
				std::ostream* logger;
				std::map<void*, WindowState> windowstates;
				std::vector<Monitor*> monitors;
				std::list<detail::WindowBlockingCommand*> commands;

				struct {
					bool connected;
					float axes[6];
					bool buttons[(int)ControllerButton::MAX];
				}input_controller[(int)Controller::MAX];
			};
			void internalHandleCommand(detail::WindowBlockingCommand* command);
			void internalCommand(detail::WindowBlockingCommand* command);
			extern WICSystemState gstate;
		}



		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		/// Window Input Context System
		class WICSystem
		{
		public:
			/// creates a new WICSystem object, only one such object can exist, thus this is a singleton.
			/// NOTE: trying to create more than one WICSystem object will result in program termination
			/// THREADING RESTRICTION: THIS FUNCTION CAN ONLY BE CALLED FROM THE MAIN THREAD
			/// @param logger the logging stream used by the WICSystem
			/// @param multithreaded this manager will run in thread-safe mode. NOTE: this DOES NOT APPLY TO OPENGL, only to the windowing system.
			WICSystem(std::ostream* logger = nullptr, bool multithreaded = false);
			WICSystem(const WICSystem&) = delete;
			WICSystem& operator=(const WICSystem&) = delete;
			WICSystem(WICSystem&&) = delete;
			WICSystem& operator=(WICSystem&&) = delete;
			/// destroys the wic manager
			/// THREADING RESTRICTION: THIS FUNCTION CAN BE ONLY CALLED FROM THE MAIN THREAD
			~WICSystem();
			
			/// process WIC events for the entire program
			/// THREADING RESTRICTION: THIS FUNCTION CAN ONLY BE CALLED FROM THE MAIN THREAD
			/// e.g.: while(app){ ... other things ...  WIC::processProgramEvents(); }
			void processProgramEvents();

			/// returns true if any WIC window is still opened
			/// @return if any WIC window is opened
			bool isAnyWindowOpened();
			/// set a new logger for WICManager
			/// @param logger an optional ostream based logger (e.g. a fstream, &std::cout, etc). Logging is disabled if this parameter is nullptr
			void setLogger(std::ostream* logger = nullptr);
			/// returns the primary monitors
			/// @return the primary monitor
			const Monitor* getMonitorPrimary();
			/// returns all monitors
			/// @return vector with all monitors
			const std::vector<Monitor*>& getMonitors();

			/// returns the number of existent windows
			/// @return the number of existent windows
			unsigned int getWindowsOpened();


#ifndef LAP_WIC_SUPPRESS_VULKAN
			/// check the availability of a vulkan loader
			/// @return the availability of a vulkan loader
			/// NOTE: this is a direct wrapper over glfwVulkanSupported()
			bool vulkanIsSupported();

			/// returns the vulkan instance extensions, as required by lapwic
			/// @param count the number of required extensions or zero if vulkan is not supported
			/// @return the vulkan instance extensions required by lapwic, or nullptr if vulkan is not supported
			/// NOTE: "If successful, the list will always contains VK_KHR_surface, so if you don't require any additional extensions you can pass this list directly to the VkInstanceCreateInfo struct."
			/// NOTE: this is a direct wrapper over glfwGetRequiredInstanceExtensions()
			const char** vulkanGetRequiredInstanceExtensions(uint32_t* count);

			/// returns the address of a vulkan function for the specified instance or nullptr if vulkan is not available. 
			/// If no instance is selected this function can be used to return any function exported by the vulkan loader (functions related to instance creation)
			/// @param instance the VkInstance for which to get the function pointer or nullptr for instance creation functions
			/// @param procname the vulkan process/function name
			/// @return the function pointer of a vulkan function or nullptr if vulkan is not available
			/// NOTE: this is a direct wrapper over glfwGetInstanceProcAddress
			/// usage:
			/// PFN_vkCreateInstance pfnCreateInstance = (PFN_vkCreateInstance) vulkanGetInstanceProcAddress(nullptr, "vkCreateInstance");
			/// PFN_vkCreateDevice pfnCreateDevice = (PFN_vkCreateDevice) vulkanGetInstanceProcAddress(pfnCreateInstance, "vkCreateDevice");
			void(*vulkanGetInstanceProcAddress(VkInstance instance, const char* procname))();

			/// returns the support of the queue family to present images to surfaces
			/// @return the support of the queue family to present images to surfaces
			/// @param instance			the vulkan instance
			/// @param device			the vulkan device
			/// @param queuefamily		the queue family
			/// NOTE: this is a direct wrapper over glfwGetPhysicalDevicePresentationSupport
			bool vulkanGetPhysicalDevicePresentationSupport(VkInstance instance, VkPhysicalDevice device, uint32_t queuefamily);

			/// return a vulkan surface for a specified window. 
			/// NOTE : The surface is owned by the caller, and can be destroyed with vkDestroySurfaceKHR.
			/// @param  instance	the vulkan instance
			/// @param  window		the window to create a surface for
			/// @param  allocator	the vulkan allocator. nullptr uses the default allocator.
			/// @param  surface		the vulkan surface handle, will be set to VK_NULL_HANDLE on failure.
			/// @return a vulkan surface for a specified window (VK_SUCCESS on success or a vulkan error code on failure)
			VkResult vulkanCreateWindowSurface(VkInstance instance, Window* window, const VkAllocationCallbacks* allocator, VkSurfaceKHR* surface);
#endif
		};
	}
}


#endif