﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP GPUTimer
/// This is a gpu timer, written with OpenGL 3.3 commands. It is useful for fine timing GPU-commands. The timer uses a time
/// query pool to quickly issue time measurment gpu commands (OpenGL time queries). Because all GPUs work with command buffers
/// the timer queries are just time commands added between the commands in the command buffer. As all GPU measurement based
/// tools, the GPUtimer inserts a small number of query commands which should be read (timer. getXXXX()) at the end of the frame/computation
/// when all commands are most likely to be finished. Otherwise the get commands will WAIT until the query commands (and all the previous
/// GPU commands) are processed. 
/// If the user is performing complicated back and forth CPU-GPU streaming operations without proper usage of synchronization (glFinish/glFlush/barriers)
/// then the timings might propagate because of automated GPU transfer synchronization. 
/// 
/// @par A simplified view of timer queries on the GPU:
/// A generic GPU command buffer:
///
///		| cmd cmd cmd cmd cmd cmd ............ cmd cmd cmd cmd |
/// A generic GPU command buffer with queries inserted, each query has a number which correponds to its index in the query pool
///
///		| cmd cmd cmd q1 cmd q5 ............ cmd q2 cmd cmd q14|
/// The query commands are inserted between the original commands, and time is measured between the two queries, e.g. q1->q5.
///
/// @version 1.00
/// @par Properties:
///	- c++11, no exceptions
///	- OpenGL 3.3
/// - the OpenGL functions must be provided by the user, see line 90. This could have been done with extern functions but
///   some OpenGL extension loader libraries are based on macros. Furthermore macros are used by many hook-based OpenGL debug libraries,
///   therefore the current system was preferred.
///
/// @par Compiling and linking:
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///		- also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Usage:
///	- basic usage
///		@code
///		#include "lap_gpu_timer.hpp"
///		... initialize window with OpenGL context ...
///		lap::gpu::GPUTimer gputimer(3);
///		gputimer.insertQuery(2);		// insert query number 2 at the beginning
///		... do some GPU work here ...
///		gputimer.insertQuery(1);		// insert query number 1 here
///		... do some GPU work here ...
///		gputimer.insertQuery(0);		// insert query number 0 at the end
///
///		gputimer.waitQueries();			//wait for queries to finish (= commands before them to finish)
///		std::cout << "The gputimer query pool has " << gputimer.getPoolSize() << " entries." << std::endl;
///		std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(2, 1) << "ms between 2 and 1." << std::endl;
///		std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(1, 0) << "ms between 1 and 0." << std::endl;
///		@endcode
///
///------------------------------------------------------------------------------------------------

#ifndef LAP_GPU_TIMER_H_
#define LAP_GPU_TIMER_H_

///------------------------------------------------------------------------------------------------
/// IMPORTANT: modify this line to include the desired OpenGL function header
/// NOTE: not using extern functions as some extension loaders use macros
#include "lap_wic.hpp"
///------------------------------------------------------------------------------------------------


namespace lap {
	namespace gpu {

		class GPUTimer{
		public:
			/// creates a GPU timer with a pool of "count" gpu timer query
			/// @param count the size of the gpu timer query pool.
			GPUTimer(unsigned int count);
			GPUTimer(const GPUTimer&) = delete;
			GPUTimer(GPUTimer&&) = delete;
			GPUTimer& operator=(const GPUTimer&) = delete;
			GPUTimer& operator=(GPUTimer&&) = delete;
			~GPUTimer();

			///------------------------------------------------------------------------------------
			/// issues a gpu timer query in the gpu command buffer. The query is stored index-th entry of the query pool.
			/// @param index	the index of the query (in the query pool)
			void insertQuery(unsigned int index);

			///------------------------------------------------------------------------------------
			/// returns the size of the query pool
			/// @return the size of the query pool
			unsigned int getPoolSize();
			/// returns the state of the query, if it is finished or still pending
			/// @param index	the index of the query (in the query pool)
			/// @return the state of the query
			bool isQueryFinished(unsigned int index);

			/// the function blocks and waits for the result of the GPU query to be available. The result is available only after all the GPU commands issued before 
			/// the query have been processed. After the result is available the function returns the gpu time when the query command was executed by the GPU.
			/// @param index	the index of the query (in the query pool)
			/// @return the gpu time when the query command was executed by the GPU
			double getTimeInMillisecondsAfterWaitingQuery(unsigned int index);
			/// the function blocks and waits for the results of the GPU queries to be available. The results are available only after all the GPU commands issued before both queries 
			/// have been processed. After the results are available the function returns the duration between the GPU execution of the two queries.
			/// @param index1	the index of the first query (in the query pool)
			/// @param index2	the index of the second query (in the query pool)
			/// @return the duration between the GPU execution of the two queries.
			double getTimeInMillisecondsAfterWaitingQueries(unsigned int index1, unsigned int index2);

		private:
			std::vector<unsigned int> queries;
		};
		
	}
}


#endif