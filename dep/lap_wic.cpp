﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------




//GLFW+GLAD implementation
#define LAP_WIC_DEP_IMPLEMENTATION
#include "lap_wic_dep.hpp"
//lap wic and the rest of GLAD
#define LAP_WIC_INTERNAL_GUARD
#include "lap_wic.hpp"

//others
#include <condition_variable>
#include <cassert>
#include <cmath>

// force enable the highest performance driver for both NVIDIA and AMD (where/if this api is exposed .. )
// in comparison to DirectX this is a mess, OpenGL would greatly benefit from a better api
// http://stackoverflow.com/questions/17458803/amd-equivalent-to-nvoptimusenablement
// http://stackoverflow.com/questions/6036292/select-a-graphic-device-in-windows-opengl
#ifdef GLFW_WINDOWS
extern "C" {
	__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}
#endif


namespace lap{
	namespace wic {

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		// this structure contains pointers to all the functions exposed by the IDC (driver implementation of OpenGL 4.5)
		// the per-context function pointers are not directly exposed as interface in the .h file because the user knows what he is asking for, 
		// and has direct access through glFUNCNAME to the function pointers
		// context switches REQUIRE synchronization
		class GLFunctionPointers{
		public:
			GLFunctionPointers() = default;
			GLFunctionPointers(const GLFunctionPointers&) = default;
			GLFunctionPointers(GLFunctionPointers&&) = default;
			GLFunctionPointers& operator=(const GLFunctionPointers&) = default;
			GLFunctionPointers& operator=(GLFunctionPointers&&) = default;
			~GLFunctionPointers() = default;

			//make all opengl function pointers current (called on context switch)
			void updateCurrent() {
				//v10
				glCullFace = v10.glCullFace;
				glFrontFace = v10.glFrontFace;
				glHint = v10.glHint;
				glLineWidth = v10.glLineWidth;
				glPointSize = v10.glPointSize;
				glPolygonMode = v10.glPolygonMode;
				glScissor = v10.glScissor;
				glTexParameterf = v10.glTexParameterf;
				glTexParameterfv = v10.glTexParameterfv;
				glTexParameteri = v10.glTexParameteri;
				glTexParameteriv = v10.glTexParameteriv;
				glTexImage1D = v10.glTexImage1D;
				glTexImage2D = v10.glTexImage2D;
				glDrawBuffer = v10.glDrawBuffer;
				glClear = v10.glClear;
				glClearColor = v10.glClearColor;
				glClearStencil = v10.glClearStencil;
				glClearDepth = v10.glClearDepth;
				glStencilMask = v10.glStencilMask;
				glColorMask = v10.glColorMask;
				glDepthMask = v10.glDepthMask;
				glDisable = v10.glDisable;
				glEnable = v10.glEnable;
				glFinish = v10.glFinish;
				glFlush = v10.glFlush;
				glBlendFunc = v10.glBlendFunc;
				glLogicOp = v10.glLogicOp;
				glStencilFunc = v10.glStencilFunc;
				glStencilOp = v10.glStencilOp;
				glDepthFunc = v10.glDepthFunc;
				glPixelStoref = v10.glPixelStoref;
				glPixelStorei = v10.glPixelStorei;
				glReadBuffer = v10.glReadBuffer;
				glReadPixels = v10.glReadPixels;
				glGetBooleanv = v10.glGetBooleanv;
				glGetDoublev = v10.glGetDoublev;
				glGetError = v10.glGetError;
				glGetFloatv = v10.glGetFloatv;
				glGetIntegerv = v10.glGetIntegerv;
				glGetString = v10.glGetString;
				glGetTexImage = v10.glGetTexImage;
				glGetTexParameterfv = v10.glGetTexParameterfv;
				glGetTexParameteriv = v10.glGetTexParameteriv;
				glGetTexLevelParameterfv = v10.glGetTexLevelParameterfv;
				glGetTexLevelParameteriv = v10.glGetTexLevelParameteriv;
				glIsEnabled = v10.glIsEnabled;
				glDepthRange = v10.glDepthRange;
				glViewport = v10.glViewport;
				glNewList = v10.glNewList;
				glEndList = v10.glEndList;
				glCallList = v10.glCallList;
				glCallLists = v10.glCallLists;
				glDeleteLists = v10.glDeleteLists;
				glGenLists = v10.glGenLists;
				glListBase = v10.glListBase;
				glBegin = v10.glBegin;
				glBitmap = v10.glBitmap;
				glColor3b = v10.glColor3b;
				glColor3bv = v10.glColor3bv;
				glColor3d = v10.glColor3d;
				glColor3dv = v10.glColor3dv;
				glColor3f = v10.glColor3f;
				glColor3fv = v10.glColor3fv;
				glColor3i = v10.glColor3i;
				glColor3iv = v10.glColor3iv;
				glColor3s = v10.glColor3s;
				glColor3sv = v10.glColor3sv;
				glColor3ub = v10.glColor3ub;
				glColor3ubv = v10.glColor3ubv;
				glColor3ui = v10.glColor3ui;
				glColor3uiv = v10.glColor3uiv;
				glColor3us = v10.glColor3us;
				glColor3usv = v10.glColor3usv;
				glColor4b = v10.glColor4b;
				glColor4bv = v10.glColor4bv;
				glColor4d = v10.glColor4d;
				glColor4dv = v10.glColor4dv;
				glColor4f = v10.glColor4f;
				glColor4fv = v10.glColor4fv;
				glColor4i = v10.glColor4i;
				glColor4iv = v10.glColor4iv;
				glColor4s = v10.glColor4s;
				glColor4sv = v10.glColor4sv;
				glColor4ub = v10.glColor4ub;
				glColor4ubv = v10.glColor4ubv;
				glColor4ui = v10.glColor4ui;
				glColor4uiv = v10.glColor4uiv;
				glColor4us = v10.glColor4us;
				glColor4usv = v10.glColor4usv;
				glEdgeFlag = v10.glEdgeFlag;
				glEdgeFlagv = v10.glEdgeFlagv;
				glEnd = v10.glEnd;
				glIndexd = v10.glIndexd;
				glIndexdv = v10.glIndexdv;
				glIndexf = v10.glIndexf;
				glIndexfv = v10.glIndexfv;
				glIndexi = v10.glIndexi;
				glIndexiv = v10.glIndexiv;
				glIndexs = v10.glIndexs;
				glIndexsv = v10.glIndexsv;
				glNormal3b = v10.glNormal3b;
				glNormal3bv = v10.glNormal3bv;
				glNormal3d = v10.glNormal3d;
				glNormal3dv = v10.glNormal3dv;
				glNormal3f = v10.glNormal3f;
				glNormal3fv = v10.glNormal3fv;
				glNormal3i = v10.glNormal3i;
				glNormal3iv = v10.glNormal3iv;
				glNormal3s = v10.glNormal3s;
				glNormal3sv = v10.glNormal3sv;
				glRasterPos2d = v10.glRasterPos2d;
				glRasterPos2dv = v10.glRasterPos2dv;
				glRasterPos2f = v10.glRasterPos2f;
				glRasterPos2fv = v10.glRasterPos2fv;
				glRasterPos2i = v10.glRasterPos2i;
				glRasterPos2iv = v10.glRasterPos2iv;
				glRasterPos2s = v10.glRasterPos2s;
				glRasterPos2sv = v10.glRasterPos2sv;
				glRasterPos3d = v10.glRasterPos3d;
				glRasterPos3dv = v10.glRasterPos3dv;
				glRasterPos3f = v10.glRasterPos3f;
				glRasterPos3fv = v10.glRasterPos3fv;
				glRasterPos3i = v10.glRasterPos3i;
				glRasterPos3iv = v10.glRasterPos3iv;
				glRasterPos3s = v10.glRasterPos3s;
				glRasterPos3sv = v10.glRasterPos3sv;
				glRasterPos4d = v10.glRasterPos4d;
				glRasterPos4dv = v10.glRasterPos4dv;
				glRasterPos4f = v10.glRasterPos4f;
				glRasterPos4fv = v10.glRasterPos4fv;
				glRasterPos4i = v10.glRasterPos4i;
				glRasterPos4iv = v10.glRasterPos4iv;
				glRasterPos4s = v10.glRasterPos4s;
				glRasterPos4sv = v10.glRasterPos4sv;
				glRectd = v10.glRectd;
				glRectdv = v10.glRectdv;
				glRectf = v10.glRectf;
				glRectfv = v10.glRectfv;
				glRecti = v10.glRecti;
				glRectiv = v10.glRectiv;
				glRects = v10.glRects;
				glRectsv = v10.glRectsv;
				glTexCoord1d = v10.glTexCoord1d;
				glTexCoord1dv = v10.glTexCoord1dv;
				glTexCoord1f = v10.glTexCoord1f;
				glTexCoord1fv = v10.glTexCoord1fv;
				glTexCoord1i = v10.glTexCoord1i;
				glTexCoord1iv = v10.glTexCoord1iv;
				glTexCoord1s = v10.glTexCoord1s;
				glTexCoord1sv = v10.glTexCoord1sv;
				glTexCoord2d = v10.glTexCoord2d;
				glTexCoord2dv = v10.glTexCoord2dv;
				glTexCoord2f = v10.glTexCoord2f;
				glTexCoord2fv = v10.glTexCoord2fv;
				glTexCoord2i = v10.glTexCoord2i;
				glTexCoord2iv = v10.glTexCoord2iv;
				glTexCoord2s = v10.glTexCoord2s;
				glTexCoord2sv = v10.glTexCoord2sv;
				glTexCoord3d = v10.glTexCoord3d;
				glTexCoord3dv = v10.glTexCoord3dv;
				glTexCoord3f = v10.glTexCoord3f;
				glTexCoord3fv = v10.glTexCoord3fv;
				glTexCoord3i = v10.glTexCoord3i;
				glTexCoord3iv = v10.glTexCoord3iv;
				glTexCoord3s = v10.glTexCoord3s;
				glTexCoord3sv = v10.glTexCoord3sv;
				glTexCoord4d = v10.glTexCoord4d;
				glTexCoord4dv = v10.glTexCoord4dv;
				glTexCoord4f = v10.glTexCoord4f;
				glTexCoord4fv = v10.glTexCoord4fv;
				glTexCoord4i = v10.glTexCoord4i;
				glTexCoord4iv = v10.glTexCoord4iv;
				glTexCoord4s = v10.glTexCoord4s;
				glTexCoord4sv = v10.glTexCoord4sv;
				glVertex2d = v10.glVertex2d;
				glVertex2dv = v10.glVertex2dv;
				glVertex2f = v10.glVertex2f;
				glVertex2fv = v10.glVertex2fv;
				glVertex2i = v10.glVertex2i;
				glVertex2iv = v10.glVertex2iv;
				glVertex2s = v10.glVertex2s;
				glVertex2sv = v10.glVertex2sv;
				glVertex3d = v10.glVertex3d;
				glVertex3dv = v10.glVertex3dv;
				glVertex3f = v10.glVertex3f;
				glVertex3fv = v10.glVertex3fv;
				glVertex3i = v10.glVertex3i;
				glVertex3iv = v10.glVertex3iv;
				glVertex3s = v10.glVertex3s;
				glVertex3sv = v10.glVertex3sv;
				glVertex4d = v10.glVertex4d;
				glVertex4dv = v10.glVertex4dv;
				glVertex4f = v10.glVertex4f;
				glVertex4fv = v10.glVertex4fv;
				glVertex4i = v10.glVertex4i;
				glVertex4iv = v10.glVertex4iv;
				glVertex4s = v10.glVertex4s;
				glVertex4sv = v10.glVertex4sv;
				glClipPlane = v10.glClipPlane;
				glColorMaterial = v10.glColorMaterial;
				glFogf = v10.glFogf;
				glFogfv = v10.glFogfv;
				glFogi = v10.glFogi;
				glFogiv = v10.glFogiv;
				glLightf = v10.glLightf;
				glLightfv = v10.glLightfv;
				glLighti = v10.glLighti;
				glLightiv = v10.glLightiv;
				glLightModelf = v10.glLightModelf;
				glLightModelfv = v10.glLightModelfv;
				glLightModeli = v10.glLightModeli;
				glLightModeliv = v10.glLightModeliv;
				glLineStipple = v10.glLineStipple;
				glMaterialf = v10.glMaterialf;
				glMaterialfv = v10.glMaterialfv;
				glMateriali = v10.glMateriali;
				glMaterialiv = v10.glMaterialiv;
				glPolygonStipple = v10.glPolygonStipple;
				glShadeModel = v10.glShadeModel;
				glTexEnvf = v10.glTexEnvf;
				glTexEnvfv = v10.glTexEnvfv;
				glTexEnvi = v10.glTexEnvi;
				glTexEnviv = v10.glTexEnviv;
				glTexGend = v10.glTexGend;
				glTexGendv = v10.glTexGendv;
				glTexGenf = v10.glTexGenf;
				glTexGenfv = v10.glTexGenfv;
				glTexGeni = v10.glTexGeni;
				glTexGeniv = v10.glTexGeniv;
				glFeedbackBuffer = v10.glFeedbackBuffer;
				glSelectBuffer = v10.glSelectBuffer;
				glRenderMode = v10.glRenderMode;
				glInitNames = v10.glInitNames;
				glLoadName = v10.glLoadName;
				glPassThrough = v10.glPassThrough;
				glPopName = v10.glPopName;
				glPushName = v10.glPushName;
				glClearAccum = v10.glClearAccum;
				glClearIndex = v10.glClearIndex;
				glIndexMask = v10.glIndexMask;
				glAccum = v10.glAccum;
				glPopAttrib = v10.glPopAttrib;
				glPushAttrib = v10.glPushAttrib;
				glMap1d = v10.glMap1d;
				glMap1f = v10.glMap1f;
				glMap2d = v10.glMap2d;
				glMap2f = v10.glMap2f;
				glMapGrid1d = v10.glMapGrid1d;
				glMapGrid1f = v10.glMapGrid1f;
				glMapGrid2d = v10.glMapGrid2d;
				glMapGrid2f = v10.glMapGrid2f;
				glEvalCoord1d = v10.glEvalCoord1d;
				glEvalCoord1dv = v10.glEvalCoord1dv;
				glEvalCoord1f = v10.glEvalCoord1f;
				glEvalCoord1fv = v10.glEvalCoord1fv;
				glEvalCoord2d = v10.glEvalCoord2d;
				glEvalCoord2dv = v10.glEvalCoord2dv;
				glEvalCoord2f = v10.glEvalCoord2f;
				glEvalCoord2fv = v10.glEvalCoord2fv;
				glEvalMesh1 = v10.glEvalMesh1;
				glEvalPoint1 = v10.glEvalPoint1;
				glEvalMesh2 = v10.glEvalMesh2;
				glEvalPoint2 = v10.glEvalPoint2;
				glAlphaFunc = v10.glAlphaFunc;
				glPixelZoom = v10.glPixelZoom;
				glPixelTransferf = v10.glPixelTransferf;
				glPixelTransferi = v10.glPixelTransferi;
				glPixelMapfv = v10.glPixelMapfv;
				glPixelMapuiv = v10.glPixelMapuiv;
				glPixelMapusv = v10.glPixelMapusv;
				glCopyPixels = v10.glCopyPixels;
				glDrawPixels = v10.glDrawPixels;
				glGetClipPlane = v10.glGetClipPlane;
				glGetLightfv = v10.glGetLightfv;
				glGetLightiv = v10.glGetLightiv;
				glGetMapdv = v10.glGetMapdv;
				glGetMapfv = v10.glGetMapfv;
				glGetMapiv = v10.glGetMapiv;
				glGetMaterialfv = v10.glGetMaterialfv;
				glGetMaterialiv = v10.glGetMaterialiv;
				glGetPixelMapfv = v10.glGetPixelMapfv;
				glGetPixelMapuiv = v10.glGetPixelMapuiv;
				glGetPixelMapusv = v10.glGetPixelMapusv;
				glGetPolygonStipple = v10.glGetPolygonStipple;
				glGetTexEnvfv = v10.glGetTexEnvfv;
				glGetTexEnviv = v10.glGetTexEnviv;
				glGetTexGendv = v10.glGetTexGendv;
				glGetTexGenfv = v10.glGetTexGenfv;
				glGetTexGeniv = v10.glGetTexGeniv;
				glIsList = v10.glIsList;
				glFrustum = v10.glFrustum;
				glLoadIdentity = v10.glLoadIdentity;
				glLoadMatrixf = v10.glLoadMatrixf;
				glLoadMatrixd = v10.glLoadMatrixd;
				glMatrixMode = v10.glMatrixMode;
				glMultMatrixf = v10.glMultMatrixf;
				glMultMatrixd = v10.glMultMatrixd;
				glOrtho = v10.glOrtho;
				glPopMatrix = v10.glPopMatrix;
				glPushMatrix = v10.glPushMatrix;
				glRotated = v10.glRotated;
				glRotatef = v10.glRotatef;
				glScaled = v10.glScaled;
				glScalef = v10.glScalef;
				glTranslated = v10.glTranslated;
				glTranslatef = v10.glTranslatef;

				//v11
				glDrawArrays = v11.glDrawArrays; 
				glDrawElements = v11.glDrawElements; 
				glPolygonOffset = v11.glPolygonOffset; 
				glCopyTexImage1D = v11.glCopyTexImage1D; 
				glCopyTexImage2D = v11.glCopyTexImage2D; 
				glCopyTexSubImage1D = v11.glCopyTexSubImage1D; 
				glCopyTexSubImage2D = v11.glCopyTexSubImage2D; 
				glTexSubImage1D = v11.glTexSubImage1D; 
				glTexSubImage2D = v11.glTexSubImage2D; 
				glBindTexture = v11.glBindTexture; 
				glDeleteTextures = v11.glDeleteTextures; 
				glGenTextures = v11.glGenTextures; 
				glIsTexture = v11.glIsTexture; 
				glArrayElement = v11.glArrayElement; 
				glColorPointer = v11.glColorPointer; 
				glDisableClientState = v11.glDisableClientState; 
				glEdgeFlagPointer = v11.glEdgeFlagPointer; 
				glEnableClientState = v11.glEnableClientState; 
				glIndexPointer = v11.glIndexPointer; 
				glInterleavedArrays = v11.glInterleavedArrays; 
				glNormalPointer = v11.glNormalPointer; 
				glTexCoordPointer = v11.glTexCoordPointer; 
				glVertexPointer = v11.glVertexPointer; 
				glAreTexturesResident = v11.glAreTexturesResident; 
				glPrioritizeTextures = v11.glPrioritizeTextures; 
				glIndexub = v11.glIndexub; 
				glIndexubv = v11.glIndexubv; 
				glPopClientAttrib = v11.glPopClientAttrib; 
				glPushClientAttrib = v11.glPushClientAttrib; 

				//v12
				glDrawRangeElements = v12.glDrawRangeElements;
				glTexImage3D = v12.glTexImage3D;
				glTexSubImage3D = v12.glTexSubImage3D;
				glCopyTexSubImage3D = v12.glCopyTexSubImage3D;

				//v13
				glActiveTexture = v13.glActiveTexture;
				glSampleCoverage = v13.glSampleCoverage;
				glCompressedTexImage3D = v13.glCompressedTexImage3D;
				glCompressedTexImage2D = v13.glCompressedTexImage2D;
				glCompressedTexImage1D = v13.glCompressedTexImage1D;
				glCompressedTexSubImage3D = v13.glCompressedTexSubImage3D;
				glCompressedTexSubImage2D = v13.glCompressedTexSubImage2D;
				glCompressedTexSubImage1D = v13.glCompressedTexSubImage1D;
				glGetCompressedTexImage = v13.glGetCompressedTexImage;
				glClientActiveTexture = v13.glClientActiveTexture;
				glMultiTexCoord1d = v13.glMultiTexCoord1d;
				glMultiTexCoord1dv = v13.glMultiTexCoord1dv;
				glMultiTexCoord1f = v13.glMultiTexCoord1f;
				glMultiTexCoord1fv = v13.glMultiTexCoord1fv;
				glMultiTexCoord1i = v13.glMultiTexCoord1i;
				glMultiTexCoord1iv = v13.glMultiTexCoord1iv;
				glMultiTexCoord1s = v13.glMultiTexCoord1s;
				glMultiTexCoord1sv = v13.glMultiTexCoord1sv;
				glMultiTexCoord2d = v13.glMultiTexCoord2d;
				glMultiTexCoord2dv = v13.glMultiTexCoord2dv;
				glMultiTexCoord2f = v13.glMultiTexCoord2f;
				glMultiTexCoord2fv = v13.glMultiTexCoord2fv;
				glMultiTexCoord2i = v13.glMultiTexCoord2i;
				glMultiTexCoord2iv = v13.glMultiTexCoord2iv;
				glMultiTexCoord2s = v13.glMultiTexCoord2s;
				glMultiTexCoord2sv = v13.glMultiTexCoord2sv;
				glMultiTexCoord3d = v13.glMultiTexCoord3d;
				glMultiTexCoord3dv = v13.glMultiTexCoord3dv;
				glMultiTexCoord3f = v13.glMultiTexCoord3f;
				glMultiTexCoord3fv = v13.glMultiTexCoord3fv;
				glMultiTexCoord3i = v13.glMultiTexCoord3i;
				glMultiTexCoord3iv = v13.glMultiTexCoord3iv;
				glMultiTexCoord3s = v13.glMultiTexCoord3s;
				glMultiTexCoord3sv = v13.glMultiTexCoord3sv;
				glMultiTexCoord4d = v13.glMultiTexCoord4d;
				glMultiTexCoord4dv = v13.glMultiTexCoord4dv;
				glMultiTexCoord4f = v13.glMultiTexCoord4f;
				glMultiTexCoord4fv = v13.glMultiTexCoord4fv;
				glMultiTexCoord4i = v13.glMultiTexCoord4i;
				glMultiTexCoord4iv = v13.glMultiTexCoord4iv;
				glMultiTexCoord4s = v13.glMultiTexCoord4s;
				glMultiTexCoord4sv = v13.glMultiTexCoord4sv;
				glLoadTransposeMatrixf = v13.glLoadTransposeMatrixf;
				glLoadTransposeMatrixd = v13.glLoadTransposeMatrixd;
				glMultTransposeMatrixf = v13.glMultTransposeMatrixf;
				glMultTransposeMatrixd = v13.glMultTransposeMatrixd;

				//v14
				glBlendFuncSeparate = v14.glBlendFuncSeparate;
				glMultiDrawArrays = v14.glMultiDrawArrays;
				glMultiDrawElements = v14.glMultiDrawElements;
				glPointParameterf = v14.glPointParameterf;
				glPointParameterfv = v14.glPointParameterfv;
				glPointParameteri = v14.glPointParameteri;
				glPointParameteriv = v14.glPointParameteriv;
				glFogCoordf = v14.glFogCoordf;
				glFogCoordfv = v14.glFogCoordfv;
				glFogCoordd = v14.glFogCoordd;
				glFogCoorddv = v14.glFogCoorddv;
				glFogCoordPointer = v14.glFogCoordPointer;
				glSecondaryColor3b = v14.glSecondaryColor3b;
				glSecondaryColor3bv = v14.glSecondaryColor3bv;
				glSecondaryColor3d = v14.glSecondaryColor3d;
				glSecondaryColor3dv = v14.glSecondaryColor3dv;
				glSecondaryColor3f = v14.glSecondaryColor3f;
				glSecondaryColor3fv = v14.glSecondaryColor3fv;
				glSecondaryColor3i = v14.glSecondaryColor3i;
				glSecondaryColor3iv = v14.glSecondaryColor3iv;
				glSecondaryColor3s = v14.glSecondaryColor3s;
				glSecondaryColor3sv = v14.glSecondaryColor3sv;
				glSecondaryColor3ub = v14.glSecondaryColor3ub;
				glSecondaryColor3ubv = v14.glSecondaryColor3ubv;
				glSecondaryColor3ui = v14.glSecondaryColor3ui;
				glSecondaryColor3uiv = v14.glSecondaryColor3uiv;
				glSecondaryColor3us = v14.glSecondaryColor3us;
				glSecondaryColor3usv = v14.glSecondaryColor3usv;
				glSecondaryColorPointer = v14.glSecondaryColorPointer;
				glWindowPos2d = v14.glWindowPos2d;
				glWindowPos2dv = v14.glWindowPos2dv;
				glWindowPos2f = v14.glWindowPos2f;
				glWindowPos2fv = v14.glWindowPos2fv;
				glWindowPos2i = v14.glWindowPos2i;
				glWindowPos2iv = v14.glWindowPos2iv;
				glWindowPos2s = v14.glWindowPos2s;
				glWindowPos2sv = v14.glWindowPos2sv;
				glWindowPos3d = v14.glWindowPos3d;
				glWindowPos3dv = v14.glWindowPos3dv;
				glWindowPos3f = v14.glWindowPos3f;
				glWindowPos3fv = v14.glWindowPos3fv;
				glWindowPos3i = v14.glWindowPos3i;
				glWindowPos3iv = v14.glWindowPos3iv;
				glWindowPos3s = v14.glWindowPos3s;
				glWindowPos3sv = v14.glWindowPos3sv;
				glBlendColor = v14.glBlendColor;
				glBlendEquation = v14.glBlendEquation;

				//v15
				glGenQueries = v15.glGenQueries;
				glDeleteQueries = v15.glDeleteQueries;
				glIsQuery = v15.glIsQuery;
				glBeginQuery = v15.glBeginQuery;
				glEndQuery = v15.glEndQuery;
				glGetQueryiv = v15.glGetQueryiv;
				glGetQueryObjectiv = v15.glGetQueryObjectiv;
				glGetQueryObjectuiv = v15.glGetQueryObjectuiv;
				glBindBuffer = v15.glBindBuffer;
				glDeleteBuffers = v15.glDeleteBuffers;
				glGenBuffers = v15.glGenBuffers;
				glIsBuffer = v15.glIsBuffer;
				glBufferData = v15.glBufferData;
				glBufferSubData = v15.glBufferSubData;
				glGetBufferSubData = v15.glGetBufferSubData;
				glMapBuffer = v15.glMapBuffer;
				glUnmapBuffer = v15.glUnmapBuffer;
				glGetBufferParameteriv = v15.glGetBufferParameteriv;
				glGetBufferPointerv = v15.glGetBufferPointerv;

				//v20
				glBlendEquationSeparate = v20.glBlendEquationSeparate;
				glDrawBuffers = v20.glDrawBuffers;
				glStencilOpSeparate = v20.glStencilOpSeparate;
				glStencilFuncSeparate = v20.glStencilFuncSeparate;
				glStencilMaskSeparate = v20.glStencilMaskSeparate;
				glAttachShader = v20.glAttachShader;
				glBindAttribLocation = v20.glBindAttribLocation;
				glCompileShader = v20.glCompileShader;
				glCreateProgram = v20.glCreateProgram;
				glCreateShader = v20.glCreateShader;
				glDeleteProgram = v20.glDeleteProgram;
				glDeleteShader = v20.glDeleteShader;
				glDetachShader = v20.glDetachShader;
				glDisableVertexAttribArray = v20.glDisableVertexAttribArray;
				glEnableVertexAttribArray = v20.glEnableVertexAttribArray;
				glGetActiveAttrib = v20.glGetActiveAttrib;
				glGetActiveUniform = v20.glGetActiveUniform;
				glGetAttachedShaders = v20.glGetAttachedShaders;
				glGetAttribLocation = v20.glGetAttribLocation;
				glGetProgramiv = v20.glGetProgramiv;
				glGetProgramInfoLog = v20.glGetProgramInfoLog;
				glGetShaderiv = v20.glGetShaderiv;
				glGetShaderInfoLog = v20.glGetShaderInfoLog;
				glGetShaderSource = v20.glGetShaderSource;
				glGetUniformLocation = v20.glGetUniformLocation;
				glGetUniformfv = v20.glGetUniformfv;
				glGetUniformiv = v20.glGetUniformiv;
				glGetVertexAttribdv = v20.glGetVertexAttribdv;
				glGetVertexAttribfv = v20.glGetVertexAttribfv;
				glGetVertexAttribiv = v20.glGetVertexAttribiv;
				glGetVertexAttribPointerv = v20.glGetVertexAttribPointerv;
				glIsProgram = v20.glIsProgram;
				glIsShader = v20.glIsShader;
				glLinkProgram = v20.glLinkProgram;
				glShaderSource = v20.glShaderSource;
				glUseProgram = v20.glUseProgram;
				glUniform1f = v20.glUniform1f;
				glUniform2f = v20.glUniform2f;
				glUniform3f = v20.glUniform3f;
				glUniform4f = v20.glUniform4f;
				glUniform1i = v20.glUniform1i;
				glUniform2i = v20.glUniform2i;
				glUniform3i = v20.glUniform3i;
				glUniform4i = v20.glUniform4i;
				glUniform1fv = v20.glUniform1fv;
				glUniform2fv = v20.glUniform2fv;
				glUniform3fv = v20.glUniform3fv;
				glUniform4fv = v20.glUniform4fv;
				glUniform1iv = v20.glUniform1iv;
				glUniform2iv = v20.glUniform2iv;
				glUniform3iv = v20.glUniform3iv;
				glUniform4iv = v20.glUniform4iv;
				glUniformMatrix2fv = v20.glUniformMatrix2fv;
				glUniformMatrix3fv = v20.glUniformMatrix3fv;
				glUniformMatrix4fv = v20.glUniformMatrix4fv;
				glValidateProgram = v20.glValidateProgram;
				glVertexAttrib1d = v20.glVertexAttrib1d;
				glVertexAttrib1dv = v20.glVertexAttrib1dv;
				glVertexAttrib1f = v20.glVertexAttrib1f;
				glVertexAttrib1fv = v20.glVertexAttrib1fv;
				glVertexAttrib1s = v20.glVertexAttrib1s;
				glVertexAttrib1sv = v20.glVertexAttrib1sv;
				glVertexAttrib2d = v20.glVertexAttrib2d;
				glVertexAttrib2dv = v20.glVertexAttrib2dv;
				glVertexAttrib2f = v20.glVertexAttrib2f;
				glVertexAttrib2fv = v20.glVertexAttrib2fv;
				glVertexAttrib2s = v20.glVertexAttrib2s;
				glVertexAttrib2sv = v20.glVertexAttrib2sv;
				glVertexAttrib3d = v20.glVertexAttrib3d;
				glVertexAttrib3dv = v20.glVertexAttrib3dv;
				glVertexAttrib3f = v20.glVertexAttrib3f;
				glVertexAttrib3fv = v20.glVertexAttrib3fv;
				glVertexAttrib3s = v20.glVertexAttrib3s;
				glVertexAttrib3sv = v20.glVertexAttrib3sv;
				glVertexAttrib4Nbv = v20.glVertexAttrib4Nbv;
				glVertexAttrib4Niv = v20.glVertexAttrib4Niv;
				glVertexAttrib4Nsv = v20.glVertexAttrib4Nsv;
				glVertexAttrib4Nub = v20.glVertexAttrib4Nub;
				glVertexAttrib4Nubv = v20.glVertexAttrib4Nubv;
				glVertexAttrib4Nuiv = v20.glVertexAttrib4Nuiv;
				glVertexAttrib4Nusv = v20.glVertexAttrib4Nusv;
				glVertexAttrib4bv = v20.glVertexAttrib4bv;
				glVertexAttrib4d = v20.glVertexAttrib4d;
				glVertexAttrib4dv = v20.glVertexAttrib4dv;
				glVertexAttrib4f = v20.glVertexAttrib4f;
				glVertexAttrib4fv = v20.glVertexAttrib4fv;
				glVertexAttrib4iv = v20.glVertexAttrib4iv;
				glVertexAttrib4s = v20.glVertexAttrib4s;
				glVertexAttrib4sv = v20.glVertexAttrib4sv;
				glVertexAttrib4ubv = v20.glVertexAttrib4ubv;
				glVertexAttrib4uiv = v20.glVertexAttrib4uiv;
				glVertexAttrib4usv = v20.glVertexAttrib4usv;
				glVertexAttribPointer = v20.glVertexAttribPointer;

				//v21
				glUniformMatrix2x3fv = v21.glUniformMatrix2x3fv;
				glUniformMatrix3x2fv = v21.glUniformMatrix3x2fv;
				glUniformMatrix2x4fv = v21.glUniformMatrix2x4fv;
				glUniformMatrix4x2fv = v21.glUniformMatrix4x2fv;
				glUniformMatrix3x4fv = v21.glUniformMatrix3x4fv;
				glUniformMatrix4x3fv = v21.glUniformMatrix4x3fv;

				//v30
				glColorMaski = v30.glColorMaski;
				glGetBooleani_v = v30.glGetBooleani_v;
				glGetIntegeri_v = v30.glGetIntegeri_v;
				glEnablei = v30.glEnablei;
				glDisablei = v30.glDisablei;
				glIsEnabledi = v30.glIsEnabledi;
				glBeginTransformFeedback = v30.glBeginTransformFeedback;
				glEndTransformFeedback = v30.glEndTransformFeedback;
				glBindBufferRange = v30.glBindBufferRange;
				glBindBufferBase = v30.glBindBufferBase;
				glTransformFeedbackVaryings = v30.glTransformFeedbackVaryings;
				glGetTransformFeedbackVarying = v30.glGetTransformFeedbackVarying;
				glClampColor = v30.glClampColor;
				glBeginConditionalRender = v30.glBeginConditionalRender;
				glEndConditionalRender = v30.glEndConditionalRender;
				glVertexAttribIPointer = v30.glVertexAttribIPointer;
				glGetVertexAttribIiv = v30.glGetVertexAttribIiv;
				glGetVertexAttribIuiv = v30.glGetVertexAttribIuiv;
				glVertexAttribI1i = v30.glVertexAttribI1i;
				glVertexAttribI2i = v30.glVertexAttribI2i;
				glVertexAttribI3i = v30.glVertexAttribI3i;
				glVertexAttribI4i = v30.glVertexAttribI4i;
				glVertexAttribI1ui = v30.glVertexAttribI1ui;
				glVertexAttribI2ui = v30.glVertexAttribI2ui;
				glVertexAttribI3ui = v30.glVertexAttribI3ui;
				glVertexAttribI4ui = v30.glVertexAttribI4ui;
				glVertexAttribI1iv = v30.glVertexAttribI1iv;
				glVertexAttribI2iv = v30.glVertexAttribI2iv;
				glVertexAttribI3iv = v30.glVertexAttribI3iv;
				glVertexAttribI4iv = v30.glVertexAttribI4iv;
				glVertexAttribI1uiv = v30.glVertexAttribI1uiv;
				glVertexAttribI2uiv = v30.glVertexAttribI2uiv;
				glVertexAttribI3uiv = v30.glVertexAttribI3uiv;
				glVertexAttribI4uiv = v30.glVertexAttribI4uiv;
				glVertexAttribI4bv = v30.glVertexAttribI4bv;
				glVertexAttribI4sv = v30.glVertexAttribI4sv;
				glVertexAttribI4ubv = v30.glVertexAttribI4ubv;
				glVertexAttribI4usv = v30.glVertexAttribI4usv;
				glGetUniformuiv = v30.glGetUniformuiv;
				glBindFragDataLocation = v30.glBindFragDataLocation;
				glGetFragDataLocation = v30.glGetFragDataLocation;
				glUniform1ui = v30.glUniform1ui;
				glUniform2ui = v30.glUniform2ui;
				glUniform3ui = v30.glUniform3ui;
				glUniform4ui = v30.glUniform4ui;
				glUniform1uiv = v30.glUniform1uiv;
				glUniform2uiv = v30.glUniform2uiv;
				glUniform3uiv = v30.glUniform3uiv;
				glUniform4uiv = v30.glUniform4uiv;
				glTexParameterIiv = v30.glTexParameterIiv;
				glTexParameterIuiv = v30.glTexParameterIuiv;
				glGetTexParameterIiv = v30.glGetTexParameterIiv;
				glGetTexParameterIuiv = v30.glGetTexParameterIuiv;
				glClearBufferiv = v30.glClearBufferiv;
				glClearBufferuiv = v30.glClearBufferuiv;
				glClearBufferfv = v30.glClearBufferfv;
				glClearBufferfi = v30.glClearBufferfi;
				glGetStringi = v30.glGetStringi;
				glIsRenderbuffer = v30.glIsRenderbuffer;
				glBindRenderbuffer = v30.glBindRenderbuffer;
				glDeleteRenderbuffers = v30.glDeleteRenderbuffers;
				glGenRenderbuffers = v30.glGenRenderbuffers;
				glRenderbufferStorage = v30.glRenderbufferStorage;
				glGetRenderbufferParameteriv = v30.glGetRenderbufferParameteriv;
				glIsFramebuffer = v30.glIsFramebuffer;
				glBindFramebuffer = v30.glBindFramebuffer;
				glDeleteFramebuffers = v30.glDeleteFramebuffers;
				glGenFramebuffers = v30.glGenFramebuffers;
				glCheckFramebufferStatus = v30.glCheckFramebufferStatus;
				glFramebufferTexture1D = v30.glFramebufferTexture1D;
				glFramebufferTexture2D = v30.glFramebufferTexture2D;
				glFramebufferTexture3D = v30.glFramebufferTexture3D;
				glFramebufferRenderbuffer = v30.glFramebufferRenderbuffer;
				glGetFramebufferAttachmentParameteriv = v30.glGetFramebufferAttachmentParameteriv;
				glGenerateMipmap = v30.glGenerateMipmap;
				glBlitFramebuffer = v30.glBlitFramebuffer;
				glRenderbufferStorageMultisample = v30.glRenderbufferStorageMultisample;
				glFramebufferTextureLayer = v30.glFramebufferTextureLayer;
				glMapBufferRange = v30.glMapBufferRange;
				glFlushMappedBufferRange = v30.glFlushMappedBufferRange;
				glBindVertexArray = v30.glBindVertexArray;
				glDeleteVertexArrays = v30.glDeleteVertexArrays;
				glGenVertexArrays = v30.glGenVertexArrays;
				glIsVertexArray = v30.glIsVertexArray;

				//v31
				glDrawArraysInstanced = v31.glDrawArraysInstanced;
				glDrawElementsInstanced = v31.glDrawElementsInstanced;
				glTexBuffer = v31.glTexBuffer;
				glPrimitiveRestartIndex = v31.glPrimitiveRestartIndex;
				glCopyBufferSubData = v31.glCopyBufferSubData;
				glGetUniformIndices = v31.glGetUniformIndices;
				glGetActiveUniformsiv = v31.glGetActiveUniformsiv;
				glGetActiveUniformName = v31.glGetActiveUniformName;
				glGetUniformBlockIndex = v31.glGetUniformBlockIndex;
				glGetActiveUniformBlockiv = v31.glGetActiveUniformBlockiv;
				glGetActiveUniformBlockName = v31.glGetActiveUniformBlockName;
				glUniformBlockBinding = v31.glUniformBlockBinding;

				//v32
				glDrawElementsBaseVertex = v32.glDrawElementsBaseVertex;
				glDrawRangeElementsBaseVertex = v32.glDrawRangeElementsBaseVertex;
				glDrawElementsInstancedBaseVertex = v32.glDrawElementsInstancedBaseVertex;
				glMultiDrawElementsBaseVertex = v32.glMultiDrawElementsBaseVertex;
				glProvokingVertex = v32.glProvokingVertex;
				glFenceSync = v32.glFenceSync;
				glIsSync = v32.glIsSync;
				glDeleteSync = v32.glDeleteSync;
				glClientWaitSync = v32.glClientWaitSync;
				glWaitSync = v32.glWaitSync;
				glGetInteger64v = v32.glGetInteger64v;
				glGetSynciv = v32.glGetSynciv;
				glGetInteger64i_v = v32.glGetInteger64i_v;
				glGetBufferParameteri64v = v32.glGetBufferParameteri64v;
				glFramebufferTexture = v32.glFramebufferTexture;
				glTexImage2DMultisample = v32.glTexImage2DMultisample;
				glTexImage3DMultisample = v32.glTexImage3DMultisample;
				glGetMultisamplefv = v32.glGetMultisamplefv;
				glSampleMaski = v32.glSampleMaski;
				glVertexP2ui = v32.glVertexP2ui;
				glVertexP2uiv = v32.glVertexP2uiv;
				glVertexP3ui = v32.glVertexP3ui;
				glVertexP3uiv = v32.glVertexP3uiv;
				glVertexP4ui = v32.glVertexP4ui;
				glVertexP4uiv = v32.glVertexP4uiv;
				glTexCoordP1ui = v32.glTexCoordP1ui;
				glTexCoordP1uiv = v32.glTexCoordP1uiv;
				glTexCoordP2ui = v32.glTexCoordP2ui;
				glTexCoordP2uiv = v32.glTexCoordP2uiv;
				glTexCoordP3ui = v32.glTexCoordP3ui;
				glTexCoordP3uiv = v32.glTexCoordP3uiv;
				glTexCoordP4ui = v32.glTexCoordP4ui;
				glTexCoordP4uiv = v32.glTexCoordP4uiv;
				glMultiTexCoordP1ui = v32.glMultiTexCoordP1ui;
				glMultiTexCoordP1uiv = v32.glMultiTexCoordP1uiv;
				glMultiTexCoordP2ui = v32.glMultiTexCoordP2ui;
				glMultiTexCoordP2uiv = v32.glMultiTexCoordP2uiv;
				glMultiTexCoordP3ui = v32.glMultiTexCoordP3ui;
				glMultiTexCoordP3uiv = v32.glMultiTexCoordP3uiv;
				glMultiTexCoordP4ui = v32.glMultiTexCoordP4ui;
				glMultiTexCoordP4uiv = v32.glMultiTexCoordP4uiv;
				glNormalP3ui = v32.glNormalP3ui;
				glNormalP3uiv = v32.glNormalP3uiv;
				glColorP3ui = v32.glColorP3ui;
				glColorP3uiv = v32.glColorP3uiv;
				glColorP4ui = v32.glColorP4ui;
				glColorP4uiv = v32.glColorP4uiv;
				glSecondaryColorP3ui = v32.glSecondaryColorP3ui;
				glSecondaryColorP3uiv = v32.glSecondaryColorP3uiv;

				//v33
				glBindFragDataLocationIndexed = v33.glBindFragDataLocationIndexed;
				glGetFragDataIndex = v33.glGetFragDataIndex;
				glGenSamplers = v33.glGenSamplers;
				glDeleteSamplers = v33.glDeleteSamplers;
				glIsSampler = v33.glIsSampler;
				glBindSampler = v33.glBindSampler;
				glSamplerParameteri = v33.glSamplerParameteri;
				glSamplerParameteriv = v33.glSamplerParameteriv;
				glSamplerParameterf = v33.glSamplerParameterf;
				glSamplerParameterfv = v33.glSamplerParameterfv;
				glSamplerParameterIiv = v33.glSamplerParameterIiv;
				glSamplerParameterIuiv = v33.glSamplerParameterIuiv;
				glGetSamplerParameteriv = v33.glGetSamplerParameteriv;
				glGetSamplerParameterIiv = v33.glGetSamplerParameterIiv;
				glGetSamplerParameterfv = v33.glGetSamplerParameterfv;
				glGetSamplerParameterIuiv = v33.glGetSamplerParameterIuiv;
				glQueryCounter = v33.glQueryCounter;
				glGetQueryObjecti64v = v33.glGetQueryObjecti64v;
				glGetQueryObjectui64v = v33.glGetQueryObjectui64v;
				glVertexAttribDivisor = v33.glVertexAttribDivisor;
				glVertexAttribP1ui = v33.glVertexAttribP1ui;
				glVertexAttribP1uiv = v33.glVertexAttribP1uiv;
				glVertexAttribP2ui = v33.glVertexAttribP2ui;
				glVertexAttribP2uiv = v33.glVertexAttribP2uiv;
				glVertexAttribP3ui = v33.glVertexAttribP3ui;
				glVertexAttribP3uiv = v33.glVertexAttribP3uiv;
				glVertexAttribP4ui = v33.glVertexAttribP4ui;
				glVertexAttribP4uiv = v33.glVertexAttribP4uiv;

				//v40
				glMinSampleShading = v40.glMinSampleShading;
				glBlendEquationi = v40.glBlendEquationi;
				glBlendEquationSeparatei = v40.glBlendEquationSeparatei;
				glBlendFunci = v40.glBlendFunci;
				glBlendFuncSeparatei = v40.glBlendFuncSeparatei;
				glDrawArraysIndirect = v40.glDrawArraysIndirect;
				glDrawElementsIndirect = v40.glDrawElementsIndirect;
				glUniform1d = v40.glUniform1d;
				glUniform2d = v40.glUniform2d;
				glUniform3d = v40.glUniform3d;
				glUniform4d = v40.glUniform4d;
				glUniform1dv = v40.glUniform1dv;
				glUniform2dv = v40.glUniform2dv;
				glUniform3dv = v40.glUniform3dv;
				glUniform4dv = v40.glUniform4dv;
				glUniformMatrix2dv = v40.glUniformMatrix2dv;
				glUniformMatrix3dv = v40.glUniformMatrix3dv;
				glUniformMatrix4dv = v40.glUniformMatrix4dv;
				glUniformMatrix2x3dv = v40.glUniformMatrix2x3dv;
				glUniformMatrix2x4dv = v40.glUniformMatrix2x4dv;
				glUniformMatrix3x2dv = v40.glUniformMatrix3x2dv;
				glUniformMatrix3x4dv = v40.glUniformMatrix3x4dv;
				glUniformMatrix4x2dv = v40.glUniformMatrix4x2dv;
				glUniformMatrix4x3dv = v40.glUniformMatrix4x3dv;
				glGetUniformdv = v40.glGetUniformdv;
				glGetSubroutineUniformLocation = v40.glGetSubroutineUniformLocation;
				glGetSubroutineIndex = v40.glGetSubroutineIndex;
				glGetActiveSubroutineUniformiv = v40.glGetActiveSubroutineUniformiv;
				glGetActiveSubroutineUniformName = v40.glGetActiveSubroutineUniformName;
				glGetActiveSubroutineName = v40.glGetActiveSubroutineName;
				glUniformSubroutinesuiv = v40.glUniformSubroutinesuiv;
				glGetUniformSubroutineuiv = v40.glGetUniformSubroutineuiv;
				glGetProgramStageiv = v40.glGetProgramStageiv;
				glPatchParameteri = v40.glPatchParameteri;
				glPatchParameterfv = v40.glPatchParameterfv;
				glBindTransformFeedback = v40.glBindTransformFeedback;
				glDeleteTransformFeedbacks = v40.glDeleteTransformFeedbacks;
				glGenTransformFeedbacks = v40.glGenTransformFeedbacks;
				glIsTransformFeedback = v40.glIsTransformFeedback;
				glPauseTransformFeedback = v40.glPauseTransformFeedback;
				glResumeTransformFeedback = v40.glResumeTransformFeedback;
				glDrawTransformFeedback = v40.glDrawTransformFeedback;
				glDrawTransformFeedbackStream = v40.glDrawTransformFeedbackStream;
				glBeginQueryIndexed = v40.glBeginQueryIndexed;
				glEndQueryIndexed = v40.glEndQueryIndexed;
				glGetQueryIndexediv = v40.glGetQueryIndexediv;

				//v41
				glReleaseShaderCompiler = v41.glReleaseShaderCompiler;
				glShaderBinary = v41.glShaderBinary;
				glGetShaderPrecisionFormat = v41.glGetShaderPrecisionFormat;
				glDepthRangef = v41.glDepthRangef;
				glClearDepthf = v41.glClearDepthf;
				glGetProgramBinary = v41.glGetProgramBinary;
				glProgramBinary = v41.glProgramBinary;
				glProgramParameteri = v41.glProgramParameteri;
				glUseProgramStages = v41.glUseProgramStages;
				glActiveShaderProgram = v41.glActiveShaderProgram;
				glCreateShaderProgramv = v41.glCreateShaderProgramv;
				glBindProgramPipeline = v41.glBindProgramPipeline;
				glDeleteProgramPipelines = v41.glDeleteProgramPipelines;
				glGenProgramPipelines = v41.glGenProgramPipelines;
				glIsProgramPipeline = v41.glIsProgramPipeline;
				glGetProgramPipelineiv = v41.glGetProgramPipelineiv;
				glProgramUniform1i = v41.glProgramUniform1i;
				glProgramUniform1iv = v41.glProgramUniform1iv;
				glProgramUniform1f = v41.glProgramUniform1f;
				glProgramUniform1fv = v41.glProgramUniform1fv;
				glProgramUniform1d = v41.glProgramUniform1d;
				glProgramUniform1dv = v41.glProgramUniform1dv;
				glProgramUniform1ui = v41.glProgramUniform1ui;
				glProgramUniform1uiv = v41.glProgramUniform1uiv;
				glProgramUniform2i = v41.glProgramUniform2i;
				glProgramUniform2iv = v41.glProgramUniform2iv;
				glProgramUniform2f = v41.glProgramUniform2f;
				glProgramUniform2fv = v41.glProgramUniform2fv;
				glProgramUniform2d = v41.glProgramUniform2d;
				glProgramUniform2dv = v41.glProgramUniform2dv;
				glProgramUniform2ui = v41.glProgramUniform2ui;
				glProgramUniform2uiv = v41.glProgramUniform2uiv;
				glProgramUniform3i = v41.glProgramUniform3i;
				glProgramUniform3iv = v41.glProgramUniform3iv;
				glProgramUniform3f = v41.glProgramUniform3f;
				glProgramUniform3fv = v41.glProgramUniform3fv;
				glProgramUniform3d = v41.glProgramUniform3d;
				glProgramUniform3dv = v41.glProgramUniform3dv;
				glProgramUniform3ui = v41.glProgramUniform3ui;
				glProgramUniform3uiv = v41.glProgramUniform3uiv;
				glProgramUniform4i = v41.glProgramUniform4i;
				glProgramUniform4iv = v41.glProgramUniform4iv;
				glProgramUniform4f = v41.glProgramUniform4f;
				glProgramUniform4fv = v41.glProgramUniform4fv;
				glProgramUniform4d = v41.glProgramUniform4d;
				glProgramUniform4dv = v41.glProgramUniform4dv;
				glProgramUniform4ui = v41.glProgramUniform4ui;
				glProgramUniform4uiv = v41.glProgramUniform4uiv;
				glProgramUniformMatrix2fv = v41.glProgramUniformMatrix2fv;
				glProgramUniformMatrix3fv = v41.glProgramUniformMatrix3fv;
				glProgramUniformMatrix4fv = v41.glProgramUniformMatrix4fv;
				glProgramUniformMatrix2dv = v41.glProgramUniformMatrix2dv;
				glProgramUniformMatrix3dv = v41.glProgramUniformMatrix3dv;
				glProgramUniformMatrix4dv = v41.glProgramUniformMatrix4dv;
				glProgramUniformMatrix2x3fv = v41.glProgramUniformMatrix2x3fv;
				glProgramUniformMatrix3x2fv = v41.glProgramUniformMatrix3x2fv;
				glProgramUniformMatrix2x4fv = v41.glProgramUniformMatrix2x4fv;
				glProgramUniformMatrix4x2fv = v41.glProgramUniformMatrix4x2fv;
				glProgramUniformMatrix3x4fv = v41.glProgramUniformMatrix3x4fv;
				glProgramUniformMatrix4x3fv = v41.glProgramUniformMatrix4x3fv;
				glProgramUniformMatrix2x3dv = v41.glProgramUniformMatrix2x3dv;
				glProgramUniformMatrix3x2dv = v41.glProgramUniformMatrix3x2dv;
				glProgramUniformMatrix2x4dv = v41.glProgramUniformMatrix2x4dv;
				glProgramUniformMatrix4x2dv = v41.glProgramUniformMatrix4x2dv;
				glProgramUniformMatrix3x4dv = v41.glProgramUniformMatrix3x4dv;
				glProgramUniformMatrix4x3dv = v41.glProgramUniformMatrix4x3dv;
				glValidateProgramPipeline = v41.glValidateProgramPipeline;
				glGetProgramPipelineInfoLog = v41.glGetProgramPipelineInfoLog;
				glVertexAttribL1d = v41.glVertexAttribL1d;
				glVertexAttribL2d = v41.glVertexAttribL2d;
				glVertexAttribL3d = v41.glVertexAttribL3d;
				glVertexAttribL4d = v41.glVertexAttribL4d;
				glVertexAttribL1dv = v41.glVertexAttribL1dv;
				glVertexAttribL2dv = v41.glVertexAttribL2dv;
				glVertexAttribL3dv = v41.glVertexAttribL3dv;
				glVertexAttribL4dv = v41.glVertexAttribL4dv;
				glVertexAttribLPointer = v41.glVertexAttribLPointer;
				glGetVertexAttribLdv = v41.glGetVertexAttribLdv;
				glViewportArrayv = v41.glViewportArrayv;
				glViewportIndexedf = v41.glViewportIndexedf;
				glViewportIndexedfv = v41.glViewportIndexedfv;
				glScissorArrayv = v41.glScissorArrayv;
				glScissorIndexed = v41.glScissorIndexed;
				glScissorIndexedv = v41.glScissorIndexedv;
				glDepthRangeArrayv = v41.glDepthRangeArrayv;
				glDepthRangeIndexed = v41.glDepthRangeIndexed;
				glGetFloati_v = v41.glGetFloati_v;
				glGetDoublei_v = v41.glGetDoublei_v;

				//v42
				glDrawArraysInstancedBaseInstance = v42.glDrawArraysInstancedBaseInstance;
				glDrawElementsInstancedBaseInstance = v42.glDrawElementsInstancedBaseInstance;
				glDrawElementsInstancedBaseVertexBaseInstance = v42.glDrawElementsInstancedBaseVertexBaseInstance;
				glGetInternalformativ = v42.glGetInternalformativ;
				glGetActiveAtomicCounterBufferiv = v42.glGetActiveAtomicCounterBufferiv;
				glBindImageTexture = v42.glBindImageTexture;
				glMemoryBarrier = v42.glMemoryBarrier;
				glTexStorage1D = v42.glTexStorage1D;
				glTexStorage2D = v42.glTexStorage2D;
				glTexStorage3D = v42.glTexStorage3D;
				glDrawTransformFeedbackInstanced = v42.glDrawTransformFeedbackInstanced;
				glDrawTransformFeedbackStreamInstanced = v42.glDrawTransformFeedbackStreamInstanced;

				//v43
				glClearBufferData = v43.glClearBufferData;
				glClearBufferSubData = v43.glClearBufferSubData;
				glDispatchCompute = v43.glDispatchCompute;
				glDispatchComputeIndirect = v43.glDispatchComputeIndirect;
				glCopyImageSubData = v43.glCopyImageSubData;
				glFramebufferParameteri = v43.glFramebufferParameteri;
				glGetFramebufferParameteriv = v43.glGetFramebufferParameteriv;
				glGetInternalformati64v = v43.glGetInternalformati64v;
				glInvalidateTexSubImage = v43.glInvalidateTexSubImage;
				glInvalidateTexImage = v43.glInvalidateTexImage;
				glInvalidateBufferSubData = v43.glInvalidateBufferSubData;
				glInvalidateBufferData = v43.glInvalidateBufferData;
				glInvalidateFramebuffer = v43.glInvalidateFramebuffer;
				glInvalidateSubFramebuffer = v43.glInvalidateSubFramebuffer;
				glMultiDrawArraysIndirect = v43.glMultiDrawArraysIndirect;
				glMultiDrawElementsIndirect = v43.glMultiDrawElementsIndirect;
				glGetProgramInterfaceiv = v43.glGetProgramInterfaceiv;
				glGetProgramResourceIndex = v43.glGetProgramResourceIndex;
				glGetProgramResourceName = v43.glGetProgramResourceName;
				glGetProgramResourceiv = v43.glGetProgramResourceiv;
				glGetProgramResourceLocation = v43.glGetProgramResourceLocation;
				glGetProgramResourceLocationIndex = v43.glGetProgramResourceLocationIndex;
				glShaderStorageBlockBinding = v43.glShaderStorageBlockBinding;
				glTexBufferRange = v43.glTexBufferRange;
				glTexStorage2DMultisample = v43.glTexStorage2DMultisample;
				glTexStorage3DMultisample = v43.glTexStorage3DMultisample;
				glTextureView = v43.glTextureView;
				glBindVertexBuffer = v43.glBindVertexBuffer;
				glVertexAttribFormat = v43.glVertexAttribFormat;
				glVertexAttribIFormat = v43.glVertexAttribIFormat;
				glVertexAttribLFormat = v43.glVertexAttribLFormat;
				glVertexAttribBinding = v43.glVertexAttribBinding;
				glVertexBindingDivisor = v43.glVertexBindingDivisor;
				glDebugMessageControl = v43.glDebugMessageControl;
				glDebugMessageInsert = v43.glDebugMessageInsert;
				glDebugMessageCallback = v43.glDebugMessageCallback;
				glGetDebugMessageLog = v43.glGetDebugMessageLog;
				glPushDebugGroup = v43.glPushDebugGroup;
				glPopDebugGroup = v43.glPopDebugGroup;
				glObjectLabel = v43.glObjectLabel;
				glGetObjectLabel = v43.glGetObjectLabel;
				glObjectPtrLabel = v43.glObjectPtrLabel;
				glGetObjectPtrLabel = v43.glGetObjectPtrLabel;
				glGetPointerv = v43.glGetPointerv;

				//v44
				glBufferStorage = v44.glBufferStorage;
				glClearTexImage = v44.glClearTexImage;
				glClearTexSubImage = v44.glClearTexSubImage;
				glBindBuffersBase = v44.glBindBuffersBase;
				glBindBuffersRange = v44.glBindBuffersRange;
				glBindTextures = v44.glBindTextures;
				glBindSamplers = v44.glBindSamplers;
				glBindImageTextures = v44.glBindImageTextures;
				glBindVertexBuffers = v44.glBindVertexBuffers;

				//v45
				glClipControl = v45.glClipControl;
				glCreateTransformFeedbacks = v45.glCreateTransformFeedbacks;
				glTransformFeedbackBufferBase = v45.glTransformFeedbackBufferBase;
				glTransformFeedbackBufferRange = v45.glTransformFeedbackBufferRange;
				glGetTransformFeedbackiv = v45.glGetTransformFeedbackiv;
				glGetTransformFeedbacki_v = v45.glGetTransformFeedbacki_v;
				glGetTransformFeedbacki64_v = v45.glGetTransformFeedbacki64_v;
				glCreateBuffers = v45.glCreateBuffers;
				glNamedBufferStorage = v45.glNamedBufferStorage;
				glNamedBufferData = v45.glNamedBufferData;
				glNamedBufferSubData = v45.glNamedBufferSubData;
				glCopyNamedBufferSubData = v45.glCopyNamedBufferSubData;
				glClearNamedBufferData = v45.glClearNamedBufferData;
				glClearNamedBufferSubData = v45.glClearNamedBufferSubData;
				glMapNamedBuffer = v45.glMapNamedBuffer;
				glMapNamedBufferRange = v45.glMapNamedBufferRange;
				glUnmapNamedBuffer = v45.glUnmapNamedBuffer;
				glFlushMappedNamedBufferRange = v45.glFlushMappedNamedBufferRange;
				glGetNamedBufferParameteriv = v45.glGetNamedBufferParameteriv;
				glGetNamedBufferParameteri64v = v45.glGetNamedBufferParameteri64v;
				glGetNamedBufferPointerv = v45.glGetNamedBufferPointerv;
				glGetNamedBufferSubData = v45.glGetNamedBufferSubData;
				glCreateFramebuffers = v45.glCreateFramebuffers;
				glNamedFramebufferRenderbuffer = v45.glNamedFramebufferRenderbuffer;
				glNamedFramebufferParameteri = v45.glNamedFramebufferParameteri;
				glNamedFramebufferTexture = v45.glNamedFramebufferTexture;
				glNamedFramebufferTextureLayer = v45.glNamedFramebufferTextureLayer;
				glNamedFramebufferDrawBuffer = v45.glNamedFramebufferDrawBuffer;
				glNamedFramebufferDrawBuffers = v45.glNamedFramebufferDrawBuffers;
				glNamedFramebufferReadBuffer = v45.glNamedFramebufferReadBuffer;
				glInvalidateNamedFramebufferData = v45.glInvalidateNamedFramebufferData;
				glInvalidateNamedFramebufferSubData = v45.glInvalidateNamedFramebufferSubData;
				glClearNamedFramebufferiv = v45.glClearNamedFramebufferiv;
				glClearNamedFramebufferuiv = v45.glClearNamedFramebufferuiv;
				glClearNamedFramebufferfv = v45.glClearNamedFramebufferfv;
				glClearNamedFramebufferfi = v45.glClearNamedFramebufferfi;
				glBlitNamedFramebuffer = v45.glBlitNamedFramebuffer;
				glCheckNamedFramebufferStatus = v45.glCheckNamedFramebufferStatus;
				glGetNamedFramebufferParameteriv = v45.glGetNamedFramebufferParameteriv;
				glGetNamedFramebufferAttachmentParameteriv = v45.glGetNamedFramebufferAttachmentParameteriv;
				glCreateRenderbuffers = v45.glCreateRenderbuffers;
				glNamedRenderbufferStorage = v45.glNamedRenderbufferStorage;
				glNamedRenderbufferStorageMultisample = v45.glNamedRenderbufferStorageMultisample;
				glGetNamedRenderbufferParameteriv = v45.glGetNamedRenderbufferParameteriv;
				glCreateTextures = v45.glCreateTextures;
				glTextureBuffer = v45.glTextureBuffer;
				glTextureBufferRange = v45.glTextureBufferRange;
				glTextureStorage1D = v45.glTextureStorage1D;
				glTextureStorage2D = v45.glTextureStorage2D;
				glTextureStorage3D = v45.glTextureStorage3D;
				glTextureStorage2DMultisample = v45.glTextureStorage2DMultisample;
				glTextureStorage3DMultisample = v45.glTextureStorage3DMultisample;
				glTextureSubImage1D = v45.glTextureSubImage1D;
				glTextureSubImage2D = v45.glTextureSubImage2D;
				glTextureSubImage3D = v45.glTextureSubImage3D;
				glCompressedTextureSubImage1D = v45.glCompressedTextureSubImage1D;
				glCompressedTextureSubImage2D = v45.glCompressedTextureSubImage2D;
				glCompressedTextureSubImage3D = v45.glCompressedTextureSubImage3D;
				glCopyTextureSubImage1D = v45.glCopyTextureSubImage1D;
				glCopyTextureSubImage2D = v45.glCopyTextureSubImage2D;
				glCopyTextureSubImage3D = v45.glCopyTextureSubImage3D;
				glTextureParameterf = v45.glTextureParameterf;
				glTextureParameterfv = v45.glTextureParameterfv;
				glTextureParameteri = v45.glTextureParameteri;
				glTextureParameterIiv = v45.glTextureParameterIiv;
				glTextureParameterIuiv = v45.glTextureParameterIuiv;
				glTextureParameteriv = v45.glTextureParameteriv;
				glGenerateTextureMipmap = v45.glGenerateTextureMipmap;
				glBindTextureUnit = v45.glBindTextureUnit;
				glGetTextureImage = v45.glGetTextureImage;
				glGetCompressedTextureImage = v45.glGetCompressedTextureImage;
				glGetTextureLevelParameterfv = v45.glGetTextureLevelParameterfv;
				glGetTextureLevelParameteriv = v45.glGetTextureLevelParameteriv;
				glGetTextureParameterfv = v45.glGetTextureParameterfv;
				glGetTextureParameterIiv = v45.glGetTextureParameterIiv;
				glGetTextureParameterIuiv = v45.glGetTextureParameterIuiv;
				glGetTextureParameteriv = v45.glGetTextureParameteriv;
				glCreateVertexArrays = v45.glCreateVertexArrays;
				glDisableVertexArrayAttrib = v45.glDisableVertexArrayAttrib;
				glEnableVertexArrayAttrib = v45.glEnableVertexArrayAttrib;
				glVertexArrayElementBuffer = v45.glVertexArrayElementBuffer;
				glVertexArrayVertexBuffer = v45.glVertexArrayVertexBuffer;
				glVertexArrayVertexBuffers = v45.glVertexArrayVertexBuffers;
				glVertexArrayAttribBinding = v45.glVertexArrayAttribBinding;
				glVertexArrayAttribFormat = v45.glVertexArrayAttribFormat;
				glVertexArrayAttribIFormat = v45.glVertexArrayAttribIFormat;
				glVertexArrayAttribLFormat = v45.glVertexArrayAttribLFormat;
				glVertexArrayBindingDivisor = v45.glVertexArrayBindingDivisor;
				glGetVertexArrayiv = v45.glGetVertexArrayiv;
				glGetVertexArrayIndexediv = v45.glGetVertexArrayIndexediv;
				glGetVertexArrayIndexed64iv = v45.glGetVertexArrayIndexed64iv;
				glCreateSamplers = v45.glCreateSamplers;
				glCreateProgramPipelines = v45.glCreateProgramPipelines;
				glCreateQueries = v45.glCreateQueries;
				glGetQueryBufferObjecti64v = v45.glGetQueryBufferObjecti64v;
				glGetQueryBufferObjectiv = v45.glGetQueryBufferObjectiv;
				glGetQueryBufferObjectui64v = v45.glGetQueryBufferObjectui64v;
				glGetQueryBufferObjectuiv = v45.glGetQueryBufferObjectuiv;
				glMemoryBarrierByRegion = v45.glMemoryBarrierByRegion;
				glGetTextureSubImage = v45.glGetTextureSubImage;
				glGetCompressedTextureSubImage = v45.glGetCompressedTextureSubImage;
				glGetGraphicsResetStatus = v45.glGetGraphicsResetStatus;
				glGetnCompressedTexImage = v45.glGetnCompressedTexImage;
				glGetnTexImage = v45.glGetnTexImage;
				glGetnUniformdv = v45.glGetnUniformdv;
				glGetnUniformfv = v45.glGetnUniformfv;
				glGetnUniformiv = v45.glGetnUniformiv;
				glGetnUniformuiv = v45.glGetnUniformuiv;
				glReadnPixels = v45.glReadnPixels;
				glGetnMapdv = v45.glGetnMapdv;
				glGetnMapfv = v45.glGetnMapfv;
				glGetnMapiv = v45.glGetnMapiv;
				glGetnPixelMapfv = v45.glGetnPixelMapfv;
				glGetnPixelMapuiv = v45.glGetnPixelMapuiv;
				glGetnPixelMapusv = v45.glGetnPixelMapusv;
				glGetnPolygonStipple = v45.glGetnPolygonStipple;
				glGetnColorTable = v45.glGetnColorTable;
				glGetnConvolutionFilter = v45.glGetnConvolutionFilter;
				glGetnSeparableFilter = v45.glGetnSeparableFilter;
				glGetnHistogram = v45.glGetnHistogram;
				glGetnMinmax = v45.glGetnMinmax;
				glTextureBarrier = v45.glTextureBarrier;

			}
			//save all current opengl function pointers (called once, after extension loading)
			void saveCurrent() {
				//v10
				v10.glCullFace = glCullFace;
				v10.glFrontFace = glFrontFace;
				v10.glHint = glHint;
				v10.glLineWidth = glLineWidth;
				v10.glPointSize = glPointSize;
				v10.glPolygonMode = glPolygonMode;
				v10.glScissor = glScissor;
				v10.glTexParameterf = glTexParameterf;
				v10.glTexParameterfv = glTexParameterfv;
				v10.glTexParameteri = glTexParameteri;
				v10.glTexParameteriv = glTexParameteriv;
				v10.glTexImage1D = glTexImage1D;
				v10.glTexImage2D = glTexImage2D;
				v10.glDrawBuffer = glDrawBuffer;
				v10.glClear = glClear;
				v10.glClearColor = glClearColor;
				v10.glClearStencil = glClearStencil;
				v10.glClearDepth = glClearDepth;
				v10.glStencilMask = glStencilMask;
				v10.glColorMask = glColorMask;
				v10.glDepthMask = glDepthMask;
				v10.glDisable = glDisable;
				v10.glEnable = glEnable;
				v10.glFinish = glFinish;
				v10.glFlush = glFlush;
				v10.glBlendFunc = glBlendFunc;
				v10.glLogicOp = glLogicOp;
				v10.glStencilFunc = glStencilFunc;
				v10.glStencilOp = glStencilOp;
				v10.glDepthFunc = glDepthFunc;
				v10.glPixelStoref = glPixelStoref;
				v10.glPixelStorei = glPixelStorei;
				v10.glReadBuffer = glReadBuffer;
				v10.glReadPixels = glReadPixels;
				v10.glGetBooleanv = glGetBooleanv;
				v10.glGetDoublev = glGetDoublev;
				v10.glGetError = glGetError;
				v10.glGetFloatv = glGetFloatv;
				v10.glGetIntegerv = glGetIntegerv;
				v10.glGetString = glGetString;
				v10.glGetTexImage = glGetTexImage;
				v10.glGetTexParameterfv = glGetTexParameterfv;
				v10.glGetTexParameteriv = glGetTexParameteriv;
				v10.glGetTexLevelParameterfv = glGetTexLevelParameterfv;
				v10.glGetTexLevelParameteriv = glGetTexLevelParameteriv;
				v10.glIsEnabled = glIsEnabled;
				v10.glDepthRange = glDepthRange;
				v10.glViewport = glViewport;
				v10.glNewList = glNewList;
				v10.glEndList = glEndList;
				v10.glCallList = glCallList;
				v10.glCallLists = glCallLists;
				v10.glDeleteLists = glDeleteLists;
				v10.glGenLists = glGenLists;
				v10.glListBase = glListBase;
				v10.glBegin = glBegin;
				v10.glBitmap = glBitmap;
				v10.glColor3b = glColor3b;
				v10.glColor3bv = glColor3bv;
				v10.glColor3d = glColor3d;
				v10.glColor3dv = glColor3dv;
				v10.glColor3f = glColor3f;
				v10.glColor3fv = glColor3fv;
				v10.glColor3i = glColor3i;
				v10.glColor3iv = glColor3iv;
				v10.glColor3s = glColor3s;
				v10.glColor3sv = glColor3sv;
				v10.glColor3ub = glColor3ub;
				v10.glColor3ubv = glColor3ubv;
				v10.glColor3ui = glColor3ui;
				v10.glColor3uiv = glColor3uiv;
				v10.glColor3us = glColor3us;
				v10.glColor3usv = glColor3usv;
				v10.glColor4b = glColor4b;
				v10.glColor4bv = glColor4bv;
				v10.glColor4d = glColor4d;
				v10.glColor4dv = glColor4dv;
				v10.glColor4f = glColor4f;
				v10.glColor4fv = glColor4fv;
				v10.glColor4i = glColor4i;
				v10.glColor4iv = glColor4iv;
				v10.glColor4s = glColor4s;
				v10.glColor4sv = glColor4sv;
				v10.glColor4ub = glColor4ub;
				v10.glColor4ubv = glColor4ubv;
				v10.glColor4ui = glColor4ui;
				v10.glColor4uiv = glColor4uiv;
				v10.glColor4us = glColor4us;
				v10.glColor4usv = glColor4usv;
				v10.glEdgeFlag = glEdgeFlag;
				v10.glEdgeFlagv = glEdgeFlagv;
				v10.glEnd = glEnd;
				v10.glIndexd = glIndexd;
				v10.glIndexdv = glIndexdv;
				v10.glIndexf = glIndexf;
				v10.glIndexfv = glIndexfv;
				v10.glIndexi = glIndexi;
				v10.glIndexiv = glIndexiv;
				v10.glIndexs = glIndexs;
				v10.glIndexsv = glIndexsv;
				v10.glNormal3b = glNormal3b;
				v10.glNormal3bv = glNormal3bv;
				v10.glNormal3d = glNormal3d;
				v10.glNormal3dv = glNormal3dv;
				v10.glNormal3f = glNormal3f;
				v10.glNormal3fv = glNormal3fv;
				v10.glNormal3i = glNormal3i;
				v10.glNormal3iv = glNormal3iv;
				v10.glNormal3s = glNormal3s;
				v10.glNormal3sv = glNormal3sv;
				v10.glRasterPos2d = glRasterPos2d;
				v10.glRasterPos2dv = glRasterPos2dv;
				v10.glRasterPos2f = glRasterPos2f;
				v10.glRasterPos2fv = glRasterPos2fv;
				v10.glRasterPos2i = glRasterPos2i;
				v10.glRasterPos2iv = glRasterPos2iv;
				v10.glRasterPos2s = glRasterPos2s;
				v10.glRasterPos2sv = glRasterPos2sv;
				v10.glRasterPos3d = glRasterPos3d;
				v10.glRasterPos3dv = glRasterPos3dv;
				v10.glRasterPos3f = glRasterPos3f;
				v10.glRasterPos3fv = glRasterPos3fv;
				v10.glRasterPos3i = glRasterPos3i;
				v10.glRasterPos3iv = glRasterPos3iv;
				v10.glRasterPos3s = glRasterPos3s;
				v10.glRasterPos3sv = glRasterPos3sv;
				v10.glRasterPos4d = glRasterPos4d;
				v10.glRasterPos4dv = glRasterPos4dv;
				v10.glRasterPos4f = glRasterPos4f;
				v10.glRasterPos4fv = glRasterPos4fv;
				v10.glRasterPos4i = glRasterPos4i;
				v10.glRasterPos4iv = glRasterPos4iv;
				v10.glRasterPos4s = glRasterPos4s;
				v10.glRasterPos4sv = glRasterPos4sv;
				v10.glRectd = glRectd;
				v10.glRectdv = glRectdv;
				v10.glRectf = glRectf;
				v10.glRectfv = glRectfv;
				v10.glRecti = glRecti;
				v10.glRectiv = glRectiv;
				v10.glRects = glRects;
				v10.glRectsv = glRectsv;
				v10.glTexCoord1d = glTexCoord1d;
				v10.glTexCoord1dv = glTexCoord1dv;
				v10.glTexCoord1f = glTexCoord1f;
				v10.glTexCoord1fv = glTexCoord1fv;
				v10.glTexCoord1i = glTexCoord1i;
				v10.glTexCoord1iv = glTexCoord1iv;
				v10.glTexCoord1s = glTexCoord1s;
				v10.glTexCoord1sv = glTexCoord1sv;
				v10.glTexCoord2d = glTexCoord2d;
				v10.glTexCoord2dv = glTexCoord2dv;
				v10.glTexCoord2f = glTexCoord2f;
				v10.glTexCoord2fv = glTexCoord2fv;
				v10.glTexCoord2i = glTexCoord2i;
				v10.glTexCoord2iv = glTexCoord2iv;
				v10.glTexCoord2s = glTexCoord2s;
				v10.glTexCoord2sv = glTexCoord2sv;
				v10.glTexCoord3d = glTexCoord3d;
				v10.glTexCoord3dv = glTexCoord3dv;
				v10.glTexCoord3f = glTexCoord3f;
				v10.glTexCoord3fv = glTexCoord3fv;
				v10.glTexCoord3i = glTexCoord3i;
				v10.glTexCoord3iv = glTexCoord3iv;
				v10.glTexCoord3s = glTexCoord3s;
				v10.glTexCoord3sv = glTexCoord3sv;
				v10.glTexCoord4d = glTexCoord4d;
				v10.glTexCoord4dv = glTexCoord4dv;
				v10.glTexCoord4f = glTexCoord4f;
				v10.glTexCoord4fv = glTexCoord4fv;
				v10.glTexCoord4i = glTexCoord4i;
				v10.glTexCoord4iv = glTexCoord4iv;
				v10.glTexCoord4s = glTexCoord4s;
				v10.glTexCoord4sv = glTexCoord4sv;
				v10.glVertex2d = glVertex2d;
				v10.glVertex2dv = glVertex2dv;
				v10.glVertex2f = glVertex2f;
				v10.glVertex2fv = glVertex2fv;
				v10.glVertex2i = glVertex2i;
				v10.glVertex2iv = glVertex2iv;
				v10.glVertex2s = glVertex2s;
				v10.glVertex2sv = glVertex2sv;
				v10.glVertex3d = glVertex3d;
				v10.glVertex3dv = glVertex3dv;
				v10.glVertex3f = glVertex3f;
				v10.glVertex3fv = glVertex3fv;
				v10.glVertex3i = glVertex3i;
				v10.glVertex3iv = glVertex3iv;
				v10.glVertex3s = glVertex3s;
				v10.glVertex3sv = glVertex3sv;
				v10.glVertex4d = glVertex4d;
				v10.glVertex4dv = glVertex4dv;
				v10.glVertex4f = glVertex4f;
				v10.glVertex4fv = glVertex4fv;
				v10.glVertex4i = glVertex4i;
				v10.glVertex4iv = glVertex4iv;
				v10.glVertex4s = glVertex4s;
				v10.glVertex4sv = glVertex4sv;
				v10.glClipPlane = glClipPlane;
				v10.glColorMaterial = glColorMaterial;
				v10.glFogf = glFogf;
				v10.glFogfv = glFogfv;
				v10.glFogi = glFogi;
				v10.glFogiv = glFogiv;
				v10.glLightf = glLightf;
				v10.glLightfv = glLightfv;
				v10.glLighti = glLighti;
				v10.glLightiv = glLightiv;
				v10.glLightModelf = glLightModelf;
				v10.glLightModelfv = glLightModelfv;
				v10.glLightModeli = glLightModeli;
				v10.glLightModeliv = glLightModeliv;
				v10.glLineStipple = glLineStipple;
				v10.glMaterialf = glMaterialf;
				v10.glMaterialfv = glMaterialfv;
				v10.glMateriali = glMateriali;
				v10.glMaterialiv = glMaterialiv;
				v10.glPolygonStipple = glPolygonStipple;
				v10.glShadeModel = glShadeModel;
				v10.glTexEnvf = glTexEnvf;
				v10.glTexEnvfv = glTexEnvfv;
				v10.glTexEnvi = glTexEnvi;
				v10.glTexEnviv = glTexEnviv;
				v10.glTexGend = glTexGend;
				v10.glTexGendv = glTexGendv;
				v10.glTexGenf = glTexGenf;
				v10.glTexGenfv = glTexGenfv;
				v10.glTexGeni = glTexGeni;
				v10.glTexGeniv = glTexGeniv;
				v10.glFeedbackBuffer = glFeedbackBuffer;
				v10.glSelectBuffer = glSelectBuffer;
				v10.glRenderMode = glRenderMode;
				v10.glInitNames = glInitNames;
				v10.glLoadName = glLoadName;
				v10.glPassThrough = glPassThrough;
				v10.glPopName = glPopName;
				v10.glPushName = glPushName;
				v10.glClearAccum = glClearAccum;
				v10.glClearIndex = glClearIndex;
				v10.glIndexMask = glIndexMask;
				v10.glAccum = glAccum;
				v10.glPopAttrib = glPopAttrib;
				v10.glPushAttrib = glPushAttrib;
				v10.glMap1d = glMap1d;
				v10.glMap1f = glMap1f;
				v10.glMap2d = glMap2d;
				v10.glMap2f = glMap2f;
				v10.glMapGrid1d = glMapGrid1d;
				v10.glMapGrid1f = glMapGrid1f;
				v10.glMapGrid2d = glMapGrid2d;
				v10.glMapGrid2f = glMapGrid2f;
				v10.glEvalCoord1d = glEvalCoord1d;
				v10.glEvalCoord1dv = glEvalCoord1dv;
				v10.glEvalCoord1f = glEvalCoord1f;
				v10.glEvalCoord1fv = glEvalCoord1fv;
				v10.glEvalCoord2d = glEvalCoord2d;
				v10.glEvalCoord2dv = glEvalCoord2dv;
				v10.glEvalCoord2f = glEvalCoord2f;
				v10.glEvalCoord2fv = glEvalCoord2fv;
				v10.glEvalMesh1 = glEvalMesh1;
				v10.glEvalPoint1 = glEvalPoint1;
				v10.glEvalMesh2 = glEvalMesh2;
				v10.glEvalPoint2 = glEvalPoint2;
				v10.glAlphaFunc = glAlphaFunc;
				v10.glPixelZoom = glPixelZoom;
				v10.glPixelTransferf = glPixelTransferf;
				v10.glPixelTransferi = glPixelTransferi;
				v10.glPixelMapfv = glPixelMapfv;
				v10.glPixelMapuiv = glPixelMapuiv;
				v10.glPixelMapusv = glPixelMapusv;
				v10.glCopyPixels = glCopyPixels;
				v10.glDrawPixels = glDrawPixels;
				v10.glGetClipPlane = glGetClipPlane;
				v10.glGetLightfv = glGetLightfv;
				v10.glGetLightiv = glGetLightiv;
				v10.glGetMapdv = glGetMapdv;
				v10.glGetMapfv = glGetMapfv;
				v10.glGetMapiv = glGetMapiv;
				v10.glGetMaterialfv = glGetMaterialfv;
				v10.glGetMaterialiv = glGetMaterialiv;
				v10.glGetPixelMapfv = glGetPixelMapfv;
				v10.glGetPixelMapuiv = glGetPixelMapuiv;
				v10.glGetPixelMapusv = glGetPixelMapusv;
				v10.glGetPolygonStipple = glGetPolygonStipple;
				v10.glGetTexEnvfv = glGetTexEnvfv;
				v10.glGetTexEnviv = glGetTexEnviv;
				v10.glGetTexGendv = glGetTexGendv;
				v10.glGetTexGenfv = glGetTexGenfv;
				v10.glGetTexGeniv = glGetTexGeniv;
				v10.glIsList = glIsList;
				v10.glFrustum = glFrustum;
				v10.glLoadIdentity = glLoadIdentity;
				v10.glLoadMatrixf = glLoadMatrixf;
				v10.glLoadMatrixd = glLoadMatrixd;
				v10.glMatrixMode = glMatrixMode;
				v10.glMultMatrixf = glMultMatrixf;
				v10.glMultMatrixd = glMultMatrixd;
				v10.glOrtho = glOrtho;
				v10.glPopMatrix = glPopMatrix;
				v10.glPushMatrix = glPushMatrix;
				v10.glRotated = glRotated;
				v10.glRotatef = glRotatef;
				v10.glScaled = glScaled;
				v10.glScalef = glScalef;
				v10.glTranslated = glTranslated;
				v10.glTranslatef = glTranslatef;

				//v11
				v11.glDrawArrays = glDrawArrays;
				v11.glDrawElements = glDrawElements;
				v11.glPolygonOffset = glPolygonOffset;
				v11.glCopyTexImage1D = glCopyTexImage1D;
				v11.glCopyTexImage2D = glCopyTexImage2D;
				v11.glCopyTexSubImage1D = glCopyTexSubImage1D;
				v11.glCopyTexSubImage2D = glCopyTexSubImage2D;
				v11.glTexSubImage1D = glTexSubImage1D;
				v11.glTexSubImage2D = glTexSubImage2D;
				v11.glBindTexture = glBindTexture;
				v11.glDeleteTextures = glDeleteTextures;
				v11.glGenTextures = glGenTextures;
				v11.glIsTexture = glIsTexture;
				v11.glArrayElement = glArrayElement;
				v11.glColorPointer = glColorPointer;
				v11.glDisableClientState = glDisableClientState;
				v11.glEdgeFlagPointer = glEdgeFlagPointer;
				v11.glEnableClientState = glEnableClientState;
				v11.glIndexPointer = glIndexPointer;
				v11.glInterleavedArrays = glInterleavedArrays;
				v11.glNormalPointer = glNormalPointer;
				v11.glTexCoordPointer = glTexCoordPointer;
				v11.glVertexPointer = glVertexPointer;
				v11.glAreTexturesResident = glAreTexturesResident;
				v11.glPrioritizeTextures = glPrioritizeTextures;
				v11.glIndexub = glIndexub;
				v11.glIndexubv = glIndexubv;
				v11.glPopClientAttrib = glPopClientAttrib;
				v11.glPushClientAttrib = glPushClientAttrib;

				//v12
				v12.glDrawRangeElements = glDrawRangeElements;
				v12.glTexImage3D = glTexImage3D;
				v12.glTexSubImage3D = glTexSubImage3D;
				v12.glCopyTexSubImage3D = glCopyTexSubImage3D;

				//v13
				v13.glActiveTexture = glActiveTexture;
				v13.glSampleCoverage = glSampleCoverage;
				v13.glCompressedTexImage3D = glCompressedTexImage3D;
				v13.glCompressedTexImage2D = glCompressedTexImage2D;
				v13.glCompressedTexImage1D = glCompressedTexImage1D;
				v13.glCompressedTexSubImage3D = glCompressedTexSubImage3D;
				v13.glCompressedTexSubImage2D = glCompressedTexSubImage2D;
				v13.glCompressedTexSubImage1D = glCompressedTexSubImage1D;
				v13.glGetCompressedTexImage = glGetCompressedTexImage;
				v13.glClientActiveTexture = glClientActiveTexture;
				v13.glMultiTexCoord1d = glMultiTexCoord1d;
				v13.glMultiTexCoord1dv = glMultiTexCoord1dv;
				v13.glMultiTexCoord1f = glMultiTexCoord1f;
				v13.glMultiTexCoord1fv = glMultiTexCoord1fv;
				v13.glMultiTexCoord1i = glMultiTexCoord1i;
				v13.glMultiTexCoord1iv = glMultiTexCoord1iv;
				v13.glMultiTexCoord1s = glMultiTexCoord1s;
				v13.glMultiTexCoord1sv = glMultiTexCoord1sv;
				v13.glMultiTexCoord2d = glMultiTexCoord2d;
				v13.glMultiTexCoord2dv = glMultiTexCoord2dv;
				v13.glMultiTexCoord2f = glMultiTexCoord2f;
				v13.glMultiTexCoord2fv = glMultiTexCoord2fv;
				v13.glMultiTexCoord2i = glMultiTexCoord2i;
				v13.glMultiTexCoord2iv = glMultiTexCoord2iv;
				v13.glMultiTexCoord2s = glMultiTexCoord2s;
				v13.glMultiTexCoord2sv = glMultiTexCoord2sv;
				v13.glMultiTexCoord3d = glMultiTexCoord3d;
				v13.glMultiTexCoord3dv = glMultiTexCoord3dv;
				v13.glMultiTexCoord3f = glMultiTexCoord3f;
				v13.glMultiTexCoord3fv = glMultiTexCoord3fv;
				v13.glMultiTexCoord3i = glMultiTexCoord3i;
				v13.glMultiTexCoord3iv = glMultiTexCoord3iv;
				v13.glMultiTexCoord3s = glMultiTexCoord3s;
				v13.glMultiTexCoord3sv = glMultiTexCoord3sv;
				v13.glMultiTexCoord4d = glMultiTexCoord4d;
				v13.glMultiTexCoord4dv = glMultiTexCoord4dv;
				v13.glMultiTexCoord4f = glMultiTexCoord4f;
				v13.glMultiTexCoord4fv = glMultiTexCoord4fv;
				v13.glMultiTexCoord4i = glMultiTexCoord4i;
				v13.glMultiTexCoord4iv = glMultiTexCoord4iv;
				v13.glMultiTexCoord4s = glMultiTexCoord4s;
				v13.glMultiTexCoord4sv = glMultiTexCoord4sv;
				v13.glLoadTransposeMatrixf = glLoadTransposeMatrixf;
				v13.glLoadTransposeMatrixd = glLoadTransposeMatrixd;
				v13.glMultTransposeMatrixf = glMultTransposeMatrixf;
				v13.glMultTransposeMatrixd = glMultTransposeMatrixd;

				//v14
				v14.glBlendFuncSeparate = glBlendFuncSeparate;
				v14.glMultiDrawArrays = glMultiDrawArrays;
				v14.glMultiDrawElements = glMultiDrawElements;
				v14.glPointParameterf = glPointParameterf;
				v14.glPointParameterfv = glPointParameterfv;
				v14.glPointParameteri = glPointParameteri;
				v14.glPointParameteriv = glPointParameteriv;
				v14.glFogCoordf = glFogCoordf;
				v14.glFogCoordfv = glFogCoordfv;
				v14.glFogCoordd = glFogCoordd;
				v14.glFogCoorddv = glFogCoorddv;
				v14.glFogCoordPointer = glFogCoordPointer;
				v14.glSecondaryColor3b = glSecondaryColor3b;
				v14.glSecondaryColor3bv = glSecondaryColor3bv;
				v14.glSecondaryColor3d = glSecondaryColor3d;
				v14.glSecondaryColor3dv = glSecondaryColor3dv;
				v14.glSecondaryColor3f = glSecondaryColor3f;
				v14.glSecondaryColor3fv = glSecondaryColor3fv;
				v14.glSecondaryColor3i = glSecondaryColor3i;
				v14.glSecondaryColor3iv = glSecondaryColor3iv;
				v14.glSecondaryColor3s = glSecondaryColor3s;
				v14.glSecondaryColor3sv = glSecondaryColor3sv;
				v14.glSecondaryColor3ub = glSecondaryColor3ub;
				v14.glSecondaryColor3ubv = glSecondaryColor3ubv;
				v14.glSecondaryColor3ui = glSecondaryColor3ui;
				v14.glSecondaryColor3uiv = glSecondaryColor3uiv;
				v14.glSecondaryColor3us = glSecondaryColor3us;
				v14.glSecondaryColor3usv = glSecondaryColor3usv;
				v14.glSecondaryColorPointer = glSecondaryColorPointer;
				v14.glWindowPos2d = glWindowPos2d;
				v14.glWindowPos2dv = glWindowPos2dv;
				v14.glWindowPos2f = glWindowPos2f;
				v14.glWindowPos2fv = glWindowPos2fv;
				v14.glWindowPos2i = glWindowPos2i;
				v14.glWindowPos2iv = glWindowPos2iv;
				v14.glWindowPos2s = glWindowPos2s;
				v14.glWindowPos2sv = glWindowPos2sv;
				v14.glWindowPos3d = glWindowPos3d;
				v14.glWindowPos3dv = glWindowPos3dv;
				v14.glWindowPos3f = glWindowPos3f;
				v14.glWindowPos3fv = glWindowPos3fv;
				v14.glWindowPos3i = glWindowPos3i;
				v14.glWindowPos3iv = glWindowPos3iv;
				v14.glWindowPos3s = glWindowPos3s;
				v14.glWindowPos3sv = glWindowPos3sv;
				v14.glBlendColor = glBlendColor;
				v14.glBlendEquation = glBlendEquation;

				//v15
				v15.glGenQueries = glGenQueries;
				v15.glDeleteQueries = glDeleteQueries;
				v15.glIsQuery = glIsQuery;
				v15.glBeginQuery = glBeginQuery;
				v15.glEndQuery = glEndQuery;
				v15.glGetQueryiv = glGetQueryiv;
				v15.glGetQueryObjectiv = glGetQueryObjectiv;
				v15.glGetQueryObjectuiv = glGetQueryObjectuiv;
				v15.glBindBuffer = glBindBuffer;
				v15.glDeleteBuffers = glDeleteBuffers;
				v15.glGenBuffers = glGenBuffers;
				v15.glIsBuffer = glIsBuffer;
				v15.glBufferData = glBufferData;
				v15.glBufferSubData = glBufferSubData;
				v15.glGetBufferSubData = glGetBufferSubData;
				v15.glMapBuffer = glMapBuffer;
				v15.glUnmapBuffer = glUnmapBuffer;
				v15.glGetBufferParameteriv = glGetBufferParameteriv;
				v15.glGetBufferPointerv = glGetBufferPointerv;

				//v20
				v20.glBlendEquationSeparate = glBlendEquationSeparate;
				v20.glDrawBuffers = glDrawBuffers;
				v20.glStencilOpSeparate = glStencilOpSeparate;
				v20.glStencilFuncSeparate = glStencilFuncSeparate;
				v20.glStencilMaskSeparate = glStencilMaskSeparate;
				v20.glAttachShader = glAttachShader;
				v20.glBindAttribLocation = glBindAttribLocation;
				v20.glCompileShader = glCompileShader;
				v20.glCreateProgram = glCreateProgram;
				v20.glCreateShader = glCreateShader;
				v20.glDeleteProgram = glDeleteProgram;
				v20.glDeleteShader = glDeleteShader;
				v20.glDetachShader = glDetachShader;
				v20.glDisableVertexAttribArray = glDisableVertexAttribArray;
				v20.glEnableVertexAttribArray = glEnableVertexAttribArray;
				v20.glGetActiveAttrib = glGetActiveAttrib;
				v20.glGetActiveUniform = glGetActiveUniform;
				v20.glGetAttachedShaders = glGetAttachedShaders;
				v20.glGetAttribLocation = glGetAttribLocation;
				v20.glGetProgramiv = glGetProgramiv;
				v20.glGetProgramInfoLog = glGetProgramInfoLog;
				v20.glGetShaderiv = glGetShaderiv;
				v20.glGetShaderInfoLog = glGetShaderInfoLog;
				v20.glGetShaderSource = glGetShaderSource;
				v20.glGetUniformLocation = glGetUniformLocation;
				v20.glGetUniformfv = glGetUniformfv;
				v20.glGetUniformiv = glGetUniformiv;
				v20.glGetVertexAttribdv = glGetVertexAttribdv;
				v20.glGetVertexAttribfv = glGetVertexAttribfv;
				v20.glGetVertexAttribiv = glGetVertexAttribiv;
				v20.glGetVertexAttribPointerv = glGetVertexAttribPointerv;
				v20.glIsProgram = glIsProgram;
				v20.glIsShader = glIsShader;
				v20.glLinkProgram = glLinkProgram;
				v20.glShaderSource = glShaderSource;
				v20.glUseProgram = glUseProgram;
				v20.glUniform1f = glUniform1f;
				v20.glUniform2f = glUniform2f;
				v20.glUniform3f = glUniform3f;
				v20.glUniform4f = glUniform4f;
				v20.glUniform1i = glUniform1i;
				v20.glUniform2i = glUniform2i;
				v20.glUniform3i = glUniform3i;
				v20.glUniform4i = glUniform4i;
				v20.glUniform1fv = glUniform1fv;
				v20.glUniform2fv = glUniform2fv;
				v20.glUniform3fv = glUniform3fv;
				v20.glUniform4fv = glUniform4fv;
				v20.glUniform1iv = glUniform1iv;
				v20.glUniform2iv = glUniform2iv;
				v20.glUniform3iv = glUniform3iv;
				v20.glUniform4iv = glUniform4iv;
				v20.glUniformMatrix2fv = glUniformMatrix2fv;
				v20.glUniformMatrix3fv = glUniformMatrix3fv;
				v20.glUniformMatrix4fv = glUniformMatrix4fv;
				v20.glValidateProgram = glValidateProgram;
				v20.glVertexAttrib1d = glVertexAttrib1d;
				v20.glVertexAttrib1dv = glVertexAttrib1dv;
				v20.glVertexAttrib1f = glVertexAttrib1f;
				v20.glVertexAttrib1fv = glVertexAttrib1fv;
				v20.glVertexAttrib1s = glVertexAttrib1s;
				v20.glVertexAttrib1sv = glVertexAttrib1sv;
				v20.glVertexAttrib2d = glVertexAttrib2d;
				v20.glVertexAttrib2dv = glVertexAttrib2dv;
				v20.glVertexAttrib2f = glVertexAttrib2f;
				v20.glVertexAttrib2fv = glVertexAttrib2fv;
				v20.glVertexAttrib2s = glVertexAttrib2s;
				v20.glVertexAttrib2sv = glVertexAttrib2sv;
				v20.glVertexAttrib3d = glVertexAttrib3d;
				v20.glVertexAttrib3dv = glVertexAttrib3dv;
				v20.glVertexAttrib3f = glVertexAttrib3f;
				v20.glVertexAttrib3fv = glVertexAttrib3fv;
				v20.glVertexAttrib3s = glVertexAttrib3s;
				v20.glVertexAttrib3sv = glVertexAttrib3sv;
				v20.glVertexAttrib4Nbv = glVertexAttrib4Nbv;
				v20.glVertexAttrib4Niv = glVertexAttrib4Niv;
				v20.glVertexAttrib4Nsv = glVertexAttrib4Nsv;
				v20.glVertexAttrib4Nub = glVertexAttrib4Nub;
				v20.glVertexAttrib4Nubv = glVertexAttrib4Nubv;
				v20.glVertexAttrib4Nuiv = glVertexAttrib4Nuiv;
				v20.glVertexAttrib4Nusv = glVertexAttrib4Nusv;
				v20.glVertexAttrib4bv = glVertexAttrib4bv;
				v20.glVertexAttrib4d = glVertexAttrib4d;
				v20.glVertexAttrib4dv = glVertexAttrib4dv;
				v20.glVertexAttrib4f = glVertexAttrib4f;
				v20.glVertexAttrib4fv = glVertexAttrib4fv;
				v20.glVertexAttrib4iv = glVertexAttrib4iv;
				v20.glVertexAttrib4s = glVertexAttrib4s;
				v20.glVertexAttrib4sv = glVertexAttrib4sv;
				v20.glVertexAttrib4ubv = glVertexAttrib4ubv;
				v20.glVertexAttrib4uiv = glVertexAttrib4uiv;
				v20.glVertexAttrib4usv = glVertexAttrib4usv;
				v20.glVertexAttribPointer = glVertexAttribPointer;

				//v21
				v21.glUniformMatrix2x3fv = glUniformMatrix2x3fv;
				v21.glUniformMatrix3x2fv = glUniformMatrix3x2fv;
				v21.glUniformMatrix2x4fv = glUniformMatrix2x4fv;
				v21.glUniformMatrix4x2fv = glUniformMatrix4x2fv;
				v21.glUniformMatrix3x4fv = glUniformMatrix3x4fv;
				v21.glUniformMatrix4x3fv = glUniformMatrix4x3fv;

				//v30
				v30.glColorMaski = glColorMaski;
				v30.glGetBooleani_v = glGetBooleani_v;
				v30.glGetIntegeri_v = glGetIntegeri_v;
				v30.glEnablei = glEnablei;
				v30.glDisablei = glDisablei;
				v30.glIsEnabledi = glIsEnabledi;
				v30.glBeginTransformFeedback = glBeginTransformFeedback;
				v30.glEndTransformFeedback = glEndTransformFeedback;
				v30.glBindBufferRange = glBindBufferRange;
				v30.glBindBufferBase = glBindBufferBase;
				v30.glTransformFeedbackVaryings = glTransformFeedbackVaryings;
				v30.glGetTransformFeedbackVarying = glGetTransformFeedbackVarying;
				v30.glClampColor = glClampColor;
				v30.glBeginConditionalRender = glBeginConditionalRender;
				v30.glEndConditionalRender = glEndConditionalRender;
				v30.glVertexAttribIPointer = glVertexAttribIPointer;
				v30.glGetVertexAttribIiv = glGetVertexAttribIiv;
				v30.glGetVertexAttribIuiv = glGetVertexAttribIuiv;
				v30.glVertexAttribI1i = glVertexAttribI1i;
				v30.glVertexAttribI2i = glVertexAttribI2i;
				v30.glVertexAttribI3i = glVertexAttribI3i;
				v30.glVertexAttribI4i = glVertexAttribI4i;
				v30.glVertexAttribI1ui = glVertexAttribI1ui;
				v30.glVertexAttribI2ui = glVertexAttribI2ui;
				v30.glVertexAttribI3ui = glVertexAttribI3ui;
				v30.glVertexAttribI4ui = glVertexAttribI4ui;
				v30.glVertexAttribI1iv = glVertexAttribI1iv;
				v30.glVertexAttribI2iv = glVertexAttribI2iv;
				v30.glVertexAttribI3iv = glVertexAttribI3iv;
				v30.glVertexAttribI4iv = glVertexAttribI4iv;
				v30.glVertexAttribI1uiv = glVertexAttribI1uiv;
				v30.glVertexAttribI2uiv = glVertexAttribI2uiv;
				v30.glVertexAttribI3uiv = glVertexAttribI3uiv;
				v30.glVertexAttribI4uiv = glVertexAttribI4uiv;
				v30.glVertexAttribI4bv = glVertexAttribI4bv;
				v30.glVertexAttribI4sv = glVertexAttribI4sv;
				v30.glVertexAttribI4ubv = glVertexAttribI4ubv;
				v30.glVertexAttribI4usv = glVertexAttribI4usv;
				v30.glGetUniformuiv = glGetUniformuiv;
				v30.glBindFragDataLocation = glBindFragDataLocation;
				v30.glGetFragDataLocation = glGetFragDataLocation;
				v30.glUniform1ui = glUniform1ui;
				v30.glUniform2ui = glUniform2ui;
				v30.glUniform3ui = glUniform3ui;
				v30.glUniform4ui = glUniform4ui;
				v30.glUniform1uiv = glUniform1uiv;
				v30.glUniform2uiv = glUniform2uiv;
				v30.glUniform3uiv = glUniform3uiv;
				v30.glUniform4uiv = glUniform4uiv;
				v30.glTexParameterIiv = glTexParameterIiv;
				v30.glTexParameterIuiv = glTexParameterIuiv;
				v30.glGetTexParameterIiv = glGetTexParameterIiv;
				v30.glGetTexParameterIuiv = glGetTexParameterIuiv;
				v30.glClearBufferiv = glClearBufferiv;
				v30.glClearBufferuiv = glClearBufferuiv;
				v30.glClearBufferfv = glClearBufferfv;
				v30.glClearBufferfi = glClearBufferfi;
				v30.glGetStringi = glGetStringi;
				v30.glIsRenderbuffer = glIsRenderbuffer;
				v30.glBindRenderbuffer = glBindRenderbuffer;
				v30.glDeleteRenderbuffers = glDeleteRenderbuffers;
				v30.glGenRenderbuffers = glGenRenderbuffers;
				v30.glRenderbufferStorage = glRenderbufferStorage;
				v30.glGetRenderbufferParameteriv = glGetRenderbufferParameteriv;
				v30.glIsFramebuffer = glIsFramebuffer;
				v30.glBindFramebuffer = glBindFramebuffer;
				v30.glDeleteFramebuffers = glDeleteFramebuffers;
				v30.glGenFramebuffers = glGenFramebuffers;
				v30.glCheckFramebufferStatus = glCheckFramebufferStatus;
				v30.glFramebufferTexture1D = glFramebufferTexture1D;
				v30.glFramebufferTexture2D = glFramebufferTexture2D;
				v30.glFramebufferTexture3D = glFramebufferTexture3D;
				v30.glFramebufferRenderbuffer = glFramebufferRenderbuffer;
				v30.glGetFramebufferAttachmentParameteriv = glGetFramebufferAttachmentParameteriv;
				v30.glGenerateMipmap = glGenerateMipmap;
				v30.glBlitFramebuffer = glBlitFramebuffer;
				v30.glRenderbufferStorageMultisample = glRenderbufferStorageMultisample;
				v30.glFramebufferTextureLayer = glFramebufferTextureLayer;
				v30.glMapBufferRange = glMapBufferRange;
				v30.glFlushMappedBufferRange = glFlushMappedBufferRange;
				v30.glBindVertexArray = glBindVertexArray;
				v30.glDeleteVertexArrays = glDeleteVertexArrays;
				v30.glGenVertexArrays = glGenVertexArrays;
				v30.glIsVertexArray = glIsVertexArray;

				//v31
				v31.glDrawArraysInstanced = glDrawArraysInstanced;
				v31.glDrawElementsInstanced = glDrawElementsInstanced;
				v31.glTexBuffer = glTexBuffer;
				v31.glPrimitiveRestartIndex = glPrimitiveRestartIndex;
				v31.glCopyBufferSubData = glCopyBufferSubData;
				v31.glGetUniformIndices = glGetUniformIndices;
				v31.glGetActiveUniformsiv = glGetActiveUniformsiv;
				v31.glGetActiveUniformName = glGetActiveUniformName;
				v31.glGetUniformBlockIndex = glGetUniformBlockIndex;
				v31.glGetActiveUniformBlockiv = glGetActiveUniformBlockiv;
				v31.glGetActiveUniformBlockName = glGetActiveUniformBlockName;
				v31.glUniformBlockBinding = glUniformBlockBinding;

				//v32
				v32.glDrawElementsBaseVertex = glDrawElementsBaseVertex;
				v32.glDrawRangeElementsBaseVertex = glDrawRangeElementsBaseVertex;
				v32.glDrawElementsInstancedBaseVertex = glDrawElementsInstancedBaseVertex;
				v32.glMultiDrawElementsBaseVertex = glMultiDrawElementsBaseVertex;
				v32.glProvokingVertex = glProvokingVertex;
				v32.glFenceSync = glFenceSync;
				v32.glIsSync = glIsSync;
				v32.glDeleteSync = glDeleteSync;
				v32.glClientWaitSync = glClientWaitSync;
				v32.glWaitSync = glWaitSync;
				v32.glGetInteger64v = glGetInteger64v;
				v32.glGetSynciv = glGetSynciv;
				v32.glGetInteger64i_v = glGetInteger64i_v;
				v32.glGetBufferParameteri64v = glGetBufferParameteri64v;
				v32.glFramebufferTexture = glFramebufferTexture;
				v32.glTexImage2DMultisample = glTexImage2DMultisample;
				v32.glTexImage3DMultisample = glTexImage3DMultisample;
				v32.glGetMultisamplefv = glGetMultisamplefv;
				v32.glSampleMaski = glSampleMaski;
				v32.glVertexP2ui = glVertexP2ui;
				v32.glVertexP2uiv = glVertexP2uiv;
				v32.glVertexP3ui = glVertexP3ui;
				v32.glVertexP3uiv = glVertexP3uiv;
				v32.glVertexP4ui = glVertexP4ui;
				v32.glVertexP4uiv = glVertexP4uiv;
				v32.glTexCoordP1ui = glTexCoordP1ui;
				v32.glTexCoordP1uiv = glTexCoordP1uiv;
				v32.glTexCoordP2ui = glTexCoordP2ui;
				v32.glTexCoordP2uiv = glTexCoordP2uiv;
				v32.glTexCoordP3ui = glTexCoordP3ui;
				v32.glTexCoordP3uiv = glTexCoordP3uiv;
				v32.glTexCoordP4ui = glTexCoordP4ui;
				v32.glTexCoordP4uiv = glTexCoordP4uiv;
				v32.glMultiTexCoordP1ui = glMultiTexCoordP1ui;
				v32.glMultiTexCoordP1uiv = glMultiTexCoordP1uiv;
				v32.glMultiTexCoordP2ui = glMultiTexCoordP2ui;
				v32.glMultiTexCoordP2uiv = glMultiTexCoordP2uiv;
				v32.glMultiTexCoordP3ui = glMultiTexCoordP3ui;
				v32.glMultiTexCoordP3uiv = glMultiTexCoordP3uiv;
				v32.glMultiTexCoordP4ui = glMultiTexCoordP4ui;
				v32.glMultiTexCoordP4uiv = glMultiTexCoordP4uiv;
				v32.glNormalP3ui = glNormalP3ui;
				v32.glNormalP3uiv = glNormalP3uiv;
				v32.glColorP3ui = glColorP3ui;
				v32.glColorP3uiv = glColorP3uiv;
				v32.glColorP4ui = glColorP4ui;
				v32.glColorP4uiv = glColorP4uiv;
				v32.glSecondaryColorP3ui = glSecondaryColorP3ui;
				v32.glSecondaryColorP3uiv = glSecondaryColorP3uiv;

				//v33
				v33.glBindFragDataLocationIndexed = glBindFragDataLocationIndexed;
				v33.glGetFragDataIndex = glGetFragDataIndex;
				v33.glGenSamplers = glGenSamplers;
				v33.glDeleteSamplers = glDeleteSamplers;
				v33.glIsSampler = glIsSampler;
				v33.glBindSampler = glBindSampler;
				v33.glSamplerParameteri = glSamplerParameteri;
				v33.glSamplerParameteriv = glSamplerParameteriv;
				v33.glSamplerParameterf = glSamplerParameterf;
				v33.glSamplerParameterfv = glSamplerParameterfv;
				v33.glSamplerParameterIiv = glSamplerParameterIiv;
				v33.glSamplerParameterIuiv = glSamplerParameterIuiv;
				v33.glGetSamplerParameteriv = glGetSamplerParameteriv;
				v33.glGetSamplerParameterIiv = glGetSamplerParameterIiv;
				v33.glGetSamplerParameterfv = glGetSamplerParameterfv;
				v33.glGetSamplerParameterIuiv = glGetSamplerParameterIuiv;
				v33.glQueryCounter = glQueryCounter;
				v33.glGetQueryObjecti64v = glGetQueryObjecti64v;
				v33.glGetQueryObjectui64v = glGetQueryObjectui64v;
				v33.glVertexAttribDivisor = glVertexAttribDivisor;
				v33.glVertexAttribP1ui = glVertexAttribP1ui;
				v33.glVertexAttribP1uiv = glVertexAttribP1uiv;
				v33.glVertexAttribP2ui = glVertexAttribP2ui;
				v33.glVertexAttribP2uiv = glVertexAttribP2uiv;
				v33.glVertexAttribP3ui = glVertexAttribP3ui;
				v33.glVertexAttribP3uiv = glVertexAttribP3uiv;
				v33.glVertexAttribP4ui = glVertexAttribP4ui;
				v33.glVertexAttribP4uiv = glVertexAttribP4uiv;


				//v40
				v40.glMinSampleShading = glMinSampleShading;
				v40.glBlendEquationi = glBlendEquationi;
				v40.glBlendEquationSeparatei = glBlendEquationSeparatei;
				v40.glBlendFunci = glBlendFunci;
				v40.glBlendFuncSeparatei = glBlendFuncSeparatei;
				v40.glDrawArraysIndirect = glDrawArraysIndirect;
				v40.glDrawElementsIndirect = glDrawElementsIndirect;
				v40.glUniform1d = glUniform1d;
				v40.glUniform2d = glUniform2d;
				v40.glUniform3d = glUniform3d;
				v40.glUniform4d = glUniform4d;
				v40.glUniform1dv = glUniform1dv;
				v40.glUniform2dv = glUniform2dv;
				v40.glUniform3dv = glUniform3dv;
				v40.glUniform4dv = glUniform4dv;
				v40.glUniformMatrix2dv = glUniformMatrix2dv;
				v40.glUniformMatrix3dv = glUniformMatrix3dv;
				v40.glUniformMatrix4dv = glUniformMatrix4dv;
				v40.glUniformMatrix2x3dv = glUniformMatrix2x3dv;
				v40.glUniformMatrix2x4dv = glUniformMatrix2x4dv;
				v40.glUniformMatrix3x2dv = glUniformMatrix3x2dv;
				v40.glUniformMatrix3x4dv = glUniformMatrix3x4dv;
				v40.glUniformMatrix4x2dv = glUniformMatrix4x2dv;
				v40.glUniformMatrix4x3dv = glUniformMatrix4x3dv;
				v40.glGetUniformdv = glGetUniformdv;
				v40.glGetSubroutineUniformLocation = glGetSubroutineUniformLocation;
				v40.glGetSubroutineIndex = glGetSubroutineIndex;
				v40.glGetActiveSubroutineUniformiv = glGetActiveSubroutineUniformiv;
				v40.glGetActiveSubroutineUniformName = glGetActiveSubroutineUniformName;
				v40.glGetActiveSubroutineName = glGetActiveSubroutineName;
				v40.glUniformSubroutinesuiv = glUniformSubroutinesuiv;
				v40.glGetUniformSubroutineuiv = glGetUniformSubroutineuiv;
				v40.glGetProgramStageiv = glGetProgramStageiv;
				v40.glPatchParameteri = glPatchParameteri;
				v40.glPatchParameterfv = glPatchParameterfv;
				v40.glBindTransformFeedback = glBindTransformFeedback;
				v40.glDeleteTransformFeedbacks = glDeleteTransformFeedbacks;
				v40.glGenTransformFeedbacks = glGenTransformFeedbacks;
				v40.glIsTransformFeedback = glIsTransformFeedback;
				v40.glPauseTransformFeedback = glPauseTransformFeedback;
				v40.glResumeTransformFeedback = glResumeTransformFeedback;
				v40.glDrawTransformFeedback = glDrawTransformFeedback;
				v40.glDrawTransformFeedbackStream = glDrawTransformFeedbackStream;
				v40.glBeginQueryIndexed = glBeginQueryIndexed;
				v40.glEndQueryIndexed = glEndQueryIndexed;
				v40.glGetQueryIndexediv = glGetQueryIndexediv;

				//v41
				v41.glReleaseShaderCompiler = glReleaseShaderCompiler;
				v41.glShaderBinary = glShaderBinary;
				v41.glGetShaderPrecisionFormat = glGetShaderPrecisionFormat;
				v41.glDepthRangef = glDepthRangef;
				v41.glClearDepthf = glClearDepthf;
				v41.glGetProgramBinary = glGetProgramBinary;
				v41.glProgramBinary = glProgramBinary;
				v41.glProgramParameteri = glProgramParameteri;
				v41.glUseProgramStages = glUseProgramStages;
				v41.glActiveShaderProgram = glActiveShaderProgram;
				v41.glCreateShaderProgramv = glCreateShaderProgramv;
				v41.glBindProgramPipeline = glBindProgramPipeline;
				v41.glDeleteProgramPipelines = glDeleteProgramPipelines;
				v41.glGenProgramPipelines = glGenProgramPipelines;
				v41.glIsProgramPipeline = glIsProgramPipeline;
				v41.glGetProgramPipelineiv = glGetProgramPipelineiv;
				v41.glProgramUniform1i = glProgramUniform1i;
				v41.glProgramUniform1iv = glProgramUniform1iv;
				v41.glProgramUniform1f = glProgramUniform1f;
				v41.glProgramUniform1fv = glProgramUniform1fv;
				v41.glProgramUniform1d = glProgramUniform1d;
				v41.glProgramUniform1dv = glProgramUniform1dv;
				v41.glProgramUniform1ui = glProgramUniform1ui;
				v41.glProgramUniform1uiv = glProgramUniform1uiv;
				v41.glProgramUniform2i = glProgramUniform2i;
				v41.glProgramUniform2iv = glProgramUniform2iv;
				v41.glProgramUniform2f = glProgramUniform2f;
				v41.glProgramUniform2fv = glProgramUniform2fv;
				v41.glProgramUniform2d = glProgramUniform2d;
				v41.glProgramUniform2dv = glProgramUniform2dv;
				v41.glProgramUniform2ui = glProgramUniform2ui;
				v41.glProgramUniform2uiv = glProgramUniform2uiv;
				v41.glProgramUniform3i = glProgramUniform3i;
				v41.glProgramUniform3iv = glProgramUniform3iv;
				v41.glProgramUniform3f = glProgramUniform3f;
				v41.glProgramUniform3fv = glProgramUniform3fv;
				v41.glProgramUniform3d = glProgramUniform3d;
				v41.glProgramUniform3dv = glProgramUniform3dv;
				v41.glProgramUniform3ui = glProgramUniform3ui;
				v41.glProgramUniform3uiv = glProgramUniform3uiv;
				v41.glProgramUniform4i = glProgramUniform4i;
				v41.glProgramUniform4iv = glProgramUniform4iv;
				v41.glProgramUniform4f = glProgramUniform4f;
				v41.glProgramUniform4fv = glProgramUniform4fv;
				v41.glProgramUniform4d = glProgramUniform4d;
				v41.glProgramUniform4dv = glProgramUniform4dv;
				v41.glProgramUniform4ui = glProgramUniform4ui;
				v41.glProgramUniform4uiv = glProgramUniform4uiv;
				v41.glProgramUniformMatrix2fv = glProgramUniformMatrix2fv;
				v41.glProgramUniformMatrix3fv = glProgramUniformMatrix3fv;
				v41.glProgramUniformMatrix4fv = glProgramUniformMatrix4fv;
				v41.glProgramUniformMatrix2dv = glProgramUniformMatrix2dv;
				v41.glProgramUniformMatrix3dv = glProgramUniformMatrix3dv;
				v41.glProgramUniformMatrix4dv = glProgramUniformMatrix4dv;
				v41.glProgramUniformMatrix2x3fv = glProgramUniformMatrix2x3fv;
				v41.glProgramUniformMatrix3x2fv = glProgramUniformMatrix3x2fv;
				v41.glProgramUniformMatrix2x4fv = glProgramUniformMatrix2x4fv;
				v41.glProgramUniformMatrix4x2fv = glProgramUniformMatrix4x2fv;
				v41.glProgramUniformMatrix3x4fv = glProgramUniformMatrix3x4fv;
				v41.glProgramUniformMatrix4x3fv = glProgramUniformMatrix4x3fv;
				v41.glProgramUniformMatrix2x3dv = glProgramUniformMatrix2x3dv;
				v41.glProgramUniformMatrix3x2dv = glProgramUniformMatrix3x2dv;
				v41.glProgramUniformMatrix2x4dv = glProgramUniformMatrix2x4dv;
				v41.glProgramUniformMatrix4x2dv = glProgramUniformMatrix4x2dv;
				v41.glProgramUniformMatrix3x4dv = glProgramUniformMatrix3x4dv;
				v41.glProgramUniformMatrix4x3dv = glProgramUniformMatrix4x3dv;
				v41.glValidateProgramPipeline = glValidateProgramPipeline;
				v41.glGetProgramPipelineInfoLog = glGetProgramPipelineInfoLog;
				v41.glVertexAttribL1d = glVertexAttribL1d;
				v41.glVertexAttribL2d = glVertexAttribL2d;
				v41.glVertexAttribL3d = glVertexAttribL3d;
				v41.glVertexAttribL4d = glVertexAttribL4d;
				v41.glVertexAttribL1dv = glVertexAttribL1dv;
				v41.glVertexAttribL2dv = glVertexAttribL2dv;
				v41.glVertexAttribL3dv = glVertexAttribL3dv;
				v41.glVertexAttribL4dv = glVertexAttribL4dv;
				v41.glVertexAttribLPointer = glVertexAttribLPointer;
				v41.glGetVertexAttribLdv = glGetVertexAttribLdv;
				v41.glViewportArrayv = glViewportArrayv;
				v41.glViewportIndexedf = glViewportIndexedf;
				v41.glViewportIndexedfv = glViewportIndexedfv;
				v41.glScissorArrayv = glScissorArrayv;
				v41.glScissorIndexed = glScissorIndexed;
				v41.glScissorIndexedv = glScissorIndexedv;
				v41.glDepthRangeArrayv = glDepthRangeArrayv;
				v41.glDepthRangeIndexed = glDepthRangeIndexed;
				v41.glGetFloati_v = glGetFloati_v;
				v41.glGetDoublei_v = glGetDoublei_v;

				//v42
				v42.glDrawArraysInstancedBaseInstance = glDrawArraysInstancedBaseInstance;
				v42.glDrawElementsInstancedBaseInstance = glDrawElementsInstancedBaseInstance;
				v42.glDrawElementsInstancedBaseVertexBaseInstance = glDrawElementsInstancedBaseVertexBaseInstance;
				v42.glGetInternalformativ = glGetInternalformativ;
				v42.glGetActiveAtomicCounterBufferiv = glGetActiveAtomicCounterBufferiv;
				v42.glBindImageTexture = glBindImageTexture;
				v42.glMemoryBarrier = glMemoryBarrier;
				v42.glTexStorage1D = glTexStorage1D;
				v42.glTexStorage2D = glTexStorage2D;
				v42.glTexStorage3D = glTexStorage3D;
				v42.glDrawTransformFeedbackInstanced = glDrawTransformFeedbackInstanced;
				v42.glDrawTransformFeedbackStreamInstanced = glDrawTransformFeedbackStreamInstanced;

				//v43
				v43.glClearBufferData = glClearBufferData;
				v43.glClearBufferSubData = glClearBufferSubData;
				v43.glDispatchCompute = glDispatchCompute;
				v43.glDispatchComputeIndirect = glDispatchComputeIndirect;
				v43.glCopyImageSubData = glCopyImageSubData;
				v43.glFramebufferParameteri = glFramebufferParameteri;
				v43.glGetFramebufferParameteriv = glGetFramebufferParameteriv;
				v43.glGetInternalformati64v = glGetInternalformati64v;
				v43.glInvalidateTexSubImage = glInvalidateTexSubImage;
				v43.glInvalidateTexImage = glInvalidateTexImage;
				v43.glInvalidateBufferSubData = glInvalidateBufferSubData;
				v43.glInvalidateBufferData = glInvalidateBufferData;
				v43.glInvalidateFramebuffer = glInvalidateFramebuffer;
				v43.glInvalidateSubFramebuffer = glInvalidateSubFramebuffer;
				v43.glMultiDrawArraysIndirect = glMultiDrawArraysIndirect;
				v43.glMultiDrawElementsIndirect = glMultiDrawElementsIndirect;
				v43.glGetProgramInterfaceiv = glGetProgramInterfaceiv;
				v43.glGetProgramResourceIndex = glGetProgramResourceIndex;
				v43.glGetProgramResourceName = glGetProgramResourceName;
				v43.glGetProgramResourceiv = glGetProgramResourceiv;
				v43.glGetProgramResourceLocation = glGetProgramResourceLocation;
				v43.glGetProgramResourceLocationIndex = glGetProgramResourceLocationIndex;
				v43.glShaderStorageBlockBinding = glShaderStorageBlockBinding;
				v43.glTexBufferRange = glTexBufferRange;
				v43.glTexStorage2DMultisample = glTexStorage2DMultisample;
				v43.glTexStorage3DMultisample = glTexStorage3DMultisample;
				v43.glTextureView = glTextureView;
				v43.glBindVertexBuffer = glBindVertexBuffer;
				v43.glVertexAttribFormat = glVertexAttribFormat;
				v43.glVertexAttribIFormat = glVertexAttribIFormat;
				v43.glVertexAttribLFormat = glVertexAttribLFormat;
				v43.glVertexAttribBinding = glVertexAttribBinding;
				v43.glVertexBindingDivisor = glVertexBindingDivisor;
				v43.glDebugMessageControl = glDebugMessageControl;
				v43.glDebugMessageInsert = glDebugMessageInsert;
				v43.glDebugMessageCallback = glDebugMessageCallback;
				v43.glGetDebugMessageLog = glGetDebugMessageLog;
				v43.glPushDebugGroup = glPushDebugGroup;
				v43.glPopDebugGroup = glPopDebugGroup;
				v43.glObjectLabel = glObjectLabel;
				v43.glGetObjectLabel = glGetObjectLabel;
				v43.glObjectPtrLabel = glObjectPtrLabel;
				v43.glGetObjectPtrLabel = glGetObjectPtrLabel;
				v43.glGetPointerv = glGetPointerv;

				//v44
				v44.glBufferStorage = glBufferStorage;
				v44.glClearTexImage = glClearTexImage;
				v44.glClearTexSubImage = glClearTexSubImage;
				v44.glBindBuffersBase = glBindBuffersBase;
				v44.glBindBuffersRange = glBindBuffersRange;
				v44.glBindTextures = glBindTextures;
				v44.glBindSamplers = glBindSamplers;
				v44.glBindImageTextures = glBindImageTextures;
				v44.glBindVertexBuffers = glBindVertexBuffers;

				//v45
				v45.glClipControl = glClipControl;
				v45.glCreateTransformFeedbacks = glCreateTransformFeedbacks;
				v45.glTransformFeedbackBufferBase = glTransformFeedbackBufferBase;
				v45.glTransformFeedbackBufferRange = glTransformFeedbackBufferRange;
				v45.glGetTransformFeedbackiv = glGetTransformFeedbackiv;
				v45.glGetTransformFeedbacki_v = glGetTransformFeedbacki_v;
				v45.glGetTransformFeedbacki64_v = glGetTransformFeedbacki64_v;
				v45.glCreateBuffers = glCreateBuffers;
				v45.glNamedBufferStorage = glNamedBufferStorage;
				v45.glNamedBufferData = glNamedBufferData;
				v45.glNamedBufferSubData = glNamedBufferSubData;
				v45.glCopyNamedBufferSubData = glCopyNamedBufferSubData;
				v45.glClearNamedBufferData = glClearNamedBufferData;
				v45.glClearNamedBufferSubData = glClearNamedBufferSubData;
				v45.glMapNamedBuffer = glMapNamedBuffer;
				v45.glMapNamedBufferRange = glMapNamedBufferRange;
				v45.glUnmapNamedBuffer = glUnmapNamedBuffer;
				v45.glFlushMappedNamedBufferRange = glFlushMappedNamedBufferRange;
				v45.glGetNamedBufferParameteriv = glGetNamedBufferParameteriv;
				v45.glGetNamedBufferParameteri64v = glGetNamedBufferParameteri64v;
				v45.glGetNamedBufferPointerv = glGetNamedBufferPointerv;
				v45.glGetNamedBufferSubData = glGetNamedBufferSubData;
				v45.glCreateFramebuffers = glCreateFramebuffers;
				v45.glNamedFramebufferRenderbuffer = glNamedFramebufferRenderbuffer;
				v45.glNamedFramebufferParameteri = glNamedFramebufferParameteri;
				v45.glNamedFramebufferTexture = glNamedFramebufferTexture;
				v45.glNamedFramebufferTextureLayer = glNamedFramebufferTextureLayer;
				v45.glNamedFramebufferDrawBuffer = glNamedFramebufferDrawBuffer;
				v45.glNamedFramebufferDrawBuffers = glNamedFramebufferDrawBuffers;
				v45.glNamedFramebufferReadBuffer = glNamedFramebufferReadBuffer;
				v45.glInvalidateNamedFramebufferData = glInvalidateNamedFramebufferData;
				v45.glInvalidateNamedFramebufferSubData = glInvalidateNamedFramebufferSubData;
				v45.glClearNamedFramebufferiv = glClearNamedFramebufferiv;
				v45.glClearNamedFramebufferuiv = glClearNamedFramebufferuiv;
				v45.glClearNamedFramebufferfv = glClearNamedFramebufferfv;
				v45.glClearNamedFramebufferfi = glClearNamedFramebufferfi;
				v45.glBlitNamedFramebuffer = glBlitNamedFramebuffer;
				v45.glCheckNamedFramebufferStatus = glCheckNamedFramebufferStatus;
				v45.glGetNamedFramebufferParameteriv = glGetNamedFramebufferParameteriv;
				v45.glGetNamedFramebufferAttachmentParameteriv = glGetNamedFramebufferAttachmentParameteriv;
				v45.glCreateRenderbuffers = glCreateRenderbuffers;
				v45.glNamedRenderbufferStorage = glNamedRenderbufferStorage;
				v45.glNamedRenderbufferStorageMultisample = glNamedRenderbufferStorageMultisample;
				v45.glGetNamedRenderbufferParameteriv = glGetNamedRenderbufferParameteriv;
				v45.glCreateTextures = glCreateTextures;
				v45.glTextureBuffer = glTextureBuffer;
				v45.glTextureBufferRange = glTextureBufferRange;
				v45.glTextureStorage1D = glTextureStorage1D;
				v45.glTextureStorage2D = glTextureStorage2D;
				v45.glTextureStorage3D = glTextureStorage3D;
				v45.glTextureStorage2DMultisample = glTextureStorage2DMultisample;
				v45.glTextureStorage3DMultisample = glTextureStorage3DMultisample;
				v45.glTextureSubImage1D = glTextureSubImage1D;
				v45.glTextureSubImage2D = glTextureSubImage2D;
				v45.glTextureSubImage3D = glTextureSubImage3D;
				v45.glCompressedTextureSubImage1D = glCompressedTextureSubImage1D;
				v45.glCompressedTextureSubImage2D = glCompressedTextureSubImage2D;
				v45.glCompressedTextureSubImage3D = glCompressedTextureSubImage3D;
				v45.glCopyTextureSubImage1D = glCopyTextureSubImage1D;
				v45.glCopyTextureSubImage2D = glCopyTextureSubImage2D;
				v45.glCopyTextureSubImage3D = glCopyTextureSubImage3D;
				v45.glTextureParameterf = glTextureParameterf;
				v45.glTextureParameterfv = glTextureParameterfv;
				v45.glTextureParameteri = glTextureParameteri;
				v45.glTextureParameterIiv = glTextureParameterIiv;
				v45.glTextureParameterIuiv = glTextureParameterIuiv;
				v45.glTextureParameteriv = glTextureParameteriv;
				v45.glGenerateTextureMipmap = glGenerateTextureMipmap;
				v45.glBindTextureUnit = glBindTextureUnit;
				v45.glGetTextureImage = glGetTextureImage;
				v45.glGetCompressedTextureImage = glGetCompressedTextureImage;
				v45.glGetTextureLevelParameterfv = glGetTextureLevelParameterfv;
				v45.glGetTextureLevelParameteriv = glGetTextureLevelParameteriv;
				v45.glGetTextureParameterfv = glGetTextureParameterfv;
				v45.glGetTextureParameterIiv = glGetTextureParameterIiv;
				v45.glGetTextureParameterIuiv = glGetTextureParameterIuiv;
				v45.glGetTextureParameteriv = glGetTextureParameteriv;
				v45.glCreateVertexArrays = glCreateVertexArrays;
				v45.glDisableVertexArrayAttrib = glDisableVertexArrayAttrib;
				v45.glEnableVertexArrayAttrib = glEnableVertexArrayAttrib;
				v45.glVertexArrayElementBuffer = glVertexArrayElementBuffer;
				v45.glVertexArrayVertexBuffer = glVertexArrayVertexBuffer;
				v45.glVertexArrayVertexBuffers = glVertexArrayVertexBuffers;
				v45.glVertexArrayAttribBinding = glVertexArrayAttribBinding;
				v45.glVertexArrayAttribFormat = glVertexArrayAttribFormat;
				v45.glVertexArrayAttribIFormat = glVertexArrayAttribIFormat;
				v45.glVertexArrayAttribLFormat = glVertexArrayAttribLFormat;
				v45.glVertexArrayBindingDivisor = glVertexArrayBindingDivisor;
				v45.glGetVertexArrayiv = glGetVertexArrayiv;
				v45.glGetVertexArrayIndexediv = glGetVertexArrayIndexediv;
				v45.glGetVertexArrayIndexed64iv = glGetVertexArrayIndexed64iv;
				v45.glCreateSamplers = glCreateSamplers;
				v45.glCreateProgramPipelines = glCreateProgramPipelines;
				v45.glCreateQueries = glCreateQueries;
				v45.glGetQueryBufferObjecti64v = glGetQueryBufferObjecti64v;
				v45.glGetQueryBufferObjectiv = glGetQueryBufferObjectiv;
				v45.glGetQueryBufferObjectui64v = glGetQueryBufferObjectui64v;
				v45.glGetQueryBufferObjectuiv = glGetQueryBufferObjectuiv;
				v45.glMemoryBarrierByRegion = glMemoryBarrierByRegion;
				v45.glGetTextureSubImage = glGetTextureSubImage;
				v45.glGetCompressedTextureSubImage = glGetCompressedTextureSubImage;
				v45.glGetGraphicsResetStatus = glGetGraphicsResetStatus;
				v45.glGetnCompressedTexImage = glGetnCompressedTexImage;
				v45.glGetnTexImage = glGetnTexImage;
				v45.glGetnUniformdv = glGetnUniformdv;
				v45.glGetnUniformfv = glGetnUniformfv;
				v45.glGetnUniformiv = glGetnUniformiv;
				v45.glGetnUniformuiv = glGetnUniformuiv;
				v45.glReadnPixels = glReadnPixels;
				v45.glGetnMapdv = glGetnMapdv;
				v45.glGetnMapfv = glGetnMapfv;
				v45.glGetnMapiv = glGetnMapiv;
				v45.glGetnPixelMapfv = glGetnPixelMapfv;
				v45.glGetnPixelMapuiv = glGetnPixelMapuiv;
				v45.glGetnPixelMapusv = glGetnPixelMapusv;
				v45.glGetnPolygonStipple = glGetnPolygonStipple;
				v45.glGetnColorTable = glGetnColorTable;
				v45.glGetnConvolutionFilter = glGetnConvolutionFilter;
				v45.glGetnSeparableFilter = glGetnSeparableFilter;
				v45.glGetnHistogram = glGetnHistogram;
				v45.glGetnMinmax = glGetnMinmax;
				v45.glTextureBarrier = glTextureBarrier;
			}
			
			// all opengl extensions by version
			struct {
				PFNGLCULLFACEPROC glCullFace;
				PFNGLFRONTFACEPROC glFrontFace;
				PFNGLHINTPROC glHint;
				PFNGLLINEWIDTHPROC glLineWidth;
				PFNGLPOINTSIZEPROC glPointSize;
				PFNGLPOLYGONMODEPROC glPolygonMode;
				PFNGLSCISSORPROC glScissor;
				PFNGLTEXPARAMETERFPROC glTexParameterf;
				PFNGLTEXPARAMETERFVPROC glTexParameterfv;
				PFNGLTEXPARAMETERIPROC glTexParameteri;
				PFNGLTEXPARAMETERIVPROC glTexParameteriv;
				PFNGLTEXIMAGE1DPROC glTexImage1D;
				PFNGLTEXIMAGE2DPROC glTexImage2D;
				PFNGLDRAWBUFFERPROC glDrawBuffer;
				PFNGLCLEARPROC glClear;
				PFNGLCLEARCOLORPROC glClearColor;
				PFNGLCLEARSTENCILPROC glClearStencil;
				PFNGLCLEARDEPTHPROC glClearDepth;
				PFNGLSTENCILMASKPROC glStencilMask;
				PFNGLCOLORMASKPROC glColorMask;
				PFNGLDEPTHMASKPROC glDepthMask;
				PFNGLDISABLEPROC glDisable;
				PFNGLENABLEPROC glEnable;
				PFNGLFINISHPROC glFinish;
				PFNGLFLUSHPROC glFlush;
				PFNGLBLENDFUNCPROC glBlendFunc;
				PFNGLLOGICOPPROC glLogicOp;
				PFNGLSTENCILFUNCPROC glStencilFunc;
				PFNGLSTENCILOPPROC glStencilOp;
				PFNGLDEPTHFUNCPROC glDepthFunc;
				PFNGLPIXELSTOREFPROC glPixelStoref;
				PFNGLPIXELSTOREIPROC glPixelStorei;
				PFNGLREADBUFFERPROC glReadBuffer;
				PFNGLREADPIXELSPROC glReadPixels;
				PFNGLGETBOOLEANVPROC glGetBooleanv;
				PFNGLGETDOUBLEVPROC glGetDoublev;
				PFNGLGETERRORPROC glGetError;
				PFNGLGETFLOATVPROC glGetFloatv;
				PFNGLGETINTEGERVPROC glGetIntegerv;
				PFNGLGETSTRINGPROC glGetString;
				PFNGLGETTEXIMAGEPROC glGetTexImage;
				PFNGLGETTEXPARAMETERFVPROC glGetTexParameterfv;
				PFNGLGETTEXPARAMETERIVPROC glGetTexParameteriv;
				PFNGLGETTEXLEVELPARAMETERFVPROC glGetTexLevelParameterfv;
				PFNGLGETTEXLEVELPARAMETERIVPROC glGetTexLevelParameteriv;
				PFNGLISENABLEDPROC glIsEnabled;
				PFNGLDEPTHRANGEPROC glDepthRange;
				PFNGLVIEWPORTPROC glViewport;
				PFNGLNEWLISTPROC glNewList;
				PFNGLENDLISTPROC glEndList;
				PFNGLCALLLISTPROC glCallList;
				PFNGLCALLLISTSPROC glCallLists;
				PFNGLDELETELISTSPROC glDeleteLists;
				PFNGLGENLISTSPROC glGenLists;
				PFNGLLISTBASEPROC glListBase;
				PFNGLBEGINPROC glBegin;
				PFNGLBITMAPPROC glBitmap;
				PFNGLCOLOR3BPROC glColor3b;
				PFNGLCOLOR3BVPROC glColor3bv;
				PFNGLCOLOR3DPROC glColor3d;
				PFNGLCOLOR3DVPROC glColor3dv;
				PFNGLCOLOR3FPROC glColor3f;
				PFNGLCOLOR3FVPROC glColor3fv;
				PFNGLCOLOR3IPROC glColor3i;
				PFNGLCOLOR3IVPROC glColor3iv;
				PFNGLCOLOR3SPROC glColor3s;
				PFNGLCOLOR3SVPROC glColor3sv;
				PFNGLCOLOR3UBPROC glColor3ub;
				PFNGLCOLOR3UBVPROC glColor3ubv;
				PFNGLCOLOR3UIPROC glColor3ui;
				PFNGLCOLOR3UIVPROC glColor3uiv;
				PFNGLCOLOR3USPROC glColor3us;
				PFNGLCOLOR3USVPROC glColor3usv;
				PFNGLCOLOR4BPROC glColor4b;
				PFNGLCOLOR4BVPROC glColor4bv;
				PFNGLCOLOR4DPROC glColor4d;
				PFNGLCOLOR4DVPROC glColor4dv;
				PFNGLCOLOR4FPROC glColor4f;
				PFNGLCOLOR4FVPROC glColor4fv;
				PFNGLCOLOR4IPROC glColor4i;
				PFNGLCOLOR4IVPROC glColor4iv;
				PFNGLCOLOR4SPROC glColor4s;
				PFNGLCOLOR4SVPROC glColor4sv;
				PFNGLCOLOR4UBPROC glColor4ub;
				PFNGLCOLOR4UBVPROC glColor4ubv;
				PFNGLCOLOR4UIPROC glColor4ui;
				PFNGLCOLOR4UIVPROC glColor4uiv;
				PFNGLCOLOR4USPROC glColor4us;
				PFNGLCOLOR4USVPROC glColor4usv;
				PFNGLEDGEFLAGPROC glEdgeFlag;
				PFNGLEDGEFLAGVPROC glEdgeFlagv;
				PFNGLENDPROC glEnd;
				PFNGLINDEXDPROC glIndexd;
				PFNGLINDEXDVPROC glIndexdv;
				PFNGLINDEXFPROC glIndexf;
				PFNGLINDEXFVPROC glIndexfv;
				PFNGLINDEXIPROC glIndexi;
				PFNGLINDEXIVPROC glIndexiv;
				PFNGLINDEXSPROC glIndexs;
				PFNGLINDEXSVPROC glIndexsv;
				PFNGLNORMAL3BPROC glNormal3b;
				PFNGLNORMAL3BVPROC glNormal3bv;
				PFNGLNORMAL3DPROC glNormal3d;
				PFNGLNORMAL3DVPROC glNormal3dv;
				PFNGLNORMAL3FPROC glNormal3f;
				PFNGLNORMAL3FVPROC glNormal3fv;
				PFNGLNORMAL3IPROC glNormal3i;
				PFNGLNORMAL3IVPROC glNormal3iv;
				PFNGLNORMAL3SPROC glNormal3s;
				PFNGLNORMAL3SVPROC glNormal3sv;
				PFNGLRASTERPOS2DPROC glRasterPos2d;
				PFNGLRASTERPOS2DVPROC glRasterPos2dv;
				PFNGLRASTERPOS2FPROC glRasterPos2f;
				PFNGLRASTERPOS2FVPROC glRasterPos2fv;
				PFNGLRASTERPOS2IPROC glRasterPos2i;
				PFNGLRASTERPOS2IVPROC glRasterPos2iv;
				PFNGLRASTERPOS2SPROC glRasterPos2s;
				PFNGLRASTERPOS2SVPROC glRasterPos2sv;
				PFNGLRASTERPOS3DPROC glRasterPos3d;
				PFNGLRASTERPOS3DVPROC glRasterPos3dv;
				PFNGLRASTERPOS3FPROC glRasterPos3f;
				PFNGLRASTERPOS3FVPROC glRasterPos3fv;
				PFNGLRASTERPOS3IPROC glRasterPos3i;
				PFNGLRASTERPOS3IVPROC glRasterPos3iv;
				PFNGLRASTERPOS3SPROC glRasterPos3s;
				PFNGLRASTERPOS3SVPROC glRasterPos3sv;
				PFNGLRASTERPOS4DPROC glRasterPos4d;
				PFNGLRASTERPOS4DVPROC glRasterPos4dv;
				PFNGLRASTERPOS4FPROC glRasterPos4f;
				PFNGLRASTERPOS4FVPROC glRasterPos4fv;
				PFNGLRASTERPOS4IPROC glRasterPos4i;
				PFNGLRASTERPOS4IVPROC glRasterPos4iv;
				PFNGLRASTERPOS4SPROC glRasterPos4s;
				PFNGLRASTERPOS4SVPROC glRasterPos4sv;
				PFNGLRECTDPROC glRectd;
				PFNGLRECTDVPROC glRectdv;
				PFNGLRECTFPROC glRectf;
				PFNGLRECTFVPROC glRectfv;
				PFNGLRECTIPROC glRecti;
				PFNGLRECTIVPROC glRectiv;
				PFNGLRECTSPROC glRects;
				PFNGLRECTSVPROC glRectsv;
				PFNGLTEXCOORD1DPROC glTexCoord1d;
				PFNGLTEXCOORD1DVPROC glTexCoord1dv;
				PFNGLTEXCOORD1FPROC glTexCoord1f;
				PFNGLTEXCOORD1FVPROC glTexCoord1fv;
				PFNGLTEXCOORD1IPROC glTexCoord1i;
				PFNGLTEXCOORD1IVPROC glTexCoord1iv;
				PFNGLTEXCOORD1SPROC glTexCoord1s;
				PFNGLTEXCOORD1SVPROC glTexCoord1sv;
				PFNGLTEXCOORD2DPROC glTexCoord2d;
				PFNGLTEXCOORD2DVPROC glTexCoord2dv;
				PFNGLTEXCOORD2FPROC glTexCoord2f;
				PFNGLTEXCOORD2FVPROC glTexCoord2fv;
				PFNGLTEXCOORD2IPROC glTexCoord2i;
				PFNGLTEXCOORD2IVPROC glTexCoord2iv;
				PFNGLTEXCOORD2SPROC glTexCoord2s;
				PFNGLTEXCOORD2SVPROC glTexCoord2sv;
				PFNGLTEXCOORD3DPROC glTexCoord3d;
				PFNGLTEXCOORD3DVPROC glTexCoord3dv;
				PFNGLTEXCOORD3FPROC glTexCoord3f;
				PFNGLTEXCOORD3FVPROC glTexCoord3fv;
				PFNGLTEXCOORD3IPROC glTexCoord3i;
				PFNGLTEXCOORD3IVPROC glTexCoord3iv;
				PFNGLTEXCOORD3SPROC glTexCoord3s;
				PFNGLTEXCOORD3SVPROC glTexCoord3sv;
				PFNGLTEXCOORD4DPROC glTexCoord4d;
				PFNGLTEXCOORD4DVPROC glTexCoord4dv;
				PFNGLTEXCOORD4FPROC glTexCoord4f;
				PFNGLTEXCOORD4FVPROC glTexCoord4fv;
				PFNGLTEXCOORD4IPROC glTexCoord4i;
				PFNGLTEXCOORD4IVPROC glTexCoord4iv;
				PFNGLTEXCOORD4SPROC glTexCoord4s;
				PFNGLTEXCOORD4SVPROC glTexCoord4sv;
				PFNGLVERTEX2DPROC glVertex2d;
				PFNGLVERTEX2DVPROC glVertex2dv;
				PFNGLVERTEX2FPROC glVertex2f;
				PFNGLVERTEX2FVPROC glVertex2fv;
				PFNGLVERTEX2IPROC glVertex2i;
				PFNGLVERTEX2IVPROC glVertex2iv;
				PFNGLVERTEX2SPROC glVertex2s;
				PFNGLVERTEX2SVPROC glVertex2sv;
				PFNGLVERTEX3DPROC glVertex3d;
				PFNGLVERTEX3DVPROC glVertex3dv;
				PFNGLVERTEX3FPROC glVertex3f;
				PFNGLVERTEX3FVPROC glVertex3fv;
				PFNGLVERTEX3IPROC glVertex3i;
				PFNGLVERTEX3IVPROC glVertex3iv;
				PFNGLVERTEX3SPROC glVertex3s;
				PFNGLVERTEX3SVPROC glVertex3sv;
				PFNGLVERTEX4DPROC glVertex4d;
				PFNGLVERTEX4DVPROC glVertex4dv;
				PFNGLVERTEX4FPROC glVertex4f;
				PFNGLVERTEX4FVPROC glVertex4fv;
				PFNGLVERTEX4IPROC glVertex4i;
				PFNGLVERTEX4IVPROC glVertex4iv;
				PFNGLVERTEX4SPROC glVertex4s;
				PFNGLVERTEX4SVPROC glVertex4sv;
				PFNGLCLIPPLANEPROC glClipPlane;
				PFNGLCOLORMATERIALPROC glColorMaterial;
				PFNGLFOGFPROC glFogf;
				PFNGLFOGFVPROC glFogfv;
				PFNGLFOGIPROC glFogi;
				PFNGLFOGIVPROC glFogiv;
				PFNGLLIGHTFPROC glLightf;
				PFNGLLIGHTFVPROC glLightfv;
				PFNGLLIGHTIPROC glLighti;
				PFNGLLIGHTIVPROC glLightiv;
				PFNGLLIGHTMODELFPROC glLightModelf;
				PFNGLLIGHTMODELFVPROC glLightModelfv;
				PFNGLLIGHTMODELIPROC glLightModeli;
				PFNGLLIGHTMODELIVPROC glLightModeliv;
				PFNGLLINESTIPPLEPROC glLineStipple;
				PFNGLMATERIALFPROC glMaterialf;
				PFNGLMATERIALFVPROC glMaterialfv;
				PFNGLMATERIALIPROC glMateriali;
				PFNGLMATERIALIVPROC glMaterialiv;
				PFNGLPOLYGONSTIPPLEPROC glPolygonStipple;
				PFNGLSHADEMODELPROC glShadeModel;
				PFNGLTEXENVFPROC glTexEnvf;
				PFNGLTEXENVFVPROC glTexEnvfv;
				PFNGLTEXENVIPROC glTexEnvi;
				PFNGLTEXENVIVPROC glTexEnviv;
				PFNGLTEXGENDPROC glTexGend;
				PFNGLTEXGENDVPROC glTexGendv;
				PFNGLTEXGENFPROC glTexGenf;
				PFNGLTEXGENFVPROC glTexGenfv;
				PFNGLTEXGENIPROC glTexGeni;
				PFNGLTEXGENIVPROC glTexGeniv;
				PFNGLFEEDBACKBUFFERPROC glFeedbackBuffer;
				PFNGLSELECTBUFFERPROC glSelectBuffer;
				PFNGLRENDERMODEPROC glRenderMode;
				PFNGLINITNAMESPROC glInitNames;
				PFNGLLOADNAMEPROC glLoadName;
				PFNGLPASSTHROUGHPROC glPassThrough;
				PFNGLPOPNAMEPROC glPopName;
				PFNGLPUSHNAMEPROC glPushName;
				PFNGLCLEARACCUMPROC glClearAccum;
				PFNGLCLEARINDEXPROC glClearIndex;
				PFNGLINDEXMASKPROC glIndexMask;
				PFNGLACCUMPROC glAccum;
				PFNGLPOPATTRIBPROC glPopAttrib;
				PFNGLPUSHATTRIBPROC glPushAttrib;
				PFNGLMAP1DPROC glMap1d;
				PFNGLMAP1FPROC glMap1f;
				PFNGLMAP2DPROC glMap2d;
				PFNGLMAP2FPROC glMap2f;
				PFNGLMAPGRID1DPROC glMapGrid1d;
				PFNGLMAPGRID1FPROC glMapGrid1f;
				PFNGLMAPGRID2DPROC glMapGrid2d;
				PFNGLMAPGRID2FPROC glMapGrid2f;
				PFNGLEVALCOORD1DPROC glEvalCoord1d;
				PFNGLEVALCOORD1DVPROC glEvalCoord1dv;
				PFNGLEVALCOORD1FPROC glEvalCoord1f;
				PFNGLEVALCOORD1FVPROC glEvalCoord1fv;
				PFNGLEVALCOORD2DPROC glEvalCoord2d;
				PFNGLEVALCOORD2DVPROC glEvalCoord2dv;
				PFNGLEVALCOORD2FPROC glEvalCoord2f;
				PFNGLEVALCOORD2FVPROC glEvalCoord2fv;
				PFNGLEVALMESH1PROC glEvalMesh1;
				PFNGLEVALPOINT1PROC glEvalPoint1;
				PFNGLEVALMESH2PROC glEvalMesh2;
				PFNGLEVALPOINT2PROC glEvalPoint2;
				PFNGLALPHAFUNCPROC glAlphaFunc;
				PFNGLPIXELZOOMPROC glPixelZoom;
				PFNGLPIXELTRANSFERFPROC glPixelTransferf;
				PFNGLPIXELTRANSFERIPROC glPixelTransferi;
				PFNGLPIXELMAPFVPROC glPixelMapfv;
				PFNGLPIXELMAPUIVPROC glPixelMapuiv;
				PFNGLPIXELMAPUSVPROC glPixelMapusv;
				PFNGLCOPYPIXELSPROC glCopyPixels;
				PFNGLDRAWPIXELSPROC glDrawPixels;
				PFNGLGETCLIPPLANEPROC glGetClipPlane;
				PFNGLGETLIGHTFVPROC glGetLightfv;
				PFNGLGETLIGHTIVPROC glGetLightiv;
				PFNGLGETMAPDVPROC glGetMapdv;
				PFNGLGETMAPFVPROC glGetMapfv;
				PFNGLGETMAPIVPROC glGetMapiv;
				PFNGLGETMATERIALFVPROC glGetMaterialfv;
				PFNGLGETMATERIALIVPROC glGetMaterialiv;
				PFNGLGETPIXELMAPFVPROC glGetPixelMapfv;
				PFNGLGETPIXELMAPUIVPROC glGetPixelMapuiv;
				PFNGLGETPIXELMAPUSVPROC glGetPixelMapusv;
				PFNGLGETPOLYGONSTIPPLEPROC glGetPolygonStipple;
				PFNGLGETTEXENVFVPROC glGetTexEnvfv;
				PFNGLGETTEXENVIVPROC glGetTexEnviv;
				PFNGLGETTEXGENDVPROC glGetTexGendv;
				PFNGLGETTEXGENFVPROC glGetTexGenfv;
				PFNGLGETTEXGENIVPROC glGetTexGeniv;
				PFNGLISLISTPROC glIsList;
				PFNGLFRUSTUMPROC glFrustum;
				PFNGLLOADIDENTITYPROC glLoadIdentity;
				PFNGLLOADMATRIXFPROC glLoadMatrixf;
				PFNGLLOADMATRIXDPROC glLoadMatrixd;
				PFNGLMATRIXMODEPROC glMatrixMode;
				PFNGLMULTMATRIXFPROC glMultMatrixf;
				PFNGLMULTMATRIXDPROC glMultMatrixd;
				PFNGLORTHOPROC glOrtho;
				PFNGLPOPMATRIXPROC glPopMatrix;
				PFNGLPUSHMATRIXPROC glPushMatrix;
				PFNGLROTATEDPROC glRotated;
				PFNGLROTATEFPROC glRotatef;
				PFNGLSCALEDPROC glScaled;
				PFNGLSCALEFPROC glScalef;
				PFNGLTRANSLATEDPROC glTranslated;
				PFNGLTRANSLATEFPROC glTranslatef;
			}v10;
			struct {
				PFNGLDRAWARRAYSPROC glDrawArrays;
				PFNGLDRAWELEMENTSPROC glDrawElements;
				PFNGLPOLYGONOFFSETPROC glPolygonOffset;
				PFNGLCOPYTEXIMAGE1DPROC glCopyTexImage1D;
				PFNGLCOPYTEXIMAGE2DPROC glCopyTexImage2D;
				PFNGLCOPYTEXSUBIMAGE1DPROC glCopyTexSubImage1D;
				PFNGLCOPYTEXSUBIMAGE2DPROC glCopyTexSubImage2D;
				PFNGLTEXSUBIMAGE1DPROC glTexSubImage1D;
				PFNGLTEXSUBIMAGE2DPROC glTexSubImage2D;
				PFNGLBINDTEXTUREPROC glBindTexture;
				PFNGLDELETETEXTURESPROC glDeleteTextures;
				PFNGLGENTEXTURESPROC glGenTextures;
				PFNGLISTEXTUREPROC glIsTexture;
				PFNGLARRAYELEMENTPROC glArrayElement;
				PFNGLCOLORPOINTERPROC glColorPointer;
				PFNGLDISABLECLIENTSTATEPROC glDisableClientState;
				PFNGLEDGEFLAGPOINTERPROC glEdgeFlagPointer;
				PFNGLENABLECLIENTSTATEPROC glEnableClientState;
				PFNGLINDEXPOINTERPROC glIndexPointer;
				PFNGLINTERLEAVEDARRAYSPROC glInterleavedArrays;
				PFNGLNORMALPOINTERPROC glNormalPointer;
				PFNGLTEXCOORDPOINTERPROC glTexCoordPointer;
				PFNGLVERTEXPOINTERPROC glVertexPointer;
				PFNGLARETEXTURESRESIDENTPROC glAreTexturesResident;
				PFNGLPRIORITIZETEXTURESPROC glPrioritizeTextures;
				PFNGLINDEXUBPROC glIndexub;
				PFNGLINDEXUBVPROC glIndexubv;
				PFNGLPOPCLIENTATTRIBPROC glPopClientAttrib;
				PFNGLPUSHCLIENTATTRIBPROC glPushClientAttrib;
			}v11;
			struct {
				PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements;
				PFNGLTEXIMAGE3DPROC glTexImage3D;
				PFNGLTEXSUBIMAGE3DPROC glTexSubImage3D;
				PFNGLCOPYTEXSUBIMAGE3DPROC glCopyTexSubImage3D;
			}v12;
			struct {
				PFNGLACTIVETEXTUREPROC glActiveTexture;
				PFNGLSAMPLECOVERAGEPROC glSampleCoverage;
				PFNGLCOMPRESSEDTEXIMAGE3DPROC glCompressedTexImage3D;
				PFNGLCOMPRESSEDTEXIMAGE2DPROC glCompressedTexImage2D;
				PFNGLCOMPRESSEDTEXIMAGE1DPROC glCompressedTexImage1D;
				PFNGLCOMPRESSEDTEXSUBIMAGE3DPROC glCompressedTexSubImage3D;
				PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC glCompressedTexSubImage2D;
				PFNGLCOMPRESSEDTEXSUBIMAGE1DPROC glCompressedTexSubImage1D;
				PFNGLGETCOMPRESSEDTEXIMAGEPROC glGetCompressedTexImage;
				PFNGLCLIENTACTIVETEXTUREPROC glClientActiveTexture;
				PFNGLMULTITEXCOORD1DPROC glMultiTexCoord1d;
				PFNGLMULTITEXCOORD1DVPROC glMultiTexCoord1dv;
				PFNGLMULTITEXCOORD1FPROC glMultiTexCoord1f;
				PFNGLMULTITEXCOORD1FVPROC glMultiTexCoord1fv;
				PFNGLMULTITEXCOORD1IPROC glMultiTexCoord1i;
				PFNGLMULTITEXCOORD1IVPROC glMultiTexCoord1iv;
				PFNGLMULTITEXCOORD1SPROC glMultiTexCoord1s;
				PFNGLMULTITEXCOORD1SVPROC glMultiTexCoord1sv;
				PFNGLMULTITEXCOORD2DPROC glMultiTexCoord2d;
				PFNGLMULTITEXCOORD2DVPROC glMultiTexCoord2dv;
				PFNGLMULTITEXCOORD2FPROC glMultiTexCoord2f;
				PFNGLMULTITEXCOORD2FVPROC glMultiTexCoord2fv;
				PFNGLMULTITEXCOORD2IPROC glMultiTexCoord2i;
				PFNGLMULTITEXCOORD2IVPROC glMultiTexCoord2iv;
				PFNGLMULTITEXCOORD2SPROC glMultiTexCoord2s;
				PFNGLMULTITEXCOORD2SVPROC glMultiTexCoord2sv;
				PFNGLMULTITEXCOORD3DPROC glMultiTexCoord3d;
				PFNGLMULTITEXCOORD3DVPROC glMultiTexCoord3dv;
				PFNGLMULTITEXCOORD3FPROC glMultiTexCoord3f;
				PFNGLMULTITEXCOORD3FVPROC glMultiTexCoord3fv;
				PFNGLMULTITEXCOORD3IPROC glMultiTexCoord3i;
				PFNGLMULTITEXCOORD3IVPROC glMultiTexCoord3iv;
				PFNGLMULTITEXCOORD3SPROC glMultiTexCoord3s;
				PFNGLMULTITEXCOORD3SVPROC glMultiTexCoord3sv;
				PFNGLMULTITEXCOORD4DPROC glMultiTexCoord4d;
				PFNGLMULTITEXCOORD4DVPROC glMultiTexCoord4dv;
				PFNGLMULTITEXCOORD4FPROC glMultiTexCoord4f;
				PFNGLMULTITEXCOORD4FVPROC glMultiTexCoord4fv;
				PFNGLMULTITEXCOORD4IPROC glMultiTexCoord4i;
				PFNGLMULTITEXCOORD4IVPROC glMultiTexCoord4iv;
				PFNGLMULTITEXCOORD4SPROC glMultiTexCoord4s;
				PFNGLMULTITEXCOORD4SVPROC glMultiTexCoord4sv;
				PFNGLLOADTRANSPOSEMATRIXFPROC glLoadTransposeMatrixf;
				PFNGLLOADTRANSPOSEMATRIXDPROC glLoadTransposeMatrixd;
				PFNGLMULTTRANSPOSEMATRIXFPROC glMultTransposeMatrixf;
				PFNGLMULTTRANSPOSEMATRIXDPROC glMultTransposeMatrixd;
			}v13;
			struct {
				PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate;
				PFNGLMULTIDRAWARRAYSPROC glMultiDrawArrays;
				PFNGLMULTIDRAWELEMENTSPROC glMultiDrawElements;
				PFNGLPOINTPARAMETERFPROC glPointParameterf;
				PFNGLPOINTPARAMETERFVPROC glPointParameterfv;
				PFNGLPOINTPARAMETERIPROC glPointParameteri;
				PFNGLPOINTPARAMETERIVPROC glPointParameteriv;
				PFNGLFOGCOORDFPROC glFogCoordf;
				PFNGLFOGCOORDFVPROC glFogCoordfv;
				PFNGLFOGCOORDDPROC glFogCoordd;
				PFNGLFOGCOORDDVPROC glFogCoorddv;
				PFNGLFOGCOORDPOINTERPROC glFogCoordPointer;
				PFNGLSECONDARYCOLOR3BPROC glSecondaryColor3b;
				PFNGLSECONDARYCOLOR3BVPROC glSecondaryColor3bv;
				PFNGLSECONDARYCOLOR3DPROC glSecondaryColor3d;
				PFNGLSECONDARYCOLOR3DVPROC glSecondaryColor3dv;
				PFNGLSECONDARYCOLOR3FPROC glSecondaryColor3f;
				PFNGLSECONDARYCOLOR3FVPROC glSecondaryColor3fv;
				PFNGLSECONDARYCOLOR3IPROC glSecondaryColor3i;
				PFNGLSECONDARYCOLOR3IVPROC glSecondaryColor3iv;
				PFNGLSECONDARYCOLOR3SPROC glSecondaryColor3s;
				PFNGLSECONDARYCOLOR3SVPROC glSecondaryColor3sv;
				PFNGLSECONDARYCOLOR3UBPROC glSecondaryColor3ub;
				PFNGLSECONDARYCOLOR3UBVPROC glSecondaryColor3ubv;
				PFNGLSECONDARYCOLOR3UIPROC glSecondaryColor3ui;
				PFNGLSECONDARYCOLOR3UIVPROC glSecondaryColor3uiv;
				PFNGLSECONDARYCOLOR3USPROC glSecondaryColor3us;
				PFNGLSECONDARYCOLOR3USVPROC glSecondaryColor3usv;
				PFNGLSECONDARYCOLORPOINTERPROC glSecondaryColorPointer;
				PFNGLWINDOWPOS2DPROC glWindowPos2d;
				PFNGLWINDOWPOS2DVPROC glWindowPos2dv;
				PFNGLWINDOWPOS2FPROC glWindowPos2f;
				PFNGLWINDOWPOS2FVPROC glWindowPos2fv;
				PFNGLWINDOWPOS2IPROC glWindowPos2i;
				PFNGLWINDOWPOS2IVPROC glWindowPos2iv;
				PFNGLWINDOWPOS2SPROC glWindowPos2s;
				PFNGLWINDOWPOS2SVPROC glWindowPos2sv;
				PFNGLWINDOWPOS3DPROC glWindowPos3d;
				PFNGLWINDOWPOS3DVPROC glWindowPos3dv;
				PFNGLWINDOWPOS3FPROC glWindowPos3f;
				PFNGLWINDOWPOS3FVPROC glWindowPos3fv;
				PFNGLWINDOWPOS3IPROC glWindowPos3i;
				PFNGLWINDOWPOS3IVPROC glWindowPos3iv;
				PFNGLWINDOWPOS3SPROC glWindowPos3s;
				PFNGLWINDOWPOS3SVPROC glWindowPos3sv;
				PFNGLBLENDCOLORPROC glBlendColor;
				PFNGLBLENDEQUATIONPROC glBlendEquation;
			}v14;
			struct {
				PFNGLGENQUERIESPROC glGenQueries;
				PFNGLDELETEQUERIESPROC glDeleteQueries;
				PFNGLISQUERYPROC glIsQuery;
				PFNGLBEGINQUERYPROC glBeginQuery;
				PFNGLENDQUERYPROC glEndQuery;
				PFNGLGETQUERYIVPROC glGetQueryiv;
				PFNGLGETQUERYOBJECTIVPROC glGetQueryObjectiv;
				PFNGLGETQUERYOBJECTUIVPROC glGetQueryObjectuiv;
				PFNGLBINDBUFFERPROC glBindBuffer;
				PFNGLDELETEBUFFERSPROC glDeleteBuffers;
				PFNGLGENBUFFERSPROC glGenBuffers;
				PFNGLISBUFFERPROC glIsBuffer;
				PFNGLBUFFERDATAPROC glBufferData;
				PFNGLBUFFERSUBDATAPROC glBufferSubData;
				PFNGLGETBUFFERSUBDATAPROC glGetBufferSubData;
				PFNGLMAPBUFFERPROC glMapBuffer;
				PFNGLUNMAPBUFFERPROC glUnmapBuffer;
				PFNGLGETBUFFERPARAMETERIVPROC glGetBufferParameteriv;
				PFNGLGETBUFFERPOINTERVPROC glGetBufferPointerv;
			}v15;
			struct {
				PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate;
				PFNGLDRAWBUFFERSPROC glDrawBuffers;
				PFNGLSTENCILOPSEPARATEPROC glStencilOpSeparate;
				PFNGLSTENCILFUNCSEPARATEPROC glStencilFuncSeparate;
				PFNGLSTENCILMASKSEPARATEPROC glStencilMaskSeparate;
				PFNGLATTACHSHADERPROC glAttachShader;
				PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
				PFNGLCOMPILESHADERPROC glCompileShader;
				PFNGLCREATEPROGRAMPROC glCreateProgram;
				PFNGLCREATESHADERPROC glCreateShader;
				PFNGLDELETEPROGRAMPROC glDeleteProgram;
				PFNGLDELETESHADERPROC glDeleteShader;
				PFNGLDETACHSHADERPROC glDetachShader;
				PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
				PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
				PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib;
				PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform;
				PFNGLGETATTACHEDSHADERSPROC glGetAttachedShaders;
				PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation;
				PFNGLGETPROGRAMIVPROC glGetProgramiv;
				PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
				PFNGLGETSHADERIVPROC glGetShaderiv;
				PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
				PFNGLGETSHADERSOURCEPROC glGetShaderSource;
				PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
				PFNGLGETUNIFORMFVPROC glGetUniformfv;
				PFNGLGETUNIFORMIVPROC glGetUniformiv;
				PFNGLGETVERTEXATTRIBDVPROC glGetVertexAttribdv;
				PFNGLGETVERTEXATTRIBFVPROC glGetVertexAttribfv;
				PFNGLGETVERTEXATTRIBIVPROC glGetVertexAttribiv;
				PFNGLGETVERTEXATTRIBPOINTERVPROC glGetVertexAttribPointerv;
				PFNGLISPROGRAMPROC glIsProgram;
				PFNGLISSHADERPROC glIsShader;
				PFNGLLINKPROGRAMPROC glLinkProgram;
				PFNGLSHADERSOURCEPROC glShaderSource;
				PFNGLUSEPROGRAMPROC glUseProgram;
				PFNGLUNIFORM1FPROC glUniform1f;
				PFNGLUNIFORM2FPROC glUniform2f;
				PFNGLUNIFORM3FPROC glUniform3f;
				PFNGLUNIFORM4FPROC glUniform4f;
				PFNGLUNIFORM1IPROC glUniform1i;
				PFNGLUNIFORM2IPROC glUniform2i;
				PFNGLUNIFORM3IPROC glUniform3i;
				PFNGLUNIFORM4IPROC glUniform4i;
				PFNGLUNIFORM1FVPROC glUniform1fv;
				PFNGLUNIFORM2FVPROC glUniform2fv;
				PFNGLUNIFORM3FVPROC glUniform3fv;
				PFNGLUNIFORM4FVPROC glUniform4fv;
				PFNGLUNIFORM1IVPROC glUniform1iv;
				PFNGLUNIFORM2IVPROC glUniform2iv;
				PFNGLUNIFORM3IVPROC glUniform3iv;
				PFNGLUNIFORM4IVPROC glUniform4iv;
				PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv;
				PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv;
				PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
				PFNGLVALIDATEPROGRAMPROC glValidateProgram;
				PFNGLVERTEXATTRIB1DPROC glVertexAttrib1d;
				PFNGLVERTEXATTRIB1DVPROC glVertexAttrib1dv;
				PFNGLVERTEXATTRIB1FPROC glVertexAttrib1f;
				PFNGLVERTEXATTRIB1FVPROC glVertexAttrib1fv;
				PFNGLVERTEXATTRIB1SPROC glVertexAttrib1s;
				PFNGLVERTEXATTRIB1SVPROC glVertexAttrib1sv;
				PFNGLVERTEXATTRIB2DPROC glVertexAttrib2d;
				PFNGLVERTEXATTRIB2DVPROC glVertexAttrib2dv;
				PFNGLVERTEXATTRIB2FPROC glVertexAttrib2f;
				PFNGLVERTEXATTRIB2FVPROC glVertexAttrib2fv;
				PFNGLVERTEXATTRIB2SPROC glVertexAttrib2s;
				PFNGLVERTEXATTRIB2SVPROC glVertexAttrib2sv;
				PFNGLVERTEXATTRIB3DPROC glVertexAttrib3d;
				PFNGLVERTEXATTRIB3DVPROC glVertexAttrib3dv;
				PFNGLVERTEXATTRIB3FPROC glVertexAttrib3f;
				PFNGLVERTEXATTRIB3FVPROC glVertexAttrib3fv;
				PFNGLVERTEXATTRIB3SPROC glVertexAttrib3s;
				PFNGLVERTEXATTRIB3SVPROC glVertexAttrib3sv;
				PFNGLVERTEXATTRIB4NBVPROC glVertexAttrib4Nbv;
				PFNGLVERTEXATTRIB4NIVPROC glVertexAttrib4Niv;
				PFNGLVERTEXATTRIB4NSVPROC glVertexAttrib4Nsv;
				PFNGLVERTEXATTRIB4NUBPROC glVertexAttrib4Nub;
				PFNGLVERTEXATTRIB4NUBVPROC glVertexAttrib4Nubv;
				PFNGLVERTEXATTRIB4NUIVPROC glVertexAttrib4Nuiv;
				PFNGLVERTEXATTRIB4NUSVPROC glVertexAttrib4Nusv;
				PFNGLVERTEXATTRIB4BVPROC glVertexAttrib4bv;
				PFNGLVERTEXATTRIB4DPROC glVertexAttrib4d;
				PFNGLVERTEXATTRIB4DVPROC glVertexAttrib4dv;
				PFNGLVERTEXATTRIB4FPROC glVertexAttrib4f;
				PFNGLVERTEXATTRIB4FVPROC glVertexAttrib4fv;
				PFNGLVERTEXATTRIB4IVPROC glVertexAttrib4iv;
				PFNGLVERTEXATTRIB4SPROC glVertexAttrib4s;
				PFNGLVERTEXATTRIB4SVPROC glVertexAttrib4sv;
				PFNGLVERTEXATTRIB4UBVPROC glVertexAttrib4ubv;
				PFNGLVERTEXATTRIB4UIVPROC glVertexAttrib4uiv;
				PFNGLVERTEXATTRIB4USVPROC glVertexAttrib4usv;
				PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
			}v20;
			struct {
				PFNGLUNIFORMMATRIX2X3FVPROC glUniformMatrix2x3fv;
				PFNGLUNIFORMMATRIX3X2FVPROC glUniformMatrix3x2fv;
				PFNGLUNIFORMMATRIX2X4FVPROC glUniformMatrix2x4fv;
				PFNGLUNIFORMMATRIX4X2FVPROC glUniformMatrix4x2fv;
				PFNGLUNIFORMMATRIX3X4FVPROC glUniformMatrix3x4fv;
				PFNGLUNIFORMMATRIX4X3FVPROC glUniformMatrix4x3fv;
			}v21;
			struct {
				PFNGLCOLORMASKIPROC glColorMaski;
				PFNGLGETBOOLEANI_VPROC glGetBooleani_v;
				PFNGLGETINTEGERI_VPROC glGetIntegeri_v;
				PFNGLENABLEIPROC glEnablei;
				PFNGLDISABLEIPROC glDisablei;
				PFNGLISENABLEDIPROC glIsEnabledi;
				PFNGLBEGINTRANSFORMFEEDBACKPROC glBeginTransformFeedback;
				PFNGLENDTRANSFORMFEEDBACKPROC glEndTransformFeedback;
				PFNGLBINDBUFFERRANGEPROC glBindBufferRange;
				PFNGLBINDBUFFERBASEPROC glBindBufferBase;
				PFNGLTRANSFORMFEEDBACKVARYINGSPROC glTransformFeedbackVaryings;
				PFNGLGETTRANSFORMFEEDBACKVARYINGPROC glGetTransformFeedbackVarying;
				PFNGLCLAMPCOLORPROC glClampColor;
				PFNGLBEGINCONDITIONALRENDERPROC glBeginConditionalRender;
				PFNGLENDCONDITIONALRENDERPROC glEndConditionalRender;
				PFNGLVERTEXATTRIBIPOINTERPROC glVertexAttribIPointer;
				PFNGLGETVERTEXATTRIBIIVPROC glGetVertexAttribIiv;
				PFNGLGETVERTEXATTRIBIUIVPROC glGetVertexAttribIuiv;
				PFNGLVERTEXATTRIBI1IPROC glVertexAttribI1i;
				PFNGLVERTEXATTRIBI2IPROC glVertexAttribI2i;
				PFNGLVERTEXATTRIBI3IPROC glVertexAttribI3i;
				PFNGLVERTEXATTRIBI4IPROC glVertexAttribI4i;
				PFNGLVERTEXATTRIBI1UIPROC glVertexAttribI1ui;
				PFNGLVERTEXATTRIBI2UIPROC glVertexAttribI2ui;
				PFNGLVERTEXATTRIBI3UIPROC glVertexAttribI3ui;
				PFNGLVERTEXATTRIBI4UIPROC glVertexAttribI4ui;
				PFNGLVERTEXATTRIBI1IVPROC glVertexAttribI1iv;
				PFNGLVERTEXATTRIBI2IVPROC glVertexAttribI2iv;
				PFNGLVERTEXATTRIBI3IVPROC glVertexAttribI3iv;
				PFNGLVERTEXATTRIBI4IVPROC glVertexAttribI4iv;
				PFNGLVERTEXATTRIBI1UIVPROC glVertexAttribI1uiv;
				PFNGLVERTEXATTRIBI2UIVPROC glVertexAttribI2uiv;
				PFNGLVERTEXATTRIBI3UIVPROC glVertexAttribI3uiv;
				PFNGLVERTEXATTRIBI4UIVPROC glVertexAttribI4uiv;
				PFNGLVERTEXATTRIBI4BVPROC glVertexAttribI4bv;
				PFNGLVERTEXATTRIBI4SVPROC glVertexAttribI4sv;
				PFNGLVERTEXATTRIBI4UBVPROC glVertexAttribI4ubv;
				PFNGLVERTEXATTRIBI4USVPROC glVertexAttribI4usv;
				PFNGLGETUNIFORMUIVPROC glGetUniformuiv;
				PFNGLBINDFRAGDATALOCATIONPROC glBindFragDataLocation;
				PFNGLGETFRAGDATALOCATIONPROC glGetFragDataLocation;
				PFNGLUNIFORM1UIPROC glUniform1ui;
				PFNGLUNIFORM2UIPROC glUniform2ui;
				PFNGLUNIFORM3UIPROC glUniform3ui;
				PFNGLUNIFORM4UIPROC glUniform4ui;
				PFNGLUNIFORM1UIVPROC glUniform1uiv;
				PFNGLUNIFORM2UIVPROC glUniform2uiv;
				PFNGLUNIFORM3UIVPROC glUniform3uiv;
				PFNGLUNIFORM4UIVPROC glUniform4uiv;
				PFNGLTEXPARAMETERIIVPROC glTexParameterIiv;
				PFNGLTEXPARAMETERIUIVPROC glTexParameterIuiv;
				PFNGLGETTEXPARAMETERIIVPROC glGetTexParameterIiv;
				PFNGLGETTEXPARAMETERIUIVPROC glGetTexParameterIuiv;
				PFNGLCLEARBUFFERIVPROC glClearBufferiv;
				PFNGLCLEARBUFFERUIVPROC glClearBufferuiv;
				PFNGLCLEARBUFFERFVPROC glClearBufferfv;
				PFNGLCLEARBUFFERFIPROC glClearBufferfi;
				PFNGLGETSTRINGIPROC glGetStringi;
				PFNGLISRENDERBUFFERPROC glIsRenderbuffer;
				PFNGLBINDRENDERBUFFERPROC glBindRenderbuffer;
				PFNGLDELETERENDERBUFFERSPROC glDeleteRenderbuffers;
				PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers;
				PFNGLRENDERBUFFERSTORAGEPROC glRenderbufferStorage;
				PFNGLGETRENDERBUFFERPARAMETERIVPROC glGetRenderbufferParameteriv;
				PFNGLISFRAMEBUFFERPROC glIsFramebuffer;
				PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
				PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers;
				PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
				PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus;
				PFNGLFRAMEBUFFERTEXTURE1DPROC glFramebufferTexture1D;
				PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;
				PFNGLFRAMEBUFFERTEXTURE3DPROC glFramebufferTexture3D;
				PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer;
				PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC glGetFramebufferAttachmentParameteriv;
				PFNGLGENERATEMIPMAPPROC glGenerateMipmap;
				PFNGLBLITFRAMEBUFFERPROC glBlitFramebuffer;
				PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC glRenderbufferStorageMultisample;
				PFNGLFRAMEBUFFERTEXTURELAYERPROC glFramebufferTextureLayer;
				PFNGLMAPBUFFERRANGEPROC glMapBufferRange;
				PFNGLFLUSHMAPPEDBUFFERRANGEPROC glFlushMappedBufferRange;
				PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
				PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;
				PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
				PFNGLISVERTEXARRAYPROC glIsVertexArray;
			}v30;
			struct {
				PFNGLDRAWARRAYSINSTANCEDPROC glDrawArraysInstanced;
				PFNGLDRAWELEMENTSINSTANCEDPROC glDrawElementsInstanced;
				PFNGLTEXBUFFERPROC glTexBuffer;
				PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex;
				PFNGLCOPYBUFFERSUBDATAPROC glCopyBufferSubData;
				PFNGLGETUNIFORMINDICESPROC glGetUniformIndices;
				PFNGLGETACTIVEUNIFORMSIVPROC glGetActiveUniformsiv;
				PFNGLGETACTIVEUNIFORMNAMEPROC glGetActiveUniformName;
				PFNGLGETUNIFORMBLOCKINDEXPROC glGetUniformBlockIndex;
				PFNGLGETACTIVEUNIFORMBLOCKIVPROC glGetActiveUniformBlockiv;
				PFNGLGETACTIVEUNIFORMBLOCKNAMEPROC glGetActiveUniformBlockName;
				PFNGLUNIFORMBLOCKBINDINGPROC glUniformBlockBinding;
			}v31;
			struct {
				PFNGLDRAWELEMENTSBASEVERTEXPROC glDrawElementsBaseVertex;
				PFNGLDRAWRANGEELEMENTSBASEVERTEXPROC glDrawRangeElementsBaseVertex;
				PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC glDrawElementsInstancedBaseVertex;
				PFNGLMULTIDRAWELEMENTSBASEVERTEXPROC glMultiDrawElementsBaseVertex;
				PFNGLPROVOKINGVERTEXPROC glProvokingVertex;
				PFNGLFENCESYNCPROC glFenceSync;
				PFNGLISSYNCPROC glIsSync;
				PFNGLDELETESYNCPROC glDeleteSync;
				PFNGLCLIENTWAITSYNCPROC glClientWaitSync;
				PFNGLWAITSYNCPROC glWaitSync;
				PFNGLGETINTEGER64VPROC glGetInteger64v;
				PFNGLGETSYNCIVPROC glGetSynciv;
				PFNGLGETINTEGER64I_VPROC glGetInteger64i_v;
				PFNGLGETBUFFERPARAMETERI64VPROC glGetBufferParameteri64v;
				PFNGLFRAMEBUFFERTEXTUREPROC glFramebufferTexture;
				PFNGLTEXIMAGE2DMULTISAMPLEPROC glTexImage2DMultisample;
				PFNGLTEXIMAGE3DMULTISAMPLEPROC glTexImage3DMultisample;
				PFNGLGETMULTISAMPLEFVPROC glGetMultisamplefv;
				PFNGLSAMPLEMASKIPROC glSampleMaski;
				PFNGLVERTEXP2UIPROC glVertexP2ui;
				PFNGLVERTEXP2UIVPROC glVertexP2uiv;
				PFNGLVERTEXP3UIPROC glVertexP3ui;
				PFNGLVERTEXP3UIVPROC glVertexP3uiv;
				PFNGLVERTEXP4UIPROC glVertexP4ui;
				PFNGLVERTEXP4UIVPROC glVertexP4uiv;
				PFNGLTEXCOORDP1UIPROC glTexCoordP1ui;
				PFNGLTEXCOORDP1UIVPROC glTexCoordP1uiv;
				PFNGLTEXCOORDP2UIPROC glTexCoordP2ui;
				PFNGLTEXCOORDP2UIVPROC glTexCoordP2uiv;
				PFNGLTEXCOORDP3UIPROC glTexCoordP3ui;
				PFNGLTEXCOORDP3UIVPROC glTexCoordP3uiv;
				PFNGLTEXCOORDP4UIPROC glTexCoordP4ui;
				PFNGLTEXCOORDP4UIVPROC glTexCoordP4uiv;
				PFNGLMULTITEXCOORDP1UIPROC glMultiTexCoordP1ui;
				PFNGLMULTITEXCOORDP1UIVPROC glMultiTexCoordP1uiv;
				PFNGLMULTITEXCOORDP2UIPROC glMultiTexCoordP2ui;
				PFNGLMULTITEXCOORDP2UIVPROC glMultiTexCoordP2uiv;
				PFNGLMULTITEXCOORDP3UIPROC glMultiTexCoordP3ui;
				PFNGLMULTITEXCOORDP3UIVPROC glMultiTexCoordP3uiv;
				PFNGLMULTITEXCOORDP4UIPROC glMultiTexCoordP4ui;
				PFNGLMULTITEXCOORDP4UIVPROC glMultiTexCoordP4uiv;
				PFNGLNORMALP3UIPROC glNormalP3ui;
				PFNGLNORMALP3UIVPROC glNormalP3uiv;
				PFNGLCOLORP3UIPROC glColorP3ui;
				PFNGLCOLORP3UIVPROC glColorP3uiv;
				PFNGLCOLORP4UIPROC glColorP4ui;
				PFNGLCOLORP4UIVPROC glColorP4uiv;
				PFNGLSECONDARYCOLORP3UIPROC glSecondaryColorP3ui;
				PFNGLSECONDARYCOLORP3UIVPROC glSecondaryColorP3uiv;
			}v32;
			struct {
				PFNGLBINDFRAGDATALOCATIONINDEXEDPROC glBindFragDataLocationIndexed;
				PFNGLGETFRAGDATAINDEXPROC glGetFragDataIndex;
				PFNGLGENSAMPLERSPROC glGenSamplers;
				PFNGLDELETESAMPLERSPROC glDeleteSamplers;
				PFNGLISSAMPLERPROC glIsSampler;
				PFNGLBINDSAMPLERPROC glBindSampler;
				PFNGLSAMPLERPARAMETERIPROC glSamplerParameteri;
				PFNGLSAMPLERPARAMETERIVPROC glSamplerParameteriv;
				PFNGLSAMPLERPARAMETERFPROC glSamplerParameterf;
				PFNGLSAMPLERPARAMETERFVPROC glSamplerParameterfv;
				PFNGLSAMPLERPARAMETERIIVPROC glSamplerParameterIiv;
				PFNGLSAMPLERPARAMETERIUIVPROC glSamplerParameterIuiv;
				PFNGLGETSAMPLERPARAMETERIVPROC glGetSamplerParameteriv;
				PFNGLGETSAMPLERPARAMETERIIVPROC glGetSamplerParameterIiv;
				PFNGLGETSAMPLERPARAMETERFVPROC glGetSamplerParameterfv;
				PFNGLGETSAMPLERPARAMETERIUIVPROC glGetSamplerParameterIuiv;
				PFNGLQUERYCOUNTERPROC glQueryCounter;
				PFNGLGETQUERYOBJECTI64VPROC glGetQueryObjecti64v;
				PFNGLGETQUERYOBJECTUI64VPROC glGetQueryObjectui64v;
				PFNGLVERTEXATTRIBDIVISORPROC glVertexAttribDivisor;
				PFNGLVERTEXATTRIBP1UIPROC glVertexAttribP1ui;
				PFNGLVERTEXATTRIBP1UIVPROC glVertexAttribP1uiv;
				PFNGLVERTEXATTRIBP2UIPROC glVertexAttribP2ui;
				PFNGLVERTEXATTRIBP2UIVPROC glVertexAttribP2uiv;
				PFNGLVERTEXATTRIBP3UIPROC glVertexAttribP3ui;
				PFNGLVERTEXATTRIBP3UIVPROC glVertexAttribP3uiv;
				PFNGLVERTEXATTRIBP4UIPROC glVertexAttribP4ui;
				PFNGLVERTEXATTRIBP4UIVPROC glVertexAttribP4uiv;
			}v33;
			struct {
				PFNGLMINSAMPLESHADINGPROC glMinSampleShading;
				PFNGLBLENDEQUATIONIPROC glBlendEquationi;
				PFNGLBLENDEQUATIONSEPARATEIPROC glBlendEquationSeparatei;
				PFNGLBLENDFUNCIPROC glBlendFunci;
				PFNGLBLENDFUNCSEPARATEIPROC glBlendFuncSeparatei;
				PFNGLDRAWARRAYSINDIRECTPROC glDrawArraysIndirect;
				PFNGLDRAWELEMENTSINDIRECTPROC glDrawElementsIndirect;
				PFNGLUNIFORM1DPROC glUniform1d;
				PFNGLUNIFORM2DPROC glUniform2d;
				PFNGLUNIFORM3DPROC glUniform3d;
				PFNGLUNIFORM4DPROC glUniform4d;
				PFNGLUNIFORM1DVPROC glUniform1dv;
				PFNGLUNIFORM2DVPROC glUniform2dv;
				PFNGLUNIFORM3DVPROC glUniform3dv;
				PFNGLUNIFORM4DVPROC glUniform4dv;
				PFNGLUNIFORMMATRIX2DVPROC glUniformMatrix2dv;
				PFNGLUNIFORMMATRIX3DVPROC glUniformMatrix3dv;
				PFNGLUNIFORMMATRIX4DVPROC glUniformMatrix4dv;
				PFNGLUNIFORMMATRIX2X3DVPROC glUniformMatrix2x3dv;
				PFNGLUNIFORMMATRIX2X4DVPROC glUniformMatrix2x4dv;
				PFNGLUNIFORMMATRIX3X2DVPROC glUniformMatrix3x2dv;
				PFNGLUNIFORMMATRIX3X4DVPROC glUniformMatrix3x4dv;
				PFNGLUNIFORMMATRIX4X2DVPROC glUniformMatrix4x2dv;
				PFNGLUNIFORMMATRIX4X3DVPROC glUniformMatrix4x3dv;
				PFNGLGETUNIFORMDVPROC glGetUniformdv;
				PFNGLGETSUBROUTINEUNIFORMLOCATIONPROC glGetSubroutineUniformLocation;
				PFNGLGETSUBROUTINEINDEXPROC glGetSubroutineIndex;
				PFNGLGETACTIVESUBROUTINEUNIFORMIVPROC glGetActiveSubroutineUniformiv;
				PFNGLGETACTIVESUBROUTINEUNIFORMNAMEPROC glGetActiveSubroutineUniformName;
				PFNGLGETACTIVESUBROUTINENAMEPROC glGetActiveSubroutineName;
				PFNGLUNIFORMSUBROUTINESUIVPROC glUniformSubroutinesuiv;
				PFNGLGETUNIFORMSUBROUTINEUIVPROC glGetUniformSubroutineuiv;
				PFNGLGETPROGRAMSTAGEIVPROC glGetProgramStageiv;
				PFNGLPATCHPARAMETERIPROC glPatchParameteri;
				PFNGLPATCHPARAMETERFVPROC glPatchParameterfv;
				PFNGLBINDTRANSFORMFEEDBACKPROC glBindTransformFeedback;
				PFNGLDELETETRANSFORMFEEDBACKSPROC glDeleteTransformFeedbacks;
				PFNGLGENTRANSFORMFEEDBACKSPROC glGenTransformFeedbacks;
				PFNGLISTRANSFORMFEEDBACKPROC glIsTransformFeedback;
				PFNGLPAUSETRANSFORMFEEDBACKPROC glPauseTransformFeedback;
				PFNGLRESUMETRANSFORMFEEDBACKPROC glResumeTransformFeedback;
				PFNGLDRAWTRANSFORMFEEDBACKPROC glDrawTransformFeedback;
				PFNGLDRAWTRANSFORMFEEDBACKSTREAMPROC glDrawTransformFeedbackStream;
				PFNGLBEGINQUERYINDEXEDPROC glBeginQueryIndexed;
				PFNGLENDQUERYINDEXEDPROC glEndQueryIndexed;
				PFNGLGETQUERYINDEXEDIVPROC glGetQueryIndexediv;
			}v40;
			struct {
				PFNGLRELEASESHADERCOMPILERPROC glReleaseShaderCompiler;
				PFNGLSHADERBINARYPROC glShaderBinary;
				PFNGLGETSHADERPRECISIONFORMATPROC glGetShaderPrecisionFormat;
				PFNGLDEPTHRANGEFPROC glDepthRangef;
				PFNGLCLEARDEPTHFPROC glClearDepthf;
				PFNGLGETPROGRAMBINARYPROC glGetProgramBinary;
				PFNGLPROGRAMBINARYPROC glProgramBinary;
				PFNGLPROGRAMPARAMETERIPROC glProgramParameteri;
				PFNGLUSEPROGRAMSTAGESPROC glUseProgramStages;
				PFNGLACTIVESHADERPROGRAMPROC glActiveShaderProgram;
				PFNGLCREATESHADERPROGRAMVPROC glCreateShaderProgramv;
				PFNGLBINDPROGRAMPIPELINEPROC glBindProgramPipeline;
				PFNGLDELETEPROGRAMPIPELINESPROC glDeleteProgramPipelines;
				PFNGLGENPROGRAMPIPELINESPROC glGenProgramPipelines;
				PFNGLISPROGRAMPIPELINEPROC glIsProgramPipeline;
				PFNGLGETPROGRAMPIPELINEIVPROC glGetProgramPipelineiv;
				PFNGLPROGRAMUNIFORM1IPROC glProgramUniform1i;
				PFNGLPROGRAMUNIFORM1IVPROC glProgramUniform1iv;
				PFNGLPROGRAMUNIFORM1FPROC glProgramUniform1f;
				PFNGLPROGRAMUNIFORM1FVPROC glProgramUniform1fv;
				PFNGLPROGRAMUNIFORM1DPROC glProgramUniform1d;
				PFNGLPROGRAMUNIFORM1DVPROC glProgramUniform1dv;
				PFNGLPROGRAMUNIFORM1UIPROC glProgramUniform1ui;
				PFNGLPROGRAMUNIFORM1UIVPROC glProgramUniform1uiv;
				PFNGLPROGRAMUNIFORM2IPROC glProgramUniform2i;
				PFNGLPROGRAMUNIFORM2IVPROC glProgramUniform2iv;
				PFNGLPROGRAMUNIFORM2FPROC glProgramUniform2f;
				PFNGLPROGRAMUNIFORM2FVPROC glProgramUniform2fv;
				PFNGLPROGRAMUNIFORM2DPROC glProgramUniform2d;
				PFNGLPROGRAMUNIFORM2DVPROC glProgramUniform2dv;
				PFNGLPROGRAMUNIFORM2UIPROC glProgramUniform2ui;
				PFNGLPROGRAMUNIFORM2UIVPROC glProgramUniform2uiv;
				PFNGLPROGRAMUNIFORM3IPROC glProgramUniform3i;
				PFNGLPROGRAMUNIFORM3IVPROC glProgramUniform3iv;
				PFNGLPROGRAMUNIFORM3FPROC glProgramUniform3f;
				PFNGLPROGRAMUNIFORM3FVPROC glProgramUniform3fv;
				PFNGLPROGRAMUNIFORM3DPROC glProgramUniform3d;
				PFNGLPROGRAMUNIFORM3DVPROC glProgramUniform3dv;
				PFNGLPROGRAMUNIFORM3UIPROC glProgramUniform3ui;
				PFNGLPROGRAMUNIFORM3UIVPROC glProgramUniform3uiv;
				PFNGLPROGRAMUNIFORM4IPROC glProgramUniform4i;
				PFNGLPROGRAMUNIFORM4IVPROC glProgramUniform4iv;
				PFNGLPROGRAMUNIFORM4FPROC glProgramUniform4f;
				PFNGLPROGRAMUNIFORM4FVPROC glProgramUniform4fv;
				PFNGLPROGRAMUNIFORM4DPROC glProgramUniform4d;
				PFNGLPROGRAMUNIFORM4DVPROC glProgramUniform4dv;
				PFNGLPROGRAMUNIFORM4UIPROC glProgramUniform4ui;
				PFNGLPROGRAMUNIFORM4UIVPROC glProgramUniform4uiv;
				PFNGLPROGRAMUNIFORMMATRIX2FVPROC glProgramUniformMatrix2fv;
				PFNGLPROGRAMUNIFORMMATRIX3FVPROC glProgramUniformMatrix3fv;
				PFNGLPROGRAMUNIFORMMATRIX4FVPROC glProgramUniformMatrix4fv;
				PFNGLPROGRAMUNIFORMMATRIX2DVPROC glProgramUniformMatrix2dv;
				PFNGLPROGRAMUNIFORMMATRIX3DVPROC glProgramUniformMatrix3dv;
				PFNGLPROGRAMUNIFORMMATRIX4DVPROC glProgramUniformMatrix4dv;
				PFNGLPROGRAMUNIFORMMATRIX2X3FVPROC glProgramUniformMatrix2x3fv;
				PFNGLPROGRAMUNIFORMMATRIX3X2FVPROC glProgramUniformMatrix3x2fv;
				PFNGLPROGRAMUNIFORMMATRIX2X4FVPROC glProgramUniformMatrix2x4fv;
				PFNGLPROGRAMUNIFORMMATRIX4X2FVPROC glProgramUniformMatrix4x2fv;
				PFNGLPROGRAMUNIFORMMATRIX3X4FVPROC glProgramUniformMatrix3x4fv;
				PFNGLPROGRAMUNIFORMMATRIX4X3FVPROC glProgramUniformMatrix4x3fv;
				PFNGLPROGRAMUNIFORMMATRIX2X3DVPROC glProgramUniformMatrix2x3dv;
				PFNGLPROGRAMUNIFORMMATRIX3X2DVPROC glProgramUniformMatrix3x2dv;
				PFNGLPROGRAMUNIFORMMATRIX2X4DVPROC glProgramUniformMatrix2x4dv;
				PFNGLPROGRAMUNIFORMMATRIX4X2DVPROC glProgramUniformMatrix4x2dv;
				PFNGLPROGRAMUNIFORMMATRIX3X4DVPROC glProgramUniformMatrix3x4dv;
				PFNGLPROGRAMUNIFORMMATRIX4X3DVPROC glProgramUniformMatrix4x3dv;
				PFNGLVALIDATEPROGRAMPIPELINEPROC glValidateProgramPipeline;
				PFNGLGETPROGRAMPIPELINEINFOLOGPROC glGetProgramPipelineInfoLog;
				PFNGLVERTEXATTRIBL1DPROC glVertexAttribL1d;
				PFNGLVERTEXATTRIBL2DPROC glVertexAttribL2d;
				PFNGLVERTEXATTRIBL3DPROC glVertexAttribL3d;
				PFNGLVERTEXATTRIBL4DPROC glVertexAttribL4d;
				PFNGLVERTEXATTRIBL1DVPROC glVertexAttribL1dv;
				PFNGLVERTEXATTRIBL2DVPROC glVertexAttribL2dv;
				PFNGLVERTEXATTRIBL3DVPROC glVertexAttribL3dv;
				PFNGLVERTEXATTRIBL4DVPROC glVertexAttribL4dv;
				PFNGLVERTEXATTRIBLPOINTERPROC glVertexAttribLPointer;
				PFNGLGETVERTEXATTRIBLDVPROC glGetVertexAttribLdv;
				PFNGLVIEWPORTARRAYVPROC glViewportArrayv;
				PFNGLVIEWPORTINDEXEDFPROC glViewportIndexedf;
				PFNGLVIEWPORTINDEXEDFVPROC glViewportIndexedfv;
				PFNGLSCISSORARRAYVPROC glScissorArrayv;
				PFNGLSCISSORINDEXEDPROC glScissorIndexed;
				PFNGLSCISSORINDEXEDVPROC glScissorIndexedv;
				PFNGLDEPTHRANGEARRAYVPROC glDepthRangeArrayv;
				PFNGLDEPTHRANGEINDEXEDPROC glDepthRangeIndexed;
				PFNGLGETFLOATI_VPROC glGetFloati_v;
				PFNGLGETDOUBLEI_VPROC glGetDoublei_v;
			}v41;
			struct {
				PFNGLDRAWARRAYSINSTANCEDBASEINSTANCEPROC glDrawArraysInstancedBaseInstance;
				PFNGLDRAWELEMENTSINSTANCEDBASEINSTANCEPROC glDrawElementsInstancedBaseInstance;
				PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXBASEINSTANCEPROC glDrawElementsInstancedBaseVertexBaseInstance;
				PFNGLGETINTERNALFORMATIVPROC glGetInternalformativ;
				PFNGLGETACTIVEATOMICCOUNTERBUFFERIVPROC glGetActiveAtomicCounterBufferiv;
				PFNGLBINDIMAGETEXTUREPROC glBindImageTexture;
				PFNGLMEMORYBARRIERPROC glMemoryBarrier;
				PFNGLTEXSTORAGE1DPROC glTexStorage1D;
				PFNGLTEXSTORAGE2DPROC glTexStorage2D;
				PFNGLTEXSTORAGE3DPROC glTexStorage3D;
				PFNGLDRAWTRANSFORMFEEDBACKINSTANCEDPROC glDrawTransformFeedbackInstanced;
				PFNGLDRAWTRANSFORMFEEDBACKSTREAMINSTANCEDPROC glDrawTransformFeedbackStreamInstanced;
			}v42;
			struct {
				PFNGLCLEARBUFFERDATAPROC glClearBufferData;
				PFNGLCLEARBUFFERSUBDATAPROC glClearBufferSubData;
				PFNGLDISPATCHCOMPUTEPROC glDispatchCompute;
				PFNGLDISPATCHCOMPUTEINDIRECTPROC glDispatchComputeIndirect;
				PFNGLCOPYIMAGESUBDATAPROC glCopyImageSubData;
				PFNGLFRAMEBUFFERPARAMETERIPROC glFramebufferParameteri;
				PFNGLGETFRAMEBUFFERPARAMETERIVPROC glGetFramebufferParameteriv;
				PFNGLGETINTERNALFORMATI64VPROC glGetInternalformati64v;
				PFNGLINVALIDATETEXSUBIMAGEPROC glInvalidateTexSubImage;
				PFNGLINVALIDATETEXIMAGEPROC glInvalidateTexImage;
				PFNGLINVALIDATEBUFFERSUBDATAPROC glInvalidateBufferSubData;
				PFNGLINVALIDATEBUFFERDATAPROC glInvalidateBufferData;
				PFNGLINVALIDATEFRAMEBUFFERPROC glInvalidateFramebuffer;
				PFNGLINVALIDATESUBFRAMEBUFFERPROC glInvalidateSubFramebuffer;
				PFNGLMULTIDRAWARRAYSINDIRECTPROC glMultiDrawArraysIndirect;
				PFNGLMULTIDRAWELEMENTSINDIRECTPROC glMultiDrawElementsIndirect;
				PFNGLGETPROGRAMINTERFACEIVPROC glGetProgramInterfaceiv;
				PFNGLGETPROGRAMRESOURCEINDEXPROC glGetProgramResourceIndex;
				PFNGLGETPROGRAMRESOURCENAMEPROC glGetProgramResourceName;
				PFNGLGETPROGRAMRESOURCEIVPROC glGetProgramResourceiv;
				PFNGLGETPROGRAMRESOURCELOCATIONPROC glGetProgramResourceLocation;
				PFNGLGETPROGRAMRESOURCELOCATIONINDEXPROC glGetProgramResourceLocationIndex;
				PFNGLSHADERSTORAGEBLOCKBINDINGPROC glShaderStorageBlockBinding;
				PFNGLTEXBUFFERRANGEPROC glTexBufferRange;
				PFNGLTEXSTORAGE2DMULTISAMPLEPROC glTexStorage2DMultisample;
				PFNGLTEXSTORAGE3DMULTISAMPLEPROC glTexStorage3DMultisample;
				PFNGLTEXTUREVIEWPROC glTextureView;
				PFNGLBINDVERTEXBUFFERPROC glBindVertexBuffer;
				PFNGLVERTEXATTRIBFORMATPROC glVertexAttribFormat;
				PFNGLVERTEXATTRIBIFORMATPROC glVertexAttribIFormat;
				PFNGLVERTEXATTRIBLFORMATPROC glVertexAttribLFormat;
				PFNGLVERTEXATTRIBBINDINGPROC glVertexAttribBinding;
				PFNGLVERTEXBINDINGDIVISORPROC glVertexBindingDivisor;
				PFNGLDEBUGMESSAGECONTROLPROC glDebugMessageControl;
				PFNGLDEBUGMESSAGEINSERTPROC glDebugMessageInsert;
				PFNGLDEBUGMESSAGECALLBACKPROC glDebugMessageCallback;
				PFNGLGETDEBUGMESSAGELOGPROC glGetDebugMessageLog;
				PFNGLPUSHDEBUGGROUPPROC glPushDebugGroup;
				PFNGLPOPDEBUGGROUPPROC glPopDebugGroup;
				PFNGLOBJECTLABELPROC glObjectLabel;
				PFNGLGETOBJECTLABELPROC glGetObjectLabel;
				PFNGLOBJECTPTRLABELPROC glObjectPtrLabel;
				PFNGLGETOBJECTPTRLABELPROC glGetObjectPtrLabel;
				PFNGLGETPOINTERVPROC glGetPointerv;
			}v43;
			struct {
				PFNGLBUFFERSTORAGEPROC glBufferStorage;
				PFNGLCLEARTEXIMAGEPROC glClearTexImage;
				PFNGLCLEARTEXSUBIMAGEPROC glClearTexSubImage;
				PFNGLBINDBUFFERSBASEPROC glBindBuffersBase;
				PFNGLBINDBUFFERSRANGEPROC glBindBuffersRange;
				PFNGLBINDTEXTURESPROC glBindTextures;
				PFNGLBINDSAMPLERSPROC glBindSamplers;
				PFNGLBINDIMAGETEXTURESPROC glBindImageTextures;
				PFNGLBINDVERTEXBUFFERSPROC glBindVertexBuffers;
			}v44;
			struct {
				PFNGLCLIPCONTROLPROC glClipControl;
				PFNGLCREATETRANSFORMFEEDBACKSPROC glCreateTransformFeedbacks;
				PFNGLTRANSFORMFEEDBACKBUFFERBASEPROC glTransformFeedbackBufferBase;
				PFNGLTRANSFORMFEEDBACKBUFFERRANGEPROC glTransformFeedbackBufferRange;
				PFNGLGETTRANSFORMFEEDBACKIVPROC glGetTransformFeedbackiv;
				PFNGLGETTRANSFORMFEEDBACKI_VPROC glGetTransformFeedbacki_v;
				PFNGLGETTRANSFORMFEEDBACKI64_VPROC glGetTransformFeedbacki64_v;
				PFNGLCREATEBUFFERSPROC glCreateBuffers;
				PFNGLNAMEDBUFFERSTORAGEPROC glNamedBufferStorage;
				PFNGLNAMEDBUFFERDATAPROC glNamedBufferData;
				PFNGLNAMEDBUFFERSUBDATAPROC glNamedBufferSubData;
				PFNGLCOPYNAMEDBUFFERSUBDATAPROC glCopyNamedBufferSubData;
				PFNGLCLEARNAMEDBUFFERDATAPROC glClearNamedBufferData;
				PFNGLCLEARNAMEDBUFFERSUBDATAPROC glClearNamedBufferSubData;
				PFNGLMAPNAMEDBUFFERPROC glMapNamedBuffer;
				PFNGLMAPNAMEDBUFFERRANGEPROC glMapNamedBufferRange;
				PFNGLUNMAPNAMEDBUFFERPROC glUnmapNamedBuffer;
				PFNGLFLUSHMAPPEDNAMEDBUFFERRANGEPROC glFlushMappedNamedBufferRange;
				PFNGLGETNAMEDBUFFERPARAMETERIVPROC glGetNamedBufferParameteriv;
				PFNGLGETNAMEDBUFFERPARAMETERI64VPROC glGetNamedBufferParameteri64v;
				PFNGLGETNAMEDBUFFERPOINTERVPROC glGetNamedBufferPointerv;
				PFNGLGETNAMEDBUFFERSUBDATAPROC glGetNamedBufferSubData;
				PFNGLCREATEFRAMEBUFFERSPROC glCreateFramebuffers;
				PFNGLNAMEDFRAMEBUFFERRENDERBUFFERPROC glNamedFramebufferRenderbuffer;
				PFNGLNAMEDFRAMEBUFFERPARAMETERIPROC glNamedFramebufferParameteri;
				PFNGLNAMEDFRAMEBUFFERTEXTUREPROC glNamedFramebufferTexture;
				PFNGLNAMEDFRAMEBUFFERTEXTURELAYERPROC glNamedFramebufferTextureLayer;
				PFNGLNAMEDFRAMEBUFFERDRAWBUFFERPROC glNamedFramebufferDrawBuffer;
				PFNGLNAMEDFRAMEBUFFERDRAWBUFFERSPROC glNamedFramebufferDrawBuffers;
				PFNGLNAMEDFRAMEBUFFERREADBUFFERPROC glNamedFramebufferReadBuffer;
				PFNGLINVALIDATENAMEDFRAMEBUFFERDATAPROC glInvalidateNamedFramebufferData;
				PFNGLINVALIDATENAMEDFRAMEBUFFERSUBDATAPROC glInvalidateNamedFramebufferSubData;
				PFNGLCLEARNAMEDFRAMEBUFFERIVPROC glClearNamedFramebufferiv;
				PFNGLCLEARNAMEDFRAMEBUFFERUIVPROC glClearNamedFramebufferuiv;
				PFNGLCLEARNAMEDFRAMEBUFFERFVPROC glClearNamedFramebufferfv;
				PFNGLCLEARNAMEDFRAMEBUFFERFIPROC glClearNamedFramebufferfi;
				PFNGLBLITNAMEDFRAMEBUFFERPROC glBlitNamedFramebuffer;
				PFNGLCHECKNAMEDFRAMEBUFFERSTATUSPROC glCheckNamedFramebufferStatus;
				PFNGLGETNAMEDFRAMEBUFFERPARAMETERIVPROC glGetNamedFramebufferParameteriv;
				PFNGLGETNAMEDFRAMEBUFFERATTACHMENTPARAMETERIVPROC glGetNamedFramebufferAttachmentParameteriv;
				PFNGLCREATERENDERBUFFERSPROC glCreateRenderbuffers;
				PFNGLNAMEDRENDERBUFFERSTORAGEPROC glNamedRenderbufferStorage;
				PFNGLNAMEDRENDERBUFFERSTORAGEMULTISAMPLEPROC glNamedRenderbufferStorageMultisample;
				PFNGLGETNAMEDRENDERBUFFERPARAMETERIVPROC glGetNamedRenderbufferParameteriv;
				PFNGLCREATETEXTURESPROC glCreateTextures;
				PFNGLTEXTUREBUFFERPROC glTextureBuffer;
				PFNGLTEXTUREBUFFERRANGEPROC glTextureBufferRange;
				PFNGLTEXTURESTORAGE1DPROC glTextureStorage1D;
				PFNGLTEXTURESTORAGE2DPROC glTextureStorage2D;
				PFNGLTEXTURESTORAGE3DPROC glTextureStorage3D;
				PFNGLTEXTURESTORAGE2DMULTISAMPLEPROC glTextureStorage2DMultisample;
				PFNGLTEXTURESTORAGE3DMULTISAMPLEPROC glTextureStorage3DMultisample;
				PFNGLTEXTURESUBIMAGE1DPROC glTextureSubImage1D;
				PFNGLTEXTURESUBIMAGE2DPROC glTextureSubImage2D;
				PFNGLTEXTURESUBIMAGE3DPROC glTextureSubImage3D;
				PFNGLCOMPRESSEDTEXTURESUBIMAGE1DPROC glCompressedTextureSubImage1D;
				PFNGLCOMPRESSEDTEXTURESUBIMAGE2DPROC glCompressedTextureSubImage2D;
				PFNGLCOMPRESSEDTEXTURESUBIMAGE3DPROC glCompressedTextureSubImage3D;
				PFNGLCOPYTEXTURESUBIMAGE1DPROC glCopyTextureSubImage1D;
				PFNGLCOPYTEXTURESUBIMAGE2DPROC glCopyTextureSubImage2D;
				PFNGLCOPYTEXTURESUBIMAGE3DPROC glCopyTextureSubImage3D;
				PFNGLTEXTUREPARAMETERFPROC glTextureParameterf;
				PFNGLTEXTUREPARAMETERFVPROC glTextureParameterfv;
				PFNGLTEXTUREPARAMETERIPROC glTextureParameteri;
				PFNGLTEXTUREPARAMETERIIVPROC glTextureParameterIiv;
				PFNGLTEXTUREPARAMETERIUIVPROC glTextureParameterIuiv;
				PFNGLTEXTUREPARAMETERIVPROC glTextureParameteriv;
				PFNGLGENERATETEXTUREMIPMAPPROC glGenerateTextureMipmap;
				PFNGLBINDTEXTUREUNITPROC glBindTextureUnit;
				PFNGLGETTEXTUREIMAGEPROC glGetTextureImage;
				PFNGLGETCOMPRESSEDTEXTUREIMAGEPROC glGetCompressedTextureImage;
				PFNGLGETTEXTURELEVELPARAMETERFVPROC glGetTextureLevelParameterfv;
				PFNGLGETTEXTURELEVELPARAMETERIVPROC glGetTextureLevelParameteriv;
				PFNGLGETTEXTUREPARAMETERFVPROC glGetTextureParameterfv;
				PFNGLGETTEXTUREPARAMETERIIVPROC glGetTextureParameterIiv;
				PFNGLGETTEXTUREPARAMETERIUIVPROC glGetTextureParameterIuiv;
				PFNGLGETTEXTUREPARAMETERIVPROC glGetTextureParameteriv;
				PFNGLCREATEVERTEXARRAYSPROC glCreateVertexArrays;
				PFNGLDISABLEVERTEXARRAYATTRIBPROC glDisableVertexArrayAttrib;
				PFNGLENABLEVERTEXARRAYATTRIBPROC glEnableVertexArrayAttrib;
				PFNGLVERTEXARRAYELEMENTBUFFERPROC glVertexArrayElementBuffer;
				PFNGLVERTEXARRAYVERTEXBUFFERPROC glVertexArrayVertexBuffer;
				PFNGLVERTEXARRAYVERTEXBUFFERSPROC glVertexArrayVertexBuffers;
				PFNGLVERTEXARRAYATTRIBBINDINGPROC glVertexArrayAttribBinding;
				PFNGLVERTEXARRAYATTRIBFORMATPROC glVertexArrayAttribFormat;
				PFNGLVERTEXARRAYATTRIBIFORMATPROC glVertexArrayAttribIFormat;
				PFNGLVERTEXARRAYATTRIBLFORMATPROC glVertexArrayAttribLFormat;
				PFNGLVERTEXARRAYBINDINGDIVISORPROC glVertexArrayBindingDivisor;
				PFNGLGETVERTEXARRAYIVPROC glGetVertexArrayiv;
				PFNGLGETVERTEXARRAYINDEXEDIVPROC glGetVertexArrayIndexediv;
				PFNGLGETVERTEXARRAYINDEXED64IVPROC glGetVertexArrayIndexed64iv;
				PFNGLCREATESAMPLERSPROC glCreateSamplers;
				PFNGLCREATEPROGRAMPIPELINESPROC glCreateProgramPipelines;
				PFNGLCREATEQUERIESPROC glCreateQueries;
				PFNGLGETQUERYBUFFEROBJECTI64VPROC glGetQueryBufferObjecti64v;
				PFNGLGETQUERYBUFFEROBJECTIVPROC glGetQueryBufferObjectiv;
				PFNGLGETQUERYBUFFEROBJECTUI64VPROC glGetQueryBufferObjectui64v;
				PFNGLGETQUERYBUFFEROBJECTUIVPROC glGetQueryBufferObjectuiv;
				PFNGLMEMORYBARRIERBYREGIONPROC glMemoryBarrierByRegion;
				PFNGLGETTEXTURESUBIMAGEPROC glGetTextureSubImage;
				PFNGLGETCOMPRESSEDTEXTURESUBIMAGEPROC glGetCompressedTextureSubImage;
				PFNGLGETGRAPHICSRESETSTATUSPROC glGetGraphicsResetStatus;
				PFNGLGETNCOMPRESSEDTEXIMAGEPROC glGetnCompressedTexImage;
				PFNGLGETNTEXIMAGEPROC glGetnTexImage;
				PFNGLGETNUNIFORMDVPROC glGetnUniformdv;
				PFNGLGETNUNIFORMFVPROC glGetnUniformfv;
				PFNGLGETNUNIFORMIVPROC glGetnUniformiv;
				PFNGLGETNUNIFORMUIVPROC glGetnUniformuiv;
				PFNGLREADNPIXELSPROC glReadnPixels;
				PFNGLGETNMAPDVPROC glGetnMapdv;
				PFNGLGETNMAPFVPROC glGetnMapfv;
				PFNGLGETNMAPIVPROC glGetnMapiv;
				PFNGLGETNPIXELMAPFVPROC glGetnPixelMapfv;
				PFNGLGETNPIXELMAPUIVPROC glGetnPixelMapuiv;
				PFNGLGETNPIXELMAPUSVPROC glGetnPixelMapusv;
				PFNGLGETNPOLYGONSTIPPLEPROC glGetnPolygonStipple;
				PFNGLGETNCOLORTABLEPROC glGetnColorTable;
				PFNGLGETNCONVOLUTIONFILTERPROC glGetnConvolutionFilter;
				PFNGLGETNSEPARABLEFILTERPROC glGetnSeparableFilter;
				PFNGLGETNHISTOGRAMPROC glGetnHistogram;
				PFNGLGETNMINMAXPROC glGetnMinmax;
				PFNGLTEXTUREBARRIERPROC glTextureBarrier;
			}v45;
		};

		//lockless context properties per thread
#if defined(__APPLE__) && defined(__clang__)
		__thread ContextProperties *thread_local_context = nullptr;
#else
		thread_local ContextProperties *thread_local_context = nullptr;
#endif

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		InputState::InputState()
		{
			std::fill_n(keys, (int)Key::MAX, false);
			uint64_t timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - detail::gstate.timestamp_program).count();
			std::fill_n(keys_timestamp, (int)Key::MAX, (uint64_t)timestamp);
			mod_alt = mod_control = mod_shift = mod_system = false;
			std::fill_n(mouse.buttons, (int)MouseButton::MAX, false);
			std::fill_n(mouse.buttons_timestamp, (int)MouseButton::MAX, timestamp);
			mouse.last_scroll_x = mouse.last_scroll_y = 0.0f;
			mouse.position_x = mouse.position_y = mouse.prev_position_x = mouse.prev_position_y = 0;
			for (int i = 0; i < (int)Controller::MAX; i++) {
				std::fill_n(controller[i].axes, JOYSTICK_MAX_AXES, 0.0f);
				std::fill_n(controller[i].buttons, (int)ControllerButton::MAX, false);
				controller[i].connected = false;
				controller[i].name = "";
			}
			clipboard_text = "";
		}
		

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		InputRemapper::InputRemapper() {
			keys.resize((int)Key::MAX + 1);
			mouse_buttons.resize((int)MouseButton::MAX + 1);
			controller_buttons.resize((int)ControllerButton::MAX + 1);
			reset();
		}
		void InputRemapper::add(Key from, Key to) {
			if (from == to) return;
			keys[(int)from] = (int)to;
		}
		void InputRemapper::add(MouseButton from, MouseButton to) {
			if (from == to) return;
			mouse_buttons[(int)from] = (int)to;
		}
		void InputRemapper::add(ControllerButton from, ControllerButton to) {
			if (from == to) return;
			controller_buttons[(int)from] = (int)to;
		}
		void InputRemapper::swap(Key first, Key second) {
			if (first == second) return;
			keys[(int)first] = (int)second;
			keys[(int)second] = (int)first;
		}
		void InputRemapper::swap(MouseButton first, MouseButton second) {
			if (first == second) return;
			mouse_buttons[(int)first] = (int)second;
			mouse_buttons[(int)second] = (int)first;
		}
		void InputRemapper::swap(ControllerButton first, ControllerButton second) {
			if (first == second) return;
			controller_buttons[(int)first] = (int)second;
			controller_buttons[(int)second] = (int)first;
		}
		void InputRemapper::reset(Key from) {
			keys[(int)from] = (int)from;
		}
		void InputRemapper::reset(MouseButton from) {
			mouse_buttons[(int)from] = (int)from;
		}
		void InputRemapper::reset(ControllerButton from) {
			controller_buttons[(int)from] = (int)from;
		}
		void InputRemapper::reset() {
			for (int i = 0; i <= (int)Key::MAX; i++) keys[i] = i;
			for (int i = 0; i <= (int)MouseButton::MAX; i++) mouse_buttons[i] = i;
			for (int i = 0; i <= (int)ControllerButton::MAX; i++) controller_buttons[i] = i;
		}
		void InputRemapper::internalRemap(std::vector<Event>& events){
			for (auto& e: events) {
				switch (e.type) {
				case Event::Type::KeyPress:
					e.KeyPress.key = (Key)keys[(int)e.KeyPress.key];
					break;
				case Event::Type::KeyRelease:
					e.KeyRelease.key = (Key)keys[(int)e.KeyRelease.key];
					break;
				case Event::Type::KeyRepeat:
					e.KeyRepeat.key = (Key)keys[(int)e.KeyRepeat.key];
					break;
				case Event::Type::MousePress:
					e.MousePress.button = (MouseButton)mouse_buttons[(int)e.MousePress.button];
					break;
				case Event::Type::MouseRelease:
					e.MouseRelease.button = (MouseButton)mouse_buttons[(int)e.MouseRelease.button];
					break;
				case Event::Type::MouseDrag:
					e.MouseDrag.button = (MouseButton)mouse_buttons[(int)e.MouseDrag.button];
					break;
				case Event::Type::ControllerButtonPress:
					e.ControllerButtonPress.button = (ControllerButton)controller_buttons[(int)e.ControllerButtonPress.button];
					break;
				case Event::Type::ControllerButtonRelease:
					e.ControllerButtonRelease.button = (ControllerButton)controller_buttons[(int)e.ControllerButtonRelease.button];
					break;
				default:
					break;
				}
			}
		}


		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		Event::Event() {
			type = Type::MonitorConnect;
			timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - detail::gstate.timestamp_program).count();
		}
		Event::~Event()
		{
			if (type == Event::Type::FilesDrop) delete FilesDrop.paths;
		}



		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		namespace detail {
			struct WindowBlockingCommand {
				enum class Type : int {
					CREATE,
					DESTROY,
					HIDE,
					MINIMIZE,
					MAXIMIZE,
					RESTORE,
					FOCUS,
					MOVE,
					RESIZE,
					TITLE,
					SHOW,
					FULLSCREEN_MONITOR,
					WINDOWED_MONITOR,
					CLIPBOARD,
					INPUT
				};
				WindowBlockingCommand(Type intype) {
					type = intype;
				}
				void wait() {
					//will not be called in singlethreaded mode
					std::unique_lock<std::mutex> lk(mutex);
					condition.wait(lk, [this] {return finished; });
				}
				void notify() {
					if(gstate.multithreaded) mutex.lock();
					finished = true;
					if (gstate.multithreaded) condition.notify_all();
					if (gstate.multithreaded) mutex.unlock();
				}
				struct CreateCmd {
					WindowProperties* wp;
					FramebufferProperties* fp;
					ContextProperties* cp;
					InputProperties* ip;
					const Monitor* monitor;
					GLFWmonitor* handle_monitor;
					GLFWwindow* handle_window;
					GLFWwindow* handle_shared_window;
				};
				struct DestroyCmd {
					GLFWwindow* handle_window;
				};
				struct HideCmd {
					GLFWwindow* handle_window;
				};
				struct MinimizeCmd {
					GLFWwindow* handle_window;
				};
				struct MaximizeCmd {
					GLFWwindow* handle_window;
				};
				struct RestoreCmd {
					GLFWwindow* handle_window;
				};
				struct FocusCmd {
					GLFWwindow* handle_window;
				};
				struct MoveCmd {
					unsigned int position_x;
					unsigned int position_y;
					GLFWwindow* handle_window;
				};
				struct ResizeCmd {
					GLFWwindow* handle_window;
					WindowProperties* wp;
					FramebufferProperties* fp;
				};
				struct TitleCmd {
					const char* text;
					GLFWwindow* handle_window;
				};
				struct ShowCmd {
					GLFWwindow* handle_window;
				};
				struct FullscreenMonitorCmd {
					const MonitorMode* mode;
					WindowProperties* wp;
					FramebufferProperties* fp;
					GLFWmonitor* handle_monitor;
					GLFWwindow* handle_window;
				};
				struct WindowedMonitorCmd {
					unsigned int width;
					unsigned int height;
					unsigned int position_x;
					unsigned int position_y;
					WindowProperties* wp;
					FramebufferProperties* fp;
					GLFWwindow* handle_window;
					GLFWmonitor* handle_monitor;
				};
				struct ClipboardCmd {
					GLFWwindow* handle_window;
					const char* text;
				};
				struct InputCmd {
					GLFWwindow* handle_window;
					InputProperties* ip;
				};

				Type type;
				std::mutex mutex;
				std::condition_variable condition;
				bool finished = false;
				union {
					CreateCmd create;
					DestroyCmd destroy;
					HideCmd hide;
					MinimizeCmd minimize;
					MaximizeCmd maximize;
					RestoreCmd restore;
					FocusCmd focus;
					MoveCmd move;
					ResizeCmd resize;
					TitleCmd title;
					ShowCmd show;
					FullscreenMonitorCmd fullscreenmonitor;
					WindowedMonitorCmd windowedmonitor;
					ClipboardCmd clipboard;
					InputCmd input;
				};
			};
			WICSystemState gstate;


			//-------------------------------------------------------------------------------------------------------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------------------------------------------
			void internalCallbackMonitor(GLFWmonitor* glfw_monitor, int connected) {
				//no synchronization required, lock is already acquired
				//find monitor in the current monitor list
				Monitor* monitor = nullptr;
				for (auto& m : gstate.monitors) if ((GLFWmonitor*)m->handle == glfw_monitor) monitor = m;
				
				//new monitor ?, add to list
				if (monitor == nullptr) {
					gstate.monitors.push_back(new Monitor());
					Monitor* m = gstate.monitors.back();
					//basics
					m->name = glfwGetMonitorName(glfw_monitor);
					m->handle = (void*)glfw_monitor;
					//spatial					
					int glfw_monitor_sizex, glfw_monitor_sizey;
					glfwGetMonitorPhysicalSize(glfw_monitor, &glfw_monitor_sizex, &glfw_monitor_sizey);
					m->physical_sizex = (unsigned int)glfw_monitor_sizex;
					m->physical_sizey = (unsigned int)glfw_monitor_sizey;
					int glfw_monitor_xpos, glfw_monitor_ypos;
					glfwGetMonitorPos(glfw_monitor, &glfw_monitor_xpos, &glfw_monitor_ypos);
					m->position_x = (unsigned int)glfw_monitor_xpos;
					m->position_y = (unsigned int)glfw_monitor_ypos;
					//modes
					const GLFWvidmode* glfw_monitor_mode = glfwGetVideoMode(glfw_monitor);
					m->mode_current.width = glfw_monitor_mode->width;
					m->mode_current.height = glfw_monitor_mode->height;
					m->mode_current.redbits = glfw_monitor_mode->redBits;
					m->mode_current.greenbits = glfw_monitor_mode->greenBits;
					m->mode_current.bluebits = glfw_monitor_mode->blueBits;
					m->mode_current.refreshrate = glfw_monitor_mode->refreshRate;
					int glfw_monitor_modes_count;
					const GLFWvidmode *glfw_monitor_modes = glfwGetVideoModes(glfw_monitor, &glfw_monitor_modes_count);
					m->modes.resize(glfw_monitor_modes_count);
					for (int j = 0; j < glfw_monitor_modes_count; j++) {
						m->modes[j].width = (glfw_monitor_modes + j)->width;
						m->modes[j].height = (glfw_monitor_modes + j)->height;
						m->modes[j].redbits = (glfw_monitor_modes + j)->redBits;
						m->modes[j].greenbits = (glfw_monitor_modes + j)->greenBits;
						m->modes[j].bluebits = (glfw_monitor_modes + j)->blueBits;
						m->modes[j].refreshrate = (glfw_monitor_modes + j)->refreshRate;
					}
					const GLFWgammaramp* glfw_monitor_gammaramp = glfwGetGammaRamp(glfw_monitor);
					m->gamma_ramp.size = glfw_monitor_gammaramp->size;
					m->gamma_ramp.red.resize(m->gamma_ramp.size);
					m->gamma_ramp.green.resize(m->gamma_ramp.size);
					m->gamma_ramp.blue.resize(m->gamma_ramp.size);
					for (unsigned int j = 0; j < m->gamma_ramp.size; j++) {
						m->gamma_ramp.red[j] = *(glfw_monitor_gammaramp->red + j);
						m->gamma_ramp.green[j] = *(glfw_monitor_gammaramp->green + j);
						m->gamma_ramp.blue[j] = *(glfw_monitor_gammaramp->blue + j);
					}
					//point to this new monitor
					monitor = m;
				}

				//apply correct state to  the referenced monitor
				if (connected == GLFW_CONNECTED)	monitor->connected = true;
				else	monitor->connected = false;

				//create event
				Event e;
				if (connected == GLFW_CONNECTED) {
					e.type = Event::Type::MonitorConnect;
					e.MonitorConnect.monitor = monitor;
					if (gstate.logger) (*gstate.logger) << "[WIC] Monitor " << monitor->name << " connected" << std::endl;
				}
				else {
					e.type = Event::Type::MonitorDisconnect;
					e.MonitorDisconnect.monitor = monitor;
					if (gstate.logger) (*gstate.logger) << "[WIC] Monitor " << monitor->name << " disconnected" << std::endl;
				}
				
				//queue it for all windows
				for (auto& window  : gstate.windowstates) {
					window.second.events.push_back(e);
				}
			}
			void internalCallbackJoystick(int joystick, int event) {
				//no synchronization required, lock is already acquired
				Event e;
				if (event == GLFW_CONNECTED) {
					e.type = Event::Type::ControllerConnect;
					e.ControllerConnect.id = joystick;

					//save to global windowless state
					detail::gstate.input_controller[e.ControllerConnect.id].connected = true;

					if (gstate.logger) (*gstate.logger) << "[WIC] Controller " << joystick << " connected" << std::endl;
				}
				else {
					e.type = Event::Type::ControllerDisconnect;
					e.ControllerDisconnect.id = joystick;

					//save to global windowless state
					detail::gstate.input_controller[e.ControllerConnect.id].connected = false;

					if (gstate.logger) (*gstate.logger) << "[WIC] Controller " << joystick << " disconnected" << std::endl;
				}

				//queue it for all windows
				for (auto& window : gstate.windowstates) {
					window.second.events.push_back(e);
				}
			}


			void internalCallbackMouseButton(GLFWwindow* window, int button, int action, int mods) {
				//no sync needed
				//get other data
				double posx, posy;
				glfwGetCursorPos(window, &posx, &posy);
				bool alt, ctrl, shift, system;
				alt = ctrl = shift = system = false;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS) alt = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) ctrl = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) shift = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_SUPER) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SUPER) == GLFW_PRESS) system = true;
				//create event
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if (action == GLFW_PRESS) {
					e.type = Event::Type::MousePress;
					e.MousePress.button = (MouseButton)button;
					e.MousePress.position_x = (unsigned int)std::floor(posx);
					e.MousePress.position_y = (unsigned int)std::floor(posy);
					e.MousePress.alt = alt;
					e.MousePress.control = ctrl;
					e.MousePress.shift = shift;
					e.MousePress.system = system;
				}
				else {
					e.type = Event::Type::MouseRelease;
					e.MouseRelease.button = (MouseButton)button;
					e.MouseRelease.position_x = (unsigned int)std::floor(posx);
					e.MouseRelease.position_y = (unsigned int)std::floor(posy);
					e.MouseRelease.alt = alt;
					e.MouseRelease.control = ctrl;
					e.MouseRelease.shift = shift;
					e.MouseRelease.system = system;
				}
			}
			void internalCallbackMouseScroll(GLFWwindow* window, double xoffset, double yoffset) {
				//no sync needed
				//get other data
				bool alt, ctrl, shift, system;
				alt = ctrl = shift = system = false;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS) alt = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) ctrl = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) shift = true;
				if (glfwGetKey(window, GLFW_KEY_RIGHT_SUPER) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SUPER) == GLFW_PRESS) system = true;
				//create event
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::MouseScroll;
				e.MouseScroll.scroll_x = (float)xoffset;
				e.MouseScroll.scroll_y = (float)yoffset;
				e.MouseScroll.alt = alt;
				e.MouseScroll.control = ctrl;
				e.MouseScroll.shift = shift;
				e.MouseScroll.system = system;
			}
			void internalCallbackCursorPos(GLFWwindow* window, double xpos, double ypos) {
				//no sync needed
				//is any button down?
				int button = -1;
				for (int i = 0; i < (int)MouseButton::MAX; i++) if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1 + i)) button = i;

				//clamp
				int width, height;
				glfwGetWindowSize(window, &width, &height);
				if (xpos < 0) xpos = 0;
				if (xpos > width) xpos = width;
				if (ypos < 0) ypos = 0;
				if (ypos > height) ypos = height;

				//lock cursor?
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				if (!wnd.cursor_enabled) glfwSetCursorPos(window, width / 2.0, height / 2.0);

				//create event
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if (button == -1) {
					//movement
					e.type = Event::Type::MouseMove;
					e.MouseMove.position_x = (unsigned int)xpos;
					e.MouseMove.position_y = (unsigned int)ypos;
				}
				else {
					//drag event
					//add modifiers
					bool alt, ctrl, shift, system;
					alt = ctrl = shift = system = false;
					if (glfwGetKey(window, GLFW_KEY_RIGHT_ALT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS) alt = true;
					if (glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) ctrl = true;
					if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) shift = true;
					if (glfwGetKey(window, GLFW_KEY_RIGHT_SUPER) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_LEFT_SUPER) == GLFW_PRESS) system = true;

					e.type = Event::Type::MouseDrag;
					e.MouseDrag.button = (MouseButton)button;
					e.MouseDrag.position_x = (unsigned int)xpos;
					e.MouseDrag.position_y = (unsigned int)ypos;
					e.MouseDrag.alt = alt;
					e.MouseDrag.control = ctrl;
					e.MouseDrag.shift = shift;
					e.MouseDrag.system = system;
				}
			}
			void internalCallbackCursorEnter(GLFWwindow* window, int entered) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if (entered == GLFW_TRUE) e.type = Event::Type::MouseEnter;
				else e.type = Event::Type::MouseLeave;
			}


			void internalCallbackKey(GLFWwindow* window, int key, int scancode, int action, int mods) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if (action == GLFW_PRESS) {
					e.type = Event::Type::KeyPress;
					e.KeyPress.key = (Key)key;
					e.KeyPress.alt = ((mods & GLFW_MOD_ALT) > 0);
					e.KeyPress.control = ((mods & GLFW_MOD_CONTROL) > 0);
					e.KeyPress.shift = ((mods & GLFW_MOD_SHIFT) > 0);
					e.KeyPress.system = ((mods & GLFW_MOD_SUPER) > 0);
				}
				if (action == GLFW_RELEASE) {
					e.type = Event::Type::KeyRelease;
					e.KeyRelease.key = (Key)key;
					e.KeyRelease.alt = ((mods & GLFW_MOD_ALT) > 0);
					e.KeyRelease.control = ((mods & GLFW_MOD_CONTROL) > 0);
					e.KeyRelease.shift = ((mods & GLFW_MOD_SHIFT) > 0);
					e.KeyRelease.system = ((mods & GLFW_MOD_SUPER) > 0);
				}
				if (action == GLFW_REPEAT) {
					e.type = Event::Type::KeyRepeat;
					e.KeyRepeat.key = (Key)key;
					e.KeyRepeat.alt = ((mods & GLFW_MOD_ALT) > 0);
					e.KeyRepeat.control = ((mods & GLFW_MOD_CONTROL) > 0);
					e.KeyRepeat.shift = ((mods & GLFW_MOD_SHIFT) > 0);
					e.KeyRepeat.system = ((mods & GLFW_MOD_SUPER) > 0);
				}
			}
			void internalCallbackChar(GLFWwindow* window, unsigned int codepoint) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::UnicodeChar;
				e.UnicodeChar.codepoint = codepoint;
			}
			void internalCallbackCharMods(GLFWwindow* window, unsigned int codepoint, int mods) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::UnicodeCharMods;
				e.UnicodeCharMods.codepoint = codepoint;
				e.UnicodeCharMods.alt = ((mods & GLFW_MOD_ALT) > 0);
				e.UnicodeCharMods.control = ((mods & GLFW_MOD_CONTROL) > 0);
				e.UnicodeCharMods.shift = ((mods & GLFW_MOD_SHIFT) > 0);
				e.UnicodeCharMods.system = ((mods & GLFW_MOD_SUPER) > 0);
			}


			void internalCallbackFramebufferSize(GLFWwindow* window, int width, int height) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::FramebufferResize;
				e.FramebufferResize.width = width;
				e.FramebufferResize.height = height;
			}
			void internalCallbackWindowSize(GLFWwindow* window, int width, int height) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::WindowResize;
				e.WindowResize.width = (unsigned int)width;
				e.WindowResize.height = (unsigned int)height;

				//GLFW maximization lacks a callback so one is hacked here
				bool maximized = ( glfwGetWindowAttrib(window, GLFW_MAXIMIZED) > 0);
				if (maximized && !wnd.maximized) {
					wnd.events.emplace_back();
					Event& eb = wnd.events.back();
					eb.type = Event::Type::WindowMaximize;
				}
			}
			void internalCallbackWindowPos(GLFWwindow* window, int xpos, int ypos) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::WindowMove;
				//protect against minimize bug
				if (xpos < 0) xpos = 0;
				if (ypos < 0) ypos = 0;
				e.WindowMove.position_x = (unsigned int)xpos;
				e.WindowMove.position_y = (unsigned int)ypos;
			}
			void internalCallbackWindowClose(GLFWwindow* window) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::WindowClose;
			}
			void internalCallbackWindowRefresh(GLFWwindow* window) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::WindowRefresh;
			}
			void internalCallbackWindowFocus(GLFWwindow* window, int focused) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if(focused == GLFW_TRUE) e.type = Event::Type::WindowFocusGain;
				else e.type = Event::Type::WindowFocusLose;
			}
			void internalCallbackWindowIconify(GLFWwindow* window, int iconified) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				if (iconified == GLFW_TRUE) e.type = Event::Type::WindowMinimize;
				else e.type = Event::Type::WindowRestore;
			}


			void internalCallbackFileDrop(GLFWwindow* window, int count, const char** paths) {
				//no synchronization required, lock is already acquired
				WICSystemState::WindowState& wnd = gstate.windowstates[window];
				wnd.events.emplace_back();
				Event& e = wnd.events.back();
				e.type = Event::Type::FilesDrop;
				e.FilesDrop.paths = new std::vector<std::string>();
				for (int i = 0; i < count; i++) e.FilesDrop.paths->emplace_back(paths[i]);

				//log
				if (gstate.logger) {
					(*gstate.logger) << "[WIC] Files dropped (" << count << ") connected" << std::endl;
					for (int i = 0; i < count; i++) (*gstate.logger) << "      " << paths[i] << std::endl;
				}
			}


			void internalCallbackError(int error, const char* description) {
				if (gstate.logger) {
					(*gstate.logger) << " [WIC] Internal error : ";
					switch (error) {
					case GLFW_NOT_INITIALIZED:
						(*gstate.logger) << "not initialized " << description << std::endl;
						break;
					case GLFW_NO_CURRENT_CONTEXT:
						(*gstate.logger) << "no current context " << description << std::endl;
						break;
					case GLFW_INVALID_ENUM:
						(*gstate.logger) << "invalid enum " << description << std::endl;
						break;
					case GLFW_INVALID_VALUE:
						(*gstate.logger) << "invalid value " << description << std::endl;
						break;
					case GLFW_OUT_OF_MEMORY:
						(*gstate.logger) << "out of memory " << description << std::endl;
						break;
					case GLFW_API_UNAVAILABLE:
						(*gstate.logger) << "api unavailable " << description << std::endl;
						break;
					case GLFW_VERSION_UNAVAILABLE:
						(*gstate.logger) << "version unavailable " << description << std::endl;
						break;
					case GLFW_PLATFORM_ERROR:
						(*gstate.logger) << "platform error " << description << std::endl;
						break;
					case GLFW_FORMAT_UNAVAILABLE:
						(*gstate.logger) << "format unavailable " << description << std::endl;
						break;
					case GLFW_NO_WINDOW_CONTEXT:
						(*gstate.logger) << "no window conext " << description << std::endl;
						break;
					}
				}
			}

			//internal functions
			void internalHandleCommand(detail::WindowBlockingCommand* command) {
				switch (command->type) {
				case detail::WindowBlockingCommand::Type::CLIPBOARD:
				{
					glfwSetClipboardString(command->clipboard.handle_window, command->clipboard.text);
					break;
				}
				case detail::WindowBlockingCommand::Type::CREATE:
				{
					//parse arguments
					WindowProperties* wp = command->create.wp;
					ContextProperties* cp = command->create.cp;
					InputProperties* ip = command->create.ip;
					FramebufferProperties *fp = command->create.fp;

					//apply constraints
					auto constraint = std::bind([&](bool condition, const char* message) {
						if (condition && gstate.logger) (*gstate.logger) << "[WIC] Warning : " << message << std::endl;
						return condition;
					}, std::placeholders::_1, std::placeholders::_2);
					if (constraint((wp->resizable && wp->fullscreen), "(window) a fullscreen window can't be resizable")) wp->resizable = false;
					if (constraint((wp->visible && wp->fullscreen), "(window) a fullscreen window can't be invisible")) wp->visible = true;
					if (constraint((wp->decorated && wp->fullscreen), "(window) a fullscreen window can't be decorated")) wp->decorated = false;
					if (constraint((wp->focused && wp->fullscreen), "(window) a fullscreen window can't be unfocused")) wp->focused = true;
					if (constraint((wp->minimized), "(window) can't start a window minimized")) wp->minimized = false;
					if (constraint((wp->auto_restore_mode_on_minimize && !wp->fullscreen), "(window) a windowed window does not suport auto video mode restore on minimization")) wp->auto_restore_mode_on_minimize = false;
					if (constraint((wp->floating && wp->fullscreen), "(window) a fullscreen window can't be floating")) wp->floating = false;

					//apply hints
					glfwWindowHint(GLFW_RESIZABLE, wp->resizable ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_VISIBLE, wp->visible ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_DECORATED, wp->decorated ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_FOCUSED, wp->focused ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_AUTO_ICONIFY, wp->auto_restore_mode_on_minimize ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_FLOATING, wp->floating ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_MAXIMIZED, wp->maximized ? GLFW_TRUE : GLFW_FALSE);
					glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);
					glfwWindowHint(GLFW_RED_BITS, fp->red_bits);
					glfwWindowHint(GLFW_GREEN_BITS, fp->green_bits);
					glfwWindowHint(GLFW_BLUE_BITS, fp->blue_bits);
					glfwWindowHint(GLFW_ALPHA_BITS, fp->alpha_bits);

					//context creation hints
					if (cp->api == ContextProperties::ApiType::OPENGL) {
						//opengl constraint
						if (constraint(((cp->version_major == 3 && cp->version_minor <= 2) || (cp->version_major < 3)) && cp->profile_core, "(context) there is no core OpenGL profile before 3.2")) cp->profile_core = false;

						//opengl framebuffer hints
						glfwWindowHint(GLFW_DEPTH_BITS, fp->depth_bits);
						glfwWindowHint(GLFW_STENCIL_BITS, fp->stencil_bits);
						glfwWindowHint(GLFW_ACCUM_RED_BITS, fp->accum_red_bits);
						glfwWindowHint(GLFW_ACCUM_GREEN_BITS, fp->accum_green_bits);
						glfwWindowHint(GLFW_ACCUM_BLUE_BITS, fp->accum_blue_bits);
						glfwWindowHint(GLFW_ACCUM_ALPHA_BITS, fp->accum_alpha_bits);
						glfwWindowHint(GLFW_AUX_BUFFERS, fp->aux_buffers);
						glfwWindowHint(GLFW_SAMPLES, fp->samples_per_pixel);
						glfwWindowHint(GLFW_SRGB_CAPABLE, fp->srgb);
						glfwWindowHint(GLFW_STEREO, fp->stereo);
						glfwWindowHint(GLFW_DOUBLEBUFFER, fp->doublebuffer);

						//opengl context hints
						glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
						glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, cp->version_major);
						glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, cp->version_minor);
						if (cp->robustness == ContextProperties::RobustnessType::NONE) glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_NO_ROBUSTNESS);
						if (cp->robustness == ContextProperties::RobustnessType::NO_RESET_NOTIFICATION) glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_NO_RESET_NOTIFICATION);
						if (cp->robustness == ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET) glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_LOSE_CONTEXT_ON_RESET);
						glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, cp->release_behavior_flush ? GLFW_RELEASE_BEHAVIOR_FLUSH : GLFW_RELEASE_BEHAVIOR_NONE);
						//no forward compatibility hints, do this compatibility profile
						glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, cp->debug_context ? GLFW_TRUE : GLFW_FALSE);
						//there is no compatibility or core profile before OpenGL 3.2.
#if defined(__APPLE__)
						//APPLE PATH
						// http://www.glfw.org/faq.html#how-do-i-create-an-opengl-30-context
						if (cp->version_major < 3) {
							glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
						}
						else {
							if (constraint(!cp->profile_core, "(APPLE) OpenGL 3.0+ contexts are only possible on core profiles")) cp->profile_core = true;
							glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
							glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
						}
#else
						//non APPLE PATH
						if ((cp->version_major == 3 && cp->version_minor < 2) || (cp->version_major < 3)) glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
						else {
							// see https://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts
							if (cp->profile_core) glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
							else glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
#endif
						}
					}
					else {
						//no api
						if (constraint(command->create.handle_shared_window!=nullptr, "(shared window) ignoring shared contexts when not working with OpenGL")) command->create.handle_shared_window = nullptr;
						if (constraint(fp->depth_bits > 0, "(framebuffer) ignoring depth bits when not working with OpenGL")) fp->depth_bits = 0;
						if (constraint(fp->stencil_bits > 0, "(framebuffer) ignoring stencil bits when not working with OpenGL")) fp->stencil_bits = 0;
						if (constraint(fp->accum_red_bits > 0, "(framebuffer) ignoring accumulation buffer red bits when not working with OpenGL")) fp->accum_red_bits = 0;
						if (constraint(fp->accum_green_bits > 0, "(framebuffer) ignoring accumulation buffer green bits when not working with OpenGL")) fp->accum_green_bits = 0;
						if (constraint(fp->accum_blue_bits > 0, "(framebuffer) ignoring accumulation buffer blue bits when not working with OpenGL")) fp->accum_blue_bits = 0;
						if (constraint(fp->accum_alpha_bits > 0, "(framebuffer) ignoring accumulation buffer alpha bits when not working with OpenGL")) fp->accum_alpha_bits = 0;
						if (constraint(fp->aux_buffers > 0, "(framebuffer) ignoring auxiliary buffers when not working with OpenGL")) fp->aux_buffers = 0;
						if (constraint(fp->samples_per_pixel > 1, "(framebuffer) ignoring multisamples when not working with OpenGL")) fp->samples_per_pixel = 1;
						if (constraint(fp->srgb, "(framebuffer) ignoring SRGB support when not working with OpenGL")) fp->srgb = false;
						if (constraint(fp->stereo, "(framebuffer) ignoring stereo support when not working with OpenGL")) fp->stereo = false;
						if (constraint(fp->doublebuffer, "(framebuffer) ignoring dublebuffering when not working with OpenGL")) fp->doublebuffer = false;
						if (constraint(cp->version_major > 0 || cp->version_minor > 0, "(context) ignoring requested OpenGL version when not working with OpenGL")) cp->version_major = cp->version_minor = 0;
						if (constraint(cp->robustness != ContextProperties::RobustnessType::NONE, "(context) ignoring robustness when not working with OpenGL")) cp->robustness = ContextProperties::RobustnessType::NONE;
						if (constraint(cp->release_behavior_flush, "(context) ignoring release behavior when not working with OpenGL")) cp->release_behavior_flush = false;
						if (constraint(cp->debug_context, "(context) ignoring debug context when not working with OpenGL")) cp->debug_context = false;
						if (constraint(cp->profile_core, "(context) ignoring profile when not working with OpenGL")) cp->profile_core = false;
						if (constraint(cp->swap_interval != 0, "(context) ignoring swap interval when not working with OpenGL")) cp->swap_interval = 0;

						//create window with no api
						glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
					}

					if (!command->create.monitor) {
						//if no monitor is specified use the primary
						if (detail::gstate.monitors.size() == 0) {
							if (detail::gstate.logger) (*detail::gstate.logger) << "[WIC] ERROR : no monitors have been found by GLFW, terminating. "<< std::endl;
							std::terminate();
						}
						command->create.monitor = detail::gstate.monitors[0];
						command->create.handle_monitor = (GLFWmonitor*)detail::gstate.monitors[0]->handle;
					}

					//create window
					GLFWwindow* handle_window;
					if(wp->fullscreen) handle_window = glfwCreateWindow(wp->width, wp->height, wp->title.c_str(), command->create.handle_monitor, command->create.handle_shared_window);
					else handle_window = glfwCreateWindow(wp->width, wp->height, wp->title.c_str(), nullptr, command->create.handle_shared_window);
					command->create.handle_window = handle_window;
				
					//check for errors and terminate if bad arguments are given
					if (!handle_window) {
						if (gstate.logger) (*gstate.logger) << "[WIC] ERROR : could not open with window with the requested properties. Terminating ! " << std::endl;
						std::terminate();
					}

					//position window
					glfwSetWindowPos(handle_window, wp->position_x, wp->position_y);

					//attach icon
					GLFWimage glfw_image;
					glfw_image.width = glfw_image.height = 32;
					glfw_image.pixels = wp->icon_pixels;
					if(wp->icon_pixels) glfwSetWindowIcon(handle_window, 1, &glfw_image);

					//input properties
					glfwSetCursor(handle_window, glfwCreateStandardCursor((int)ip->cursor_type));
					if (ip->cursor_enabled) {
						if (ip->cursor_visible) glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
						else glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
					}else glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
					if (ip->sticky_keys) glfwSetInputMode(handle_window, GLFW_STICKY_KEYS, GLFW_TRUE);
					else glfwSetInputMode(handle_window, GLFW_STICKY_KEYS, GLFW_FALSE);
					if (ip->sticky_mouse) glfwSetInputMode(handle_window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);
					else glfwSetInputMode(handle_window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_FALSE);
					
					//attach callbacks
					glfwSetCharCallback(handle_window, internalCallbackChar);
					glfwSetCharModsCallback(handle_window, internalCallbackCharMods);
					glfwSetCursorEnterCallback(handle_window, internalCallbackCursorEnter);
					glfwSetCursorPosCallback(handle_window, internalCallbackCursorPos);
					glfwSetDropCallback(handle_window, internalCallbackFileDrop);
					glfwSetKeyCallback(handle_window, internalCallbackKey);
					glfwSetMouseButtonCallback(handle_window, internalCallbackMouseButton);
					glfwSetScrollCallback(handle_window, internalCallbackMouseScroll);
					glfwSetFramebufferSizeCallback(handle_window, internalCallbackFramebufferSize);
					glfwSetWindowCloseCallback(handle_window, internalCallbackWindowClose);
					glfwSetWindowFocusCallback(handle_window, internalCallbackWindowFocus);
					glfwSetWindowIconifyCallback(handle_window, internalCallbackWindowIconify);
					glfwSetWindowPosCallback(handle_window, internalCallbackWindowPos);
					glfwSetWindowSizeCallback(handle_window, internalCallbackWindowSize);
					glfwSetWindowRefreshCallback(handle_window, internalCallbackWindowRefresh);

					//update global state
					detail::WICSystemState::WindowState windowstate;
					windowstate.cursor_enabled = ip->cursor_enabled;
					windowstate.maximized = wp->maximized;
					windowstate.events.reserve(EVENT_BUFFER_START_SIZE);
					windowstate.title = wp->title;
					gstate.windowstates[(void*)handle_window] = windowstate;

					//update information framebuffer size(decoration related)
					int w, h;
					glfwGetFramebufferSize(handle_window, &w, &h);
					fp->width = w; 
					fp->height = h;
					
					//update refresh rate for 
					GLFWmonitor* handle_monitor = glfwGetWindowMonitor(handle_window);
					if (handle_monitor) wp->refresh_rate = glfwGetVideoMode(handle_monitor)->refreshRate;	 //fullscreen
					else wp->refresh_rate = glfwGetVideoMode(glfwGetPrimaryMonitor())->refreshRate;	//windowed


					//OpenGL related
					if (cp->api == ContextProperties::ApiType::OPENGL) {
						//make context current and set swap interval
						glfwMakeContextCurrent(handle_window);
						glfwSwapInterval(cp->swap_interval);

						//load extensions for this context with a modified glad
#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS
						int result = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress, cp->profile_core);
						if(result == 0) assert(false);

						//query OpenGL driver for various specs
						cp->driver_renderer = (const char*)glGetString(GL_RENDERER);
						cp->driver_vendor = (const char*)glGetString(GL_VENDOR);
						cp->driver_version = (const char*)glGetString(GL_VERSION);
						cp->driver_glsl_version = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

						//save extensions on the created context (owned by window, who owns the context)
						GLFunctionPointers* glfp = reinterpret_cast<GLFunctionPointers*>(cp->detail);
						glfp->saveCurrent();
#endif

						//disconnect the context from the main thread (THIS IS ABSOLUTELY CRITICAL)
						glfwMakeContextCurrent(nullptr);
					}
					
					//log
					if (gstate.logger) {
						(*gstate.logger) << "[WIC] opened new window, named [" << wp->title << "] on monitor ["<< command->create.monitor->name <<"]"<< std::endl;
						(*gstate.logger) << "      Window properties: " << std::endl;
						(*gstate.logger) << "       resolution = " << wp->width << " x " << wp->height << " @" << wp->refresh_rate << "Hz, position = (" << wp->position_x << "," << wp->position_y <<")" << std::endl;
						(*gstate.logger) << std::boolalpha << "       resizeable = " << wp->resizable << ", visible = " << wp->visible << ", decorated = " << wp->decorated<< ", focused = " << wp->focused << ",  fullscreen = " << wp->fullscreen << std::endl;
						(*gstate.logger) << "       minimized = " << wp->minimized << ", maximized = " << wp->maximized << ", autorestore = " << wp->auto_restore_mode_on_minimize <<", floating = " << wp->floating << std::endl;
						
						(*gstate.logger) << "      Framebuffer properties:" << std::endl;
						(*gstate.logger) << "       mode = " << fp->width << "x" << fp->height << " "<<fp->samples_per_pixel<<"spp R" << fp->red_bits << " G" << fp->green_bits << " B" << fp->blue_bits << " A" << fp->alpha_bits;
						(*gstate.logger) << " D" << fp->depth_bits << " S" << fp->stencil_bits << " and "<<fp->aux_buffers<<" auxiliary buffers with R" << fp->accum_red_bits << " G" << fp->accum_green_bits << " B" << fp->accum_blue_bits << " A" << fp->accum_alpha_bits << std::endl;
						(*gstate.logger) << "       stereo = " << fp->stereo << ", doublebuffered = " << fp->doublebuffer << ", srgb = " << fp->srgb << std::endl;

						(*gstate.logger) << "      Context properties:" << std::endl;
						(*gstate.logger) << "       thread = " << cp->thread_binded<<", ";
						if (cp->api == ContextProperties::ApiType::NONE) {
							(*gstate.logger) << "API = NONE,";
						}
						else {
							(*gstate.logger) << "API = OPENGL, context version="<<cp->version_major<<"."<<cp->version_minor<<", debug = "<<cp->debug_context<<", core = "<<cp->profile_core<<", flush_on_release = "<<cp->release_behavior_flush<<", robustness behavior= ";
							if (cp->robustness == ContextProperties::RobustnessType::NONE) (*gstate.logger) << " none";
							if (cp->robustness == ContextProperties::RobustnessType::NO_RESET_NOTIFICATION) (*gstate.logger) << " no reset notification";
							if (cp->robustness == ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET) (*gstate.logger) << " lose context on reset";
							(*gstate.logger) << ", swap interval = " << cp->swap_interval << std::endl;
							(*gstate.logger) << "       Driver: version = " << cp->driver_version << " glsl version = "<< cp->driver_glsl_version <<" vendor = " << cp->driver_vendor << " renderer = " << cp->driver_renderer << std::endl;
						}
						
						(*gstate.logger) << "      Input properties:" << std::endl;
						(*gstate.logger) << "       sticky keys = " << ip->sticky_keys << ", sticky mouse = " << ip->sticky_mouse << ", cursor visible = " << ip->cursor_visible << ", cursor enabled = " << ip->cursor_enabled<<",";
						if (InputProperties::CursorType::ARROW == ip->cursor_type) (*gstate.logger) << " cursor = ARROW" << std::endl;
						if (InputProperties::CursorType::IBEAM == ip->cursor_type) (*gstate.logger) << " cursor = IBEAM" << std::endl;
						if (InputProperties::CursorType::CROSSHAIR == ip->cursor_type) (*gstate.logger) << " cursor = CROSSHAIR" << std::endl;
						if (InputProperties::CursorType::HAND == ip->cursor_type) (*gstate.logger) << " cursor = HAND" << std::endl;
						if (InputProperties::CursorType::HRESIZE == ip->cursor_type) (*gstate.logger) << " cursor = HRESIZE" << std::endl;
						if (InputProperties::CursorType::VRESIZE == ip->cursor_type) (*gstate.logger) << " cursor = VRESIZE" << std::endl;
					}

					break;
				}
				case detail::WindowBlockingCommand::Type::DESTROY:
				{
					// guaranteed that this succeeds the destructor of this window
					// other events can't appear to lead to bad map lookups because main is the
					// ONLY thread handling input&windows, thus this is executed synchronously. 
					std::string title = gstate.windowstates[command->destroy.handle_window].title;
					gstate.windowstates.erase(command->destroy.handle_window);
					// destroy with glfw
					glfwDestroyWindow(command->destroy.handle_window);
					//log
					if (gstate.logger) (*gstate.logger) << "[WIC] destroyed window [" << title << "]" << std::endl;
					break;
				}
				case detail::WindowBlockingCommand::Type::HIDE:
				{
					glfwHideWindow(command->hide.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::MINIMIZE:
				{
					glfwIconifyWindow(command->hide.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::MAXIMIZE:
				{
					glfwMaximizeWindow(command->maximize.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::FOCUS:
				{
					glfwFocusWindow(command->maximize.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::MOVE:
				{
					glfwSetWindowPos(command->move.handle_window, command->move.position_x, command->move.position_y);
					break;
				}
				case detail::WindowBlockingCommand::Type::RESIZE:
				{
					glfwSetWindowSize(command->resize.handle_window, command->resize.wp->width, command->resize.wp->height);
					//query framebuffer (some platforms have differences due to decorations/borders/etc)
					int w, h;
					glfwGetFramebufferSize(command->resize.handle_window, &w, &h);
					command->resize.fp->width = w;
					command->resize.fp->height = h;
					break;
				}
				case detail::WindowBlockingCommand::Type::RESTORE:
				{
					glfwRestoreWindow(command->restore.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::SHOW:
				{
					glfwShowWindow(command->show.handle_window);
					break;
				}
				case detail::WindowBlockingCommand::Type::TITLE:
				{
					auto& state = gstate.windowstates[(void*)command->title.handle_window];
					glfwSetWindowTitle(command->title.handle_window, command->title.text);
					//log
					if (gstate.logger) (*gstate.logger) << "[WIC] renamed window [" << state.title << "] to [" << command->title.text << "]" << std::endl;
					//state
					state.title = command->title.text;
					break;
				}
				case detail::WindowBlockingCommand::Type::INPUT:
				{
					//input properties
					GLFWwindow* handle_window = command->input.handle_window;
					InputProperties* ip = command->input.ip;
					gstate.windowstates[handle_window].cursor_enabled = ip->cursor_enabled;
					//apply
					glfwSetCursor(handle_window, glfwCreateStandardCursor((int)ip->cursor_type));
					if (ip->cursor_enabled) {
						if (ip->cursor_visible) glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
						else glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
					}
					else glfwSetInputMode(handle_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
					if (ip->sticky_keys) glfwSetInputMode(handle_window, GLFW_STICKY_KEYS, GLFW_TRUE);
					else glfwSetInputMode(handle_window, GLFW_STICKY_KEYS, GLFW_FALSE);
					if (ip->sticky_mouse) glfwSetInputMode(handle_window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);
					else glfwSetInputMode(handle_window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_FALSE);
					break;
				}
				case detail::WindowBlockingCommand::Type::FULLSCREEN_MONITOR:
				{
					GLFWwindow* handle_window = command->fullscreenmonitor.handle_window;
					GLFWmonitor* handle_monitor = command->fullscreenmonitor.handle_monitor;
					unsigned int w = command->fullscreenmonitor.mode->width;
					unsigned int h = command->fullscreenmonitor.mode->height;
					unsigned int rr = command->fullscreenmonitor.mode->refreshrate;
					glfwSetWindowMonitor(handle_window, handle_monitor, 0, 0, w, h, rr);
					int x, y;
					glfwGetFramebufferSize(handle_window, &x, &y);
					command->fullscreenmonitor.wp->width = x;
					command->fullscreenmonitor.wp->height = y;
					command->fullscreenmonitor.wp->refresh_rate = glfwGetVideoMode(handle_monitor)->refreshRate;
					break;

				}
				case detail::WindowBlockingCommand::Type::WINDOWED_MONITOR:
				{
					GLFWwindow* handle_window = command->windowedmonitor.handle_window;
					unsigned int px = command->windowedmonitor.position_x;
					unsigned int py = command->windowedmonitor.position_y;
					unsigned int w = command->windowedmonitor.width;
					unsigned int h = command->windowedmonitor.height;
					unsigned int rr = glfwGetVideoMode(command->windowedmonitor.handle_monitor)->refreshRate;
					glfwSetWindowMonitor(handle_window, nullptr, px, py, w, h, rr);
					int x, y;
					glfwGetFramebufferSize(handle_window, &x, &y);
					command->windowedmonitor.wp->width = x;
					command->windowedmonitor.wp->height = y;
					break;
				}
				}

				//notify the waiting thread that the command was executed
				command->notify();
			}



			void internalCommand(detail::WindowBlockingCommand* command) {
				if (detail::gstate.multithreaded) {
					//multithreaded path
					if (std::this_thread::get_id() == detail::gstate.mainthread) {
						//optimization, run directly on the main thread -> execute, don't store command (relevant for singlethreaded usage)
						detail::gstate.mutex.lock();
						internalHandleCommand(command);
						detail::gstate.mutex.unlock();
					}
					else {
						//else enqueue blocking comand and wait for it to finish
						detail::gstate.mutex.lock();
						detail::gstate.commands.push_back(command);
						detail::gstate.mutex.unlock();
						//wait for it to finish after releasing the lock
						command->wait();
					}
				}
				else {
					//singlethreaded path
					internalHandleCommand(command);
				}
			}


			//-------------------------------------------------------------------------------------------------------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------------------------------------------
			WICSystemState::WICSystemState() {
				//statics can only be instanced by the main thread
				mainthread = std::this_thread::get_id();
				//not initialized
				initialized = false;
			}
			WICSystemState::~WICSystemState() {
				initialized = false;
			}
			void WICSystemState::init(std::ostream* in_logger, bool in_multithreaded){
				//reality check
				if (initialized) {
					if(*in_logger) (*in_logger) << "[WIC] ERROR: Creating multiple WICSystem objects.\n";
					std::cout << "[WIC] ERROR: Creating multiple WICSystem objects.\n";
					std::terminate();
				}

				//save data
				logger = in_logger;
				multithreaded = in_multithreaded;
				
				//register failure callback
				glfwSetErrorCallback(internalCallbackError);

				//initialize glfw and assert if failure
				if (glfwInit() != GLFW_TRUE) {
					if (*logger) (*logger) << "[WIC] ERROR: Unexpected failure in initialization of GLFW dependency. Terminating.\n";
					std::terminate();
				}

				//get all monitors (primary is the first)
				int glfw_monitor_count;
				GLFWmonitor** glfw_monitors = glfwGetMonitors(&glfw_monitor_count);
				for (int i = 0; i < glfw_monitor_count; i++) {
					GLFWmonitor* glfw_monitor = *(glfw_monitors + i);
					Monitor* monitor = new Monitor();
					monitors.push_back(monitor);

					//basics
					monitor->name = glfwGetMonitorName(glfw_monitor);
					monitor->connected = true;
					monitor->handle = (void*)glfw_monitor;
					//spatial					
					int glfw_monitor_sizex, glfw_monitor_sizey;
					glfwGetMonitorPhysicalSize(*(glfw_monitors + i), &glfw_monitor_sizex, &glfw_monitor_sizey);
					monitor->physical_sizex = (unsigned int)glfw_monitor_sizex;
					monitor->physical_sizey = (unsigned int)glfw_monitor_sizey;
					int glfw_monitor_xpos, glfw_monitor_ypos;
					glfwGetMonitorPos(glfw_monitor, &glfw_monitor_xpos, &glfw_monitor_ypos);
					monitor->position_x = (unsigned int)glfw_monitor_xpos;
					monitor->position_y = (unsigned int)glfw_monitor_ypos;
					//modes
					const GLFWvidmode* glfw_monitor_mode = glfwGetVideoMode(glfw_monitor);
					monitor->mode_current.width = glfw_monitor_mode->width;
					monitor->mode_current.height = glfw_monitor_mode->height;
					monitor->mode_current.redbits = glfw_monitor_mode->redBits;
					monitor->mode_current.greenbits = glfw_monitor_mode->greenBits;
					monitor->mode_current.bluebits = glfw_monitor_mode->blueBits;
					monitor->mode_current.refreshrate = glfw_monitor_mode->refreshRate;
					int glfw_monitor_modes_count;
					const GLFWvidmode *glfw_monitor_modes = glfwGetVideoModes(glfw_monitor, &glfw_monitor_modes_count);
					monitor->modes.resize(glfw_monitor_modes_count);
					for (int j = 0; j < glfw_monitor_modes_count; j++) {
						monitor->modes[j].width = (glfw_monitor_modes + j)->width;
						monitor->modes[j].height = (glfw_monitor_modes + j)->height;
						monitor->modes[j].redbits = (glfw_monitor_modes + j)->redBits;
						monitor->modes[j].greenbits = (glfw_monitor_modes + j)->greenBits;
						monitor->modes[j].bluebits = (glfw_monitor_modes + j)->blueBits;
						monitor->modes[j].refreshrate = (glfw_monitor_modes + j)->refreshRate;
					}
					const GLFWgammaramp* glfw_monitor_gammaramp = glfwGetGammaRamp(glfw_monitor);
					monitor->gamma_ramp.size = glfw_monitor_gammaramp->size;
					monitor->gamma_ramp.red.resize(monitor->gamma_ramp.size);
					monitor->gamma_ramp.green.resize(monitor->gamma_ramp.size);
					monitor->gamma_ramp.blue.resize(monitor->gamma_ramp.size);
					for (unsigned int j = 0; j < monitor->gamma_ramp.size; j++) {
						monitor->gamma_ramp.red[j] = *(glfw_monitor_gammaramp->red + j);
						monitor->gamma_ramp.green[j] = *(glfw_monitor_gammaramp->green + j);
						monitor->gamma_ramp.blue[j] = *(glfw_monitor_gammaramp->blue + j);
					}
				}

				//register global callbacks (not related to monitor)
				glfwSetJoystickCallback(internalCallbackJoystick);
				glfwSetMonitorCallback(internalCallbackMonitor);

				//program begin timestamp
				timestamp_program = std::chrono::high_resolution_clock::now();

				//finished initialization
				initialized = true;

				//log
				if (gstate.logger) {
					(*gstate.logger) << "[WIC] initialized system : "<< std::endl;
					(*gstate.logger) << std::boolalpha << "      mainthread = " << mainthread << ", multithreaded = " << multithreaded << ", with " << monitors.size() << " monitors:" << std::endl;
					for (auto& m : monitors) {
						(*gstate.logger) << "      monitor [" << m->name << "] with physical size " << m->physical_sizex << " x " << m->physical_sizey << " at (" << m->position_x << "," << m->position_y << ") virtual monitor position";
						auto& mm = m->mode_current;
						(*gstate.logger) << " running in "<<mm.width<<" x "<<mm.height<<" @"<<mm.refreshrate<<"Hz R"<<mm.redbits<<" G"<<mm.greenbits<<" B"<<mm.bluebits<<" mode" << std::endl;
					}
				}

			}
			void WICSystemState::terminate() {
				//reality check
				if (!initialized) {
					if (*logger) (*logger) << "[WIC] ERROR: internal error, destroying WICSystem multiple times.\n";
					std::cout << "[WIC] ERROR: internal error, destroying WICSystem multiple times.\n";
					std::terminate();
				}

				//if in multithreaded mode signal all the threads outside main thread and WAIT for them
				if (multithreaded) {
					// not using conditional variables as it is not known if the remaining windows
					// were created from this thread or from another thread (which would lead to all sorts of deadlocks)
					mutex.lock();
					//solve the remaining of the window commands (some of them might be destroy commands)
					for (detail::WindowBlockingCommand* cmd : commands) internalHandleCommand(cmd);
					//add a closing event for all existing windows
					for (auto& e : windowstates) {
						e.second.events.emplace_back();
						e.second.events.back().type = Event::Type::WindowClose;
					}
					mutex.unlock();

					//busy waiting (at system termination, definately not performance critical)
					bool all_closed = false;
					while (!all_closed) {
						std::this_thread::yield();
						mutex.lock();
						//process events until all windows have responded (when a window is destroyed it is erased from the windowstates map)
						for (detail::WindowBlockingCommand* cmd : commands) internalHandleCommand(cmd);
						all_closed = (windowstates.size() == 0);
						mutex.unlock();
					}
				}

				//guaranteed that all windows have been destroyed. Clean up glfw and exit.
				glfwTerminate();

				//state cleanup
				windowstates.clear();
				for (auto& m : monitors) delete m; 
				monitors.clear();
				for (auto& c : commands) delete c; //this should always be empty
				commands.clear();

				//finished destruction
				initialized = false;

				//log
				if (gstate.logger) (*gstate.logger) << "[WIC] destroyed system. " << std::endl;
			}
		}

		


		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		WICSystem::WICSystem(std::ostream* logger, bool multithreaded) {
			detail::gstate.init(logger, multithreaded);

			//initialize windowless input state
			for (int cid = 0; cid < (int)Controller::MAX; cid++) {
				detail::gstate.input_controller[cid].connected = false;
				for (int i = 0; i < (int)JOYSTICK_MAX_AXES; i++) detail::gstate.input_controller[cid].axes[i] = 0.0f;
				for (int i = 0; i < (int)ControllerButton::MAX; i++) detail::gstate.input_controller[cid].buttons[i] = false;
			}
		}
		WICSystem::~WICSystem() {
			detail::gstate.terminate();
		}
		bool WICSystem::isAnyWindowOpened()
		{
			return detail::gstate.windowstates.size() > 0;
		}
		void WICSystem::setLogger(std::ostream* l)
		{
			if (detail::gstate.multithreaded) detail::gstate.mutex.lock();
			detail::gstate.logger = l;
			if (detail::gstate.multithreaded) detail::gstate.mutex.unlock();
		}
		const Monitor* WICSystem::getMonitorPrimary()
		{
			if (detail::gstate.monitors.size() == 0) return nullptr;
			return detail::gstate.monitors[0];
		}
		const std::vector<Monitor*>& WICSystem::getMonitors()
		{
			return detail::gstate.monitors;
		}
		unsigned int WICSystem::getWindowsOpened() {
			if (detail::gstate.multithreaded) {
				std::lock_guard<std::mutex> lock(detail::gstate.mutex);
				return (unsigned int)detail::gstate.windowstates.size();
			}
			return (unsigned int)detail::gstate.windowstates.size();
		}

		void WICSystem::processProgramEvents()
		{
			if (detail::gstate.multithreaded) detail::gstate.mutex.lock();

			//enforce running on main thread
			if (std::this_thread::get_id() != detail::gstate.mainthread) {
				if (detail::gstate.logger) (*detail::gstate.logger) << "[WICSystem] ERROR: WICSystem::processProgramEvents can ONLY be used from the main thread. Terminating.\n";
				std::terminate();
			}

			//solve window commands received from windows
			for (detail::WindowBlockingCommand* cmd : detail::gstate.commands) internalHandleCommand(cmd);
			detail::gstate.commands.clear();

			//poll events, which calls the internal callbacks in WICSystem, storing events for each window
			glfwPollEvents();

			//glfw does not generate all desireable events so they are generated here manually
			for (int cid = 0; cid < (int)Controller::MAX; cid++) {
				if (!detail::gstate.input_controller[cid].connected) continue;

				//buttons
				int count;
				const unsigned char* buttons = glfwGetJoystickButtons(cid, &count);
				if (count == 0) continue;
				for (int i = 0; i < count; i++) {
					//state change on button?
					bool pressed = (buttons[i] == GLFW_PRESS) ? true : false;
					if (detail::gstate.input_controller[cid].buttons[i] != pressed) {
						//create press/release event
						Event e;
						if (pressed) {
							e.type = Event::Type::ControllerButtonPress;
							e.ControllerButtonPress.id = cid;
							e.ControllerButtonPress.button = (ControllerButton)i;
						}
						else {
							e.type = Event::Type::ControllerButtonRelease;
							e.ControllerButtonRelease.id = cid;
							e.ControllerButtonRelease.button = (ControllerButton)i;
						}

						//queue it for all windows
						for (auto& window : detail::gstate.windowstates) {
							window.second.events.push_back(e);
						}
					}
				}

				//axes
				const float* axes = glfwGetJoystickAxes(cid, &count);
				if (count == 0) continue;
				bool axes_changed = false;
				for (int i = 0; i < count; i++) if (std::abs(axes[i] - detail::gstate.input_controller[cid].axes[i])>0.01f) axes_changed = true;
				if (axes_changed) {
					//create event
					Event e;
					e.type = Event::Type::ControllerAxisMove;
					e.ControllerAxisMove.id = cid;
					for (int i = 0; i < count; i++) e.ControllerAxisMove.axes[i] = axes[i];
					for (int i = count; i < (int)JOYSTICK_MAX_AXES; i++) e.ControllerAxisMove.axes[i] = 0.0f; //padd till max axes
					
					//queue it for all windows
					for (auto& window : detail::gstate.windowstates) {
						window.second.events.push_back(e);
					}
				}
			}

			if (detail::gstate.multithreaded) detail::gstate.mutex.unlock();
		}

		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//vulkan
#ifndef LAP_WIC_SUPPRESS_VULKAN
		bool WICSystem::vulkanIsSupported() {
			return glfwVulkanSupported() == GLFW_TRUE;
		}
		const char** WICSystem::vulkanGetRequiredInstanceExtensions(uint32_t* count) {
			return glfwGetRequiredInstanceExtensions(count);
		}
		void(*WICSystem::vulkanGetInstanceProcAddress(VkInstance instance, const char* procname))() {
			return glfwGetInstanceProcAddress(instance, procname);
		}
		bool WICSystem::vulkanGetPhysicalDevicePresentationSupport(VkInstance instance, VkPhysicalDevice device, uint32_t queuefamily) {
			return glfwGetPhysicalDevicePresentationSupport(instance, device, queuefamily) == GLFW_TRUE;
		}
		VkResult WICSystem::vulkanCreateWindowSurface(VkInstance instance, Window* window, const VkAllocationCallbacks* allocator, VkSurfaceKHR* surface) {
			assert(window);
			if (detail::gstate.multithreaded) {
				std::lock_guard<std::mutex>(detail::gstate.mutex);
				return glfwCreateWindowSurface(instance, (GLFWwindow*)window->handle_window, allocator, surface);
			}
			return glfwCreateWindowSurface(instance, (GLFWwindow*)window->handle_window, allocator, surface);
		}
#endif





		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		//-------------------------------------------------------------------------------------------------------------------------------------------------
		Window::Window(const WindowProperties& in_window, const FramebufferProperties& in_framebuffer, const ContextProperties& in_context, const InputProperties& in_input, const Monitor* in_monitor, const Window* in_shared_window)
		{
			//reality check
			assert(detail::gstate.initialized);

			//copy
			window_properties = in_window;
			framebuffer_properties = in_framebuffer;
			context_properties = in_context;
#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS
			context_properties.detail = (void*) new GLFunctionPointers();
#endif
			input_properties = in_input;
			monitor = in_monitor;

			//save thread id
			context_properties.thread_binded = std::this_thread::get_id();

			//create blocking command and wait after it, constraints are also applied and warnings issued on command execution
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::CREATE);
			command.create.cp = &context_properties;
			command.create.fp = &framebuffer_properties;
			command.create.ip = &input_properties;
			command.create.wp = &window_properties;
			command.create.monitor = in_monitor;
			if (in_monitor) command.create.handle_monitor = (GLFWmonitor*) in_monitor->handle;
			if(in_shared_window) command.create.handle_shared_window = (GLFWwindow*)in_shared_window->handle_window;
			else command.create.handle_shared_window = nullptr;
			detail::internalCommand(&command); //(automatically wait for it to finish)

			//save command results
			monitor = command.create.monitor;
			handle_monitor = command.create.handle_monitor;
			handle_window = command.create.handle_window;
			handle_window_shared = command.create.handle_shared_window;
			
			//bind opengl context (if applicable)
			if (context_properties.api == ContextProperties::ApiType::OPENGL) {
				glfwMakeContextCurrent((GLFWwindow*)handle_window);
#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS
				reinterpret_cast<GLFunctionPointers*>(context_properties.detail)->updateCurrent();
#endif
				thread_local_context = &context_properties;
			}

			//state
			window_properties.opened = true;
			events.reserve(EVENT_BUFFER_START_SIZE);
			event_current = 0;

			//callbacks
			callback_controller_connect = [](unsigned int, uint64_t) {};
			callback_controller_disconnect = [](unsigned int, uint64_t) {};
			callback_controller_button_press = [](unsigned int, ControllerButton, uint64_t) {};
			callback_controller_button_release = [](unsigned int, ControllerButton, uint64_t) {};
			callback_controller_axes_move = [](unsigned int, float(&)[6], uint64_t) {};
			callback_monitor_connect = [](const Monitor&, uint64_t) {};
			callback_monitor_disconnect = [](const Monitor&, uint64_t) {};
			callback_window_filesdrop = [](const Window&, const std::vector<std::string>&, uint64_t) {};
			callback_window_close = [](const Window&, uint64_t) {};
			callback_window_move = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_window_resize = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_window_minimize = [](const Window&, uint64_t) {};
			callback_window_maximize = [](const Window&, uint64_t) {};
			callback_window_restore = [](const Window&, uint64_t) {};
			callback_window_focus_gain = [](const Window&, uint64_t) {};
			callback_window_focus_lose = [](const Window&, uint64_t) {};
			callback_window_refresh = [](const Window&, uint64_t) {};
			callback_framebuffer_resize = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_key_press = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_key_release = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_key_repeat = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_unicode_char = [](const Window&, unsigned int, const InputState&, uint64_t) {};
			callback_unicode_char_mods = [](const Window&, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_press = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_release = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_drag = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_move = [](const Window&, unsigned int, unsigned int, const InputState&, uint64_t) {};
			callback_mouse_enter = [](const Window&, uint64_t) {};
			callback_mouse_leave = [](const Window&, uint64_t) {};
			callback_mouse_scroll = [](const Window&, float, float, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
		}
		Window::~Window()
		{
			// if the window was not closed by the manager (through the close command generated by clicking on x button)
			if (window_properties.opened) {
				//create blocking command
				detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::DESTROY);
				command.destroy.handle_window = (GLFWwindow*)handle_window;
				detail::internalCommand(&command); //(automatically wait for it to finish)
				//perform cleanup on the object
				internalCleanup();
			}
			//otherwise it was already destroyed
		}
		void Window::internalCleanup() {
			//While the window is already destroyed THIS object lives and consistency has to be enforced.
			//window properties
			window_properties.auto_restore_mode_on_minimize = false;
			window_properties.decorated = false;
			window_properties.floating = false;
			window_properties.focused = false;
			window_properties.fullscreen = false;
			window_properties.height = 0;
			window_properties.icon_pixels = nullptr;
			window_properties.maximized = false;
			window_properties.minimized = false;
			window_properties.opened = false;
			window_properties.position_x = 0;
			window_properties.position_y = 0;
			window_properties.refresh_rate = 0;
			window_properties.resizable = false;
			window_properties.title = "";
			window_properties.visible = false;
			window_properties.width = 0;
			//framebuffer properties
			framebuffer_properties.accum_alpha_bits = 0;
			framebuffer_properties.accum_blue_bits = 0;
			framebuffer_properties.accum_green_bits = 0;
			framebuffer_properties.accum_red_bits = 0;
			framebuffer_properties.alpha_bits = 0;
			framebuffer_properties.aux_buffers = 0;
			framebuffer_properties.blue_bits = 0;
			framebuffer_properties.depth_bits = 0;
			framebuffer_properties.doublebuffer = false;
			framebuffer_properties.green_bits = 0;
			framebuffer_properties.height = 0;
			framebuffer_properties.red_bits = 0;
			framebuffer_properties.samples_per_pixel = 0;
			framebuffer_properties.srgb = 0;
			framebuffer_properties.stencil_bits = 0;
			framebuffer_properties.stereo = false;
			framebuffer_properties.width = 0;
			//context properties
			context_properties.api = ContextProperties::ApiType::NONE;
			context_properties.debug_context = false;
			context_properties.driver_glsl_version = "";
			context_properties.driver_renderer = "";
			context_properties.driver_vendor = "";
			context_properties.driver_version = "";
			context_properties.profile_core = false;
			context_properties.release_behavior_flush = false;			
			context_properties.robustness = ContextProperties::RobustnessType::NONE;
			context_properties.thread_binded = std::thread::id();
			context_properties.version_major = 0;
			context_properties.version_minor = 0;
			context_properties.swap_interval = 0;
			//clean up function pointers
#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS
			delete reinterpret_cast<GLFunctionPointers*>(context_properties.detail);
#endif
			//input properties
			input_properties.cursor_enabled = false;
			input_properties.cursor_type = InputProperties::CursorType::ARROW;
			input_properties.cursor_visible = false;
			input_properties.sticky_keys = false;
			input_properties.sticky_mouse = false;
			//events
			events.clear();
			event_current = 0;
			//callbacks
			callback_controller_connect = [](unsigned int, uint64_t) {};
			callback_controller_disconnect = [](unsigned int, uint64_t) {};
			callback_controller_button_press = [](unsigned int, ControllerButton, uint64_t) {};
			callback_controller_button_release = [](unsigned int, ControllerButton, uint64_t) {};
			callback_controller_axes_move = [](unsigned int, float(&)[6], uint64_t) {};
			callback_monitor_connect = [](const Monitor&, uint64_t) {};
			callback_monitor_disconnect = [](const Monitor&, uint64_t) {};
			callback_window_filesdrop = [](const Window&, const std::vector<std::string>&, uint64_t) {};
			callback_window_close = [](const Window&, uint64_t) {};
			callback_window_move = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_window_resize = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_window_minimize = [](const Window&, uint64_t) {};
			callback_window_maximize = [](const Window&, uint64_t) {};
			callback_window_restore = [](const Window&, uint64_t) {};
			callback_window_focus_gain = [](const Window&, uint64_t) {};
			callback_window_focus_lose = [](const Window&, uint64_t) {};
			callback_window_refresh = [](const Window&, uint64_t) {};
			callback_framebuffer_resize = [](const Window&, unsigned int, unsigned int, uint64_t) {};
			callback_key_press = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_key_release = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_key_repeat = [](const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_unicode_char = [](const Window&, unsigned int, const InputState&, uint64_t) {};
			callback_unicode_char_mods = [](const Window&, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_press = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_release = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_drag = [](const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
			callback_mouse_move = [](const Window&, unsigned int, unsigned int, const InputState&, uint64_t) {};
			callback_mouse_enter = [](const Window&, uint64_t) {};
			callback_mouse_leave = [](const Window&, uint64_t) {};
			callback_mouse_scroll = [](const Window&, float, float, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t) {};
		}


		bool Window::isOpened() const{
			return window_properties.opened;
		}
		const WindowProperties& Window::getWindowProperties() const{
			return window_properties;
		}
		const FramebufferProperties& Window::getFramebufferProperties() const {
			return framebuffer_properties;
		}
		const ContextProperties& Window::getContextProperties() const{
			return context_properties;
		}
		const InputProperties& Window::getInputProperties() const{
			return input_properties;
		}
		const InputState& Window::getInputState() const{
			return input_state;
		}
		const InputRemapper& Window::getInputRemapper() const{
			return input_remapper;
		}
		InputRemapper& Window::getInputRemapper() {
			return input_remapper;
		}
		const Monitor* Window::getMonitor() const {
			return monitor;
		}



		void Window::bindContext() {
			assert(window_properties.opened && context_properties.api == ContextProperties::ApiType::OPENGL);
			glfwMakeContextCurrent((GLFWwindow*)handle_window);

			//optimization: update context only if changing the local thread context.
			if (thread_local_context != &context_properties) {
				//update the current context function pointers
#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS
				reinterpret_cast<GLFunctionPointers*>(context_properties.detail)->updateCurrent();
#endif
				//update state
				context_properties.thread_binded = std::this_thread::get_id();
				thread_local_context = &context_properties;
			}
		}
		void Window::unbindContext() {
			assert(window_properties.opened && context_properties.api == ContextProperties::ApiType::OPENGL);
			glfwMakeContextCurrent(nullptr);
			context_properties.thread_binded = std::thread::id();
			thread_local_context = nullptr;

		}
		void Window::setSwapInterval(int interval) {
			//still can be called if window1.bind then window2.bind 
			assert(window_properties.opened && context_properties.api == ContextProperties::ApiType::OPENGL && context_properties.thread_binded == std::this_thread::get_id());
			glfwSwapInterval(interval);
		}
		void Window::swapBuffers() {
			//still can be called if window1.bind then window2.bind 
			assert(window_properties.opened && context_properties.api == ContextProperties::ApiType::OPENGL && framebuffer_properties.doublebuffer && context_properties.thread_binded == std::this_thread::get_id());
			glfwSwapBuffers((GLFWwindow*)handle_window);
		}


		void Window::processEvents() {
			//if window is closed return directly
			if (!window_properties.opened) return;

			//get events
			detail::gstate.mutex.lock();
			//swap window events with events collected for this window
			std::swap(detail::gstate.windowstates[handle_window].events, events);
			detail::gstate.mutex.unlock();

			//remap
			input_remapper.internalRemap(events);
			
			//callbacks for all events, in order
			for (auto& e : events) {
				switch (e.type) {
					case Event::Type::ControllerConnect:
						input_state.controller[e.ControllerConnect.id].connected = true;
						callback_controller_connect(e.ControllerConnect.id, e.timestamp);
						break;
					case Event::Type::ControllerDisconnect:
						input_state.controller[e.ControllerDisconnect.id].connected = false;
						callback_controller_disconnect(e.ControllerDisconnect.id, e.timestamp);
						break;
					case Event::Type::ControllerButtonPress:
						input_state.controller[e.ControllerButtonPress.id].buttons[(int)e.ControllerButtonPress.button] = true;
						callback_controller_button_press(e.ControllerButtonPress.id, e.ControllerButtonPress.button, e.timestamp);
						break;
					case Event::Type::ControllerButtonRelease:
						input_state.controller[e.ControllerButtonRelease.id].buttons[(int)e.ControllerButtonRelease.button] = false;
						callback_controller_button_release(e.ControllerButtonRelease.id, e.ControllerButtonRelease.button, e.timestamp);
						break;
					case Event::Type::ControllerAxisMove:
						for (int i = 0; i < (int)JOYSTICK_MAX_AXES; i++) input_state.controller[e.ControllerAxisMove.id].axes[i] = e.ControllerAxisMove.axes[i];
						callback_controller_axes_move(e.ControllerAxisMove.id, e.ControllerAxisMove.axes, e.timestamp);
						break;
					case Event::Type::FilesDrop:
						callback_window_filesdrop((*this), (*e.FilesDrop.paths), e.timestamp);
						break;
					case Event::Type::FramebufferResize:
						framebuffer_properties.width = e.FramebufferResize.width;
						framebuffer_properties.height = e.FramebufferResize.height;
						callback_framebuffer_resize((*this), e.FramebufferResize.width, e.FramebufferResize.height, e.timestamp);
						break;
					case Event::Type::KeyPress:
						if((int)e.KeyPress.key >=0) input_state.keys[(int)e.KeyPress.key] = true;	//unknown keys have no state
						if (e.KeyPress.key == Key::LEFT_ALT || e.KeyPress.key == Key::RIGHT_ALT) input_state.mod_alt = true;
						if (e.KeyPress.key == Key::LEFT_CONTROL || e.KeyPress.key == Key::RIGHT_CONTROL) input_state.mod_control = true;
						if (e.KeyPress.key == Key::LEFT_SHIFT || e.KeyPress.key == Key::RIGHT_SHIFT) input_state.mod_shift = true;
						if (e.KeyPress.key == Key::LEFT_SUPER || e.KeyPress.key == Key::RIGHT_SUPER) input_state.mod_system = true;
						callback_key_press((*this), e.KeyPress.key, e.KeyPress.alt, e.KeyPress.control, e.KeyPress.shift, e.KeyPress.system, input_state, e.timestamp);
						//write the timestamp AFTER the callback, in order to give the callback caller access to the previous timestamp (correct from a logical standpoint, otherwise it would always be NOW)
						if ((int)e.KeyPress.key >= 0)input_state.keys_timestamp[(int)e.KeyPress.key] = e.timestamp;	//unknown keys have no state
						break;
					case Event::Type::KeyRelease:
						if ((int)e.KeyRelease.key >= 0) input_state.keys[(int)e.KeyRelease.key] = false;	//unknown keys have no state
						callback_key_release((*this), e.KeyRelease.key, e.KeyRelease.alt, e.KeyRelease.control, e.KeyRelease.shift, e.KeyRelease.system, input_state, e.timestamp);
						break;
					case Event::Type::KeyRepeat:
						if ((int)e.KeyRepeat.key >= 0) input_state.keys[(int)e.KeyRepeat.key] = true;	//unknown keys have no state
						if (e.KeyRepeat.key == Key::LEFT_ALT || e.KeyRepeat.key == Key::RIGHT_ALT) input_state.mod_alt = true;
						if (e.KeyRepeat.key == Key::LEFT_CONTROL || e.KeyRepeat.key == Key::RIGHT_CONTROL) input_state.mod_control = true;
						if (e.KeyRepeat.key == Key::LEFT_SHIFT || e.KeyRepeat.key == Key::RIGHT_SHIFT) input_state.mod_shift = true;
						if (e.KeyRepeat.key == Key::LEFT_SUPER || e.KeyRepeat.key == Key::RIGHT_SUPER) input_state.mod_system = true;
						callback_key_repeat((*this), e.KeyRepeat.key, e.KeyRepeat.alt, e.KeyRepeat.control, e.KeyRepeat.shift, e.KeyRepeat.system, input_state, e.timestamp);
						//write the timestamp AFTER the callback, in order to give the callback caller access to the previous timestamp (correct from a logical standpoint, otherwise it would always be NOW)
						if ((int)e.KeyRepeat.key >= 0) input_state.keys_timestamp[(int)e.KeyRepeat.key] = e.timestamp;	//unknown keys have no state
						break;
					case Event::Type::MonitorConnect:
						//already handled by internal callback
						callback_monitor_connect((*e.MonitorConnect.monitor), e.timestamp);
						break;
					case Event::Type::MonitorDisconnect:
						//already handled by internal callback
						callback_monitor_disconnect((*e.MonitorDisconnect.monitor), e.timestamp);
						break;
					case Event::Type::MousePress:
						input_state.mouse.buttons[(int)e.MousePress.button] = true;
						callback_mouse_press((*this), e.MousePress.button, e.MousePress.position_x, e.MousePress.position_y, e.MousePress.alt, e.MousePress.control, e.MousePress.shift, e.MousePress.system, input_state, e.timestamp);
						//write the timestamp AFTER the callback, in order to give the callback caller access to the previous timestamp (correct from a logical standpoint, otherwise it would always be NOW)
						input_state.mouse.buttons_timestamp[(int)e.MousePress.button] = e.timestamp;
						break;
					case Event::Type::MouseRelease:
						input_state.mouse.buttons[(int)e.MouseRelease.button] = false;
						callback_mouse_release((*this), e.MouseRelease.button, e.MouseRelease.position_x, e.MouseRelease.position_y, e.MouseRelease.alt, e.MouseRelease.control, e.MouseRelease.shift, e.MouseRelease.system, input_state, e.timestamp);
						break;
					case Event::Type::MouseDrag:
						input_state.mouse.buttons[(int)e.MouseDrag.button] = true;
						input_state.mouse.prev_position_x = input_state.mouse.position_x;
						input_state.mouse.prev_position_y = input_state.mouse.position_y;
						input_state.mouse.position_x = e.MouseDrag.position_x;
						input_state.mouse.position_y = e.MouseDrag.position_y;
						callback_mouse_drag((*this), e.MouseDrag.button, e.MouseDrag.position_x, e.MouseDrag.position_y, e.MouseDrag.alt, e.MouseDrag.control, e.MouseDrag.shift, e.MouseDrag.system, input_state, e.timestamp);
						//write the timestamp AFTER the callback, in order to give the callback caller access to the previous timestamp (correct from a logical standpoint, otherwise it would always be NOW)
						input_state.mouse.buttons_timestamp[(int)e.MouseDrag.button] = e.timestamp;
						break;
					case Event::Type::MouseEnter:
						callback_mouse_enter((*this), e.timestamp);
						break;
					case Event::Type::MouseLeave:
						callback_mouse_leave((*this), e.timestamp);
						break;
					case Event::Type::MouseMove:
						input_state.mouse.prev_position_x = input_state.mouse.position_x;
						input_state.mouse.prev_position_y = input_state.mouse.position_y;
						input_state.mouse.position_x = e.MouseMove.position_x;
						input_state.mouse.position_y = e.MouseMove.position_y;
						callback_mouse_move((*this), e.MouseMove.position_x, e.MouseMove.position_y, input_state, e.timestamp);
						break;
					case Event::Type::MouseScroll:
						input_state.mouse.last_scroll_x = e.MouseScroll.scroll_x;
						input_state.mouse.last_scroll_y = e.MouseScroll.scroll_y;
						callback_mouse_scroll((*this), e.MouseScroll.scroll_x, e.MouseScroll.scroll_y, e.MouseScroll.position_x, e.MouseScroll.position_y, e.MouseScroll.alt, e.MouseScroll.control, e.MouseScroll.shift, e.MouseScroll.system, input_state, e.timestamp);
						break;
					case Event::Type::UnicodeChar:
						//no action here
						callback_unicode_char((*this), e.UnicodeChar.codepoint, input_state, e.timestamp);
						break;
					case Event::Type::UnicodeCharMods:
						//no action here
						callback_unicode_char_mods((*this), e.UnicodeCharMods.codepoint, e.UnicodeCharMods.alt, e.UnicodeCharMods.control, e.UnicodeCharMods.shift, e.UnicodeCharMods.system, input_state, e.timestamp);
						break;
					case Event::Type::WindowClose:
						{
						//callback just before closing (this window is still opened, thus nothing is modified yet)
						callback_window_close((*this), e.timestamp);
						//now that the callback has been handled create a blocking command to destroy the glfw related window
						detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::DESTROY);
						command.destroy.handle_window = (GLFWwindow*)handle_window;
						detail::internalCommand(&command); //(automatically wait for it to finish)
						//perform cleanup on this object, while living the object alive
						internalCleanup();
						//window is closed, nothing else to do.
						return;
						}
						break;
					case Event::Type::WindowFocusGain:
						window_properties.focused = true;
						callback_window_focus_gain((*this), e.timestamp);
						break;
					case Event::Type::WindowFocusLose:
						window_properties.focused = false;
						callback_window_focus_lose((*this), e.timestamp);
						break;
					case Event::Type::WindowMinimize:
						window_properties.minimized = true;
						window_properties.maximized = false;
						callback_window_minimize((*this), e.timestamp);
						break;
					case Event::Type::WindowMaximize:
						window_properties.minimized = false;
						window_properties.maximized = true;
						callback_window_maximize((*this), e.timestamp);
						break;
					case Event::Type::WindowMove:
						window_properties.position_x = e.WindowMove.position_x;
						window_properties.position_y = e.WindowMove.position_y;
						callback_window_move((*this), e.WindowMove.position_x, e.WindowMove.position_y, e.timestamp);
						break;
					case Event::Type::WindowRefresh:
						callback_window_refresh((*this), e.timestamp);
						break;
					case Event::Type::WindowResize:
						window_properties.width = e.WindowResize.width;
						window_properties.height = e.WindowResize.height;
						callback_window_resize((*this), e.WindowResize.width, e.WindowResize.height, e.timestamp);
						break;
					case Event::Type::WindowRestore:
						window_properties.minimized = false;
						window_properties.maximized = false;
						callback_window_restore((*this), e.timestamp);
						break;
				}
			}
			//treated all events
			events.clear();
		}

		const Event* Window::processEvent() {
			//if window is closed return nullptr directly
			if (!window_properties.opened) return nullptr;

			//get events
			if (event_current == 0) {
				detail::gstate.mutex.lock();
				//swap window events with events collected for this window
				std::swap(detail::gstate.windowstates[handle_window].events, events);
				detail::gstate.mutex.unlock();

				//remap
				input_remapper.internalRemap(events);
			}

			//some events need to partially or totally processed after they were processed by the caller.
			//e.g. write the timestamp AFTER the event was processed, in order to give the caller access to the previous timestamp (correct from a logical standpoint, otherwise it would always be NOW)
			//e.g. close
			if (event_current > 0) {
				Event& eprev = events[event_current - 1];
				if (eprev.type == Event::Type::KeyPress) if ((int)eprev.KeyPress.key >= 0) input_state.keys_timestamp[(int)eprev.KeyPress.key] = eprev.timestamp;	//unknown keys have no state
				if (eprev.type == Event::Type::KeyRepeat) if ((int)eprev.KeyRepeat.key >= 0) input_state.keys_timestamp[(int)eprev.KeyRepeat.key] = eprev.timestamp;	//unknown keys have no state
				if (eprev.type == Event::Type::MousePress) input_state.mouse.buttons_timestamp[(int)eprev.MousePress.button] = eprev.timestamp;
				if (eprev.type == Event::Type::MouseDrag) input_state.mouse.buttons_timestamp[(int)eprev.MouseDrag.button] = eprev.timestamp;
				if (eprev.type == Event::Type::WindowClose) {
					//the event was handled, time to close the system window
					detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::DESTROY);
					command.destroy.handle_window = (GLFWwindow*)handle_window;
					detail::internalCommand(&command); //(automatically wait for it to finish)
					//perform cleanup on this object, while living the object alive
					internalCleanup();
					//window is closed, nothing else to do.
					return nullptr;
				}
			}

			//if the last event was reached then return nullptr to signal end of events but get the events for the next frame
			if (event_current == events.size()) {
				event_current = 0;
				events.clear();
				return nullptr;
			}

			//grab event
			Event* eptr = &events[event_current++];
			Event& e = (*eptr);

			//maintain state
			switch (e.type) {
			case Event::Type::ControllerConnect:
				input_state.controller[e.ControllerConnect.id].connected = true;
				break;
			case Event::Type::ControllerDisconnect:
				input_state.controller[e.ControllerDisconnect.id].connected = false;
				break;
			case Event::Type::ControllerButtonPress:
				input_state.controller[e.ControllerButtonPress.id].buttons[(int)e.ControllerButtonPress.button] = true;
				break;
			case Event::Type::ControllerButtonRelease:
				input_state.controller[e.ControllerButtonRelease.id].buttons[(int)e.ControllerButtonRelease.button] = false;
				break;
			case Event::Type::ControllerAxisMove:
				for (int i = 0; i < (int)JOYSTICK_MAX_AXES; i++) input_state.controller[e.ControllerAxisMove.id].axes[i] = e.ControllerAxisMove.axes[i];
				break;
			case Event::Type::FilesDrop:
				//no action here
				break;
			case Event::Type::FramebufferResize:
				framebuffer_properties.width = e.FramebufferResize.width;
				framebuffer_properties.height = e.FramebufferResize.height;
				break;
			case Event::Type::KeyPress:
				if ((int)e.KeyPress.key >= 0) input_state.keys[(int)e.KeyPress.key] = true;	//unknown keys have no state
				if (e.KeyPress.key == Key::LEFT_ALT || e.KeyPress.key == Key::RIGHT_ALT) input_state.mod_alt = true;
				if (e.KeyPress.key == Key::LEFT_CONTROL || e.KeyPress.key == Key::RIGHT_CONTROL) input_state.mod_control = true;
				if (e.KeyPress.key == Key::LEFT_SHIFT || e.KeyPress.key == Key::RIGHT_SHIFT) input_state.mod_shift = true;
				if (e.KeyPress.key == Key::LEFT_SUPER || e.KeyPress.key == Key::RIGHT_SUPER) input_state.mod_system = true;
				//timestamp will be written on next processEvent() call in order to give caller access to the previous timestamp
				break;
			case Event::Type::KeyRelease:
				if ((int)e.KeyRelease.key >= 0) input_state.keys[(int)e.KeyRelease.key] = false;	//unknown keys have no state
				break;
			case Event::Type::KeyRepeat:
				if ((int)e.KeyRepeat.key >= 0) input_state.keys[(int)e.KeyRepeat.key] = true;	//unknown keys have no state
				if (e.KeyRepeat.key == Key::LEFT_ALT || e.KeyRepeat.key == Key::RIGHT_ALT) input_state.mod_alt = true;
				if (e.KeyRepeat.key == Key::LEFT_CONTROL || e.KeyRepeat.key == Key::RIGHT_CONTROL) input_state.mod_control = true;
				if (e.KeyRepeat.key == Key::LEFT_SHIFT || e.KeyRepeat.key == Key::RIGHT_SHIFT) input_state.mod_shift = true;
				if (e.KeyRepeat.key == Key::LEFT_SUPER || e.KeyRepeat.key == Key::RIGHT_SUPER) input_state.mod_system = true;
				//timestamp will be written on next processEvent() call in order to give caller access to the previous timestamp
				break;
			case Event::Type::MonitorConnect:
				//already handled by internal callback
				break;
			case Event::Type::MonitorDisconnect:
				//already handled by internal callback
				break;
			case Event::Type::MousePress:
				input_state.mouse.buttons[(int)e.MousePress.button] = true;
				//timestamp will be written on next processEvent() call in order to give caller access to the previous timestamp
				break;
			case Event::Type::MouseRelease:
				input_state.mouse.buttons[(int)e.MouseRelease.button] = false;
				break;
			case Event::Type::MouseDrag:
				input_state.mouse.buttons[(int)e.MouseDrag.button] = true;
				input_state.mouse.prev_position_x = input_state.mouse.position_x;
				input_state.mouse.prev_position_y = input_state.mouse.position_y;
				input_state.mouse.position_x = e.MouseDrag.position_x;
				input_state.mouse.position_y = e.MouseDrag.position_y;
				//timestamp will be written on next processEvent() call in order to give caller access to the previous timestamp				break;
				break;
			case Event::Type::MouseEnter:
				//no action here
				break;
			case Event::Type::MouseLeave:
				//no action here
				break;
			case Event::Type::MouseMove:
				input_state.mouse.prev_position_x = input_state.mouse.position_x;
				input_state.mouse.prev_position_y = input_state.mouse.position_y;
				input_state.mouse.position_x = e.MouseMove.position_x;
				input_state.mouse.position_y = e.MouseMove.position_y;
				break;
			case Event::Type::MouseScroll:
				input_state.mouse.last_scroll_x = e.MouseScroll.scroll_x;
				input_state.mouse.last_scroll_y = e.MouseScroll.scroll_y;
				break;
			case Event::Type::UnicodeChar:
				//no action here
				break;
			case Event::Type::UnicodeCharMods:
				//no action here
				break;
			case Event::Type::WindowClose:
				//will be processed internally on next processEvent	
				break;
			case Event::Type::WindowFocusGain:
				window_properties.focused = true;
				break;
			case Event::Type::WindowFocusLose:
				window_properties.focused = false;
				break;
			case Event::Type::WindowMinimize:
				window_properties.minimized = true;
				window_properties.maximized = false;
				break;
			case Event::Type::WindowMaximize:
				window_properties.minimized = false;
				window_properties.maximized = true;
				break;
			case Event::Type::WindowMove:
				window_properties.position_x = e.WindowMove.position_x;
				window_properties.position_y = e.WindowMove.position_y;
				break;
			case Event::Type::WindowRefresh:
				callback_window_refresh((*this), e.timestamp);
				break;
			case Event::Type::WindowResize:
				window_properties.width = e.WindowResize.width;
				window_properties.height = e.WindowResize.height;
				break;
			case Event::Type::WindowRestore:
				window_properties.minimized = false;
				window_properties.maximized = false;
				break;
			}

			return eptr;
		}





		void Window::setFullscreen(const MonitorMode* mode, const Monitor* inmonitor) {
			//reality check
			assert(window_properties.opened);

			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::FULLSCREEN_MONITOR);
			command.fullscreenmonitor.handle_window = (GLFWwindow*)handle_window;
			if(mode) command.fullscreenmonitor.mode = mode;
			else command.fullscreenmonitor.mode = &monitor->mode_current;	//default to current mode if no mode is specified
			if (inmonitor) {
				monitor = inmonitor;
				handle_monitor = (void*)monitor->handle;
			}
			command.fullscreenmonitor.handle_monitor = (GLFWmonitor*)handle_monitor;
			
			command.fullscreenmonitor.wp = &window_properties;
			command.fullscreenmonitor.fp = &framebuffer_properties;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::setWindowed(unsigned int width, unsigned int height, unsigned int position_x, unsigned int position_y, const Monitor* inmonitor) {
			//reality check
			assert(window_properties.opened);

			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::WINDOWED_MONITOR);
			command.windowedmonitor.handle_window = (GLFWwindow*)handle_window;
			command.windowedmonitor.width = width;
			command.windowedmonitor.height = height;
			command.windowedmonitor.position_x = position_x;
			command.windowedmonitor.position_y = position_y;
			if (inmonitor) {
				monitor = inmonitor;
				handle_monitor = (void*)monitor->handle;
			}
			command.windowedmonitor.handle_monitor = (GLFWmonitor*)handle_monitor;
			command.windowedmonitor.wp = &window_properties;
			command.windowedmonitor.fp = &framebuffer_properties;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::setSize(unsigned int width, unsigned int height) {
			//reality check
			assert(window_properties.opened);

			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::RESIZE);
			command.resize.handle_window = (GLFWwindow*)handle_window;
			command.resize.wp = &window_properties;
			command.resize.fp = &framebuffer_properties;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::setPosition(unsigned int position_x, unsigned int position_y) {
			//reality check
			assert(window_properties.opened);

			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::MOVE);
			command.move.handle_window = (GLFWwindow*)handle_window;
			command.move.position_x = position_x;
			command.move.position_y = position_y;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::setTitle(const std::string& title) {
			//reality check
			assert(window_properties.opened);

			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::TITLE);
			command.title.handle_window = (GLFWwindow*)handle_window;
			command.title.text = title.c_str();
			detail::internalCommand(&command); //(automatically wait for it to finish)

			//apply internally
			window_properties.title = title;
		}
		void Window::setInputProperties(bool cursor_visible, bool cursor_enabled, InputProperties::CursorType& cursor_type, bool sticky_keys, bool sticky_mouse) {
			//reality check
			assert(window_properties.opened);
			
			input_properties.cursor_enabled = cursor_enabled;
			input_properties.cursor_type = cursor_type;
			input_properties.cursor_visible = cursor_visible;
			input_properties.sticky_keys = sticky_keys;
			input_properties.sticky_mouse = sticky_mouse;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::INPUT);
			command.input.handle_window = (GLFWwindow*)handle_window;
			command.input.ip = &input_properties;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::setInputRemapper(const InputRemapper& remapper) {
			input_remapper = remapper;
		}
		void Window::setClipboardString(const std::string& text) {
			//reality check
			assert(window_properties.opened);

			input_state.clipboard_text = text;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::CLIPBOARD);
			command.clipboard.handle_window = (GLFWwindow*)handle_window;
			command.clipboard.text = text.c_str();
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}



		void Window::minimize() {
			//reality check
			assert(window_properties.opened);

			window_properties.minimized = true;
			window_properties.maximized = false;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::MINIMIZE);
			command.minimize.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::maximize() {
			//reality check
			assert(window_properties.opened);

			window_properties.minimized = false;
			window_properties.maximized = true;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::MAXIMIZE);
			command.maximize.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::restore() {
			//reality check
			assert(window_properties.opened);

			window_properties.minimized = false;
			window_properties.maximized = false;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::RESTORE);
			command.restore.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::focus() {
			//reality check
			assert(window_properties.opened);

			window_properties.focused = true;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::FOCUS);
			command.focus.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::hide() {
			//reality check
			assert(window_properties.opened);

			window_properties.visible = false;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::HIDE);
			command.hide.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::show() {
			//reality check
			assert(window_properties.opened);

			window_properties.visible = true;
			//create blocking command
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::SHOW);
			command.show.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
		}
		void Window::close() {
			//reality check
			assert(window_properties.opened);

			//callback just before closing (this window is still opened, thus nothing is modified yet)
			auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - detail::gstate.timestamp_program).count();
			callback_window_close((*this), timestamp);
			//now that the callback has been handled create a blocking command to destroy the glfw related window
			detail::WindowBlockingCommand command(detail::WindowBlockingCommand::Type::DESTROY);
			command.destroy.handle_window = (GLFWwindow*)handle_window;
			detail::internalCommand(&command); //(automatically wait for it to finish)
			//perform cleanup on this object, while living the object alive
			internalCleanup();
			//window is closed, nothing else to do.
		}


	

		void Window::setCallbackFramebufferResize(const std::function<void(const Window&, unsigned int, unsigned int, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_framebuffer_resize = callback;
		}

		void Window::setCallbackMonitorConnect(const std::function<void(const Monitor&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_monitor_connect = callback;
		}

		void Window::setCallbackMonitorDisconnect(const std::function<void(const Monitor&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_monitor_disconnect = callback;
		}

		void Window::setCallbackControllerConnect(const std::function<void(unsigned int, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_controller_connect = callback;
		}

		void Window::setCallbackControllerDisconnect(const std::function<void(unsigned int, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_controller_disconnect = callback;
		}

		void Window::setCallbackControllerButtonPress(const std::function<void(unsigned int, ControllerButton, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_controller_button_press = callback;
		}

		void Window::setCallbackControllerButtonRelease(const std::function<void(unsigned int, ControllerButton, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_controller_button_release = callback;
		}

		void Window::setCallbackControllerAxisMove(const std::function<void(unsigned int, float (&)[6], uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_controller_axes_move = callback;
		}

		void Window::setCallbackWindowClose(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_close = callback;
		}

		void Window::setCallbackWindowMove(const std::function<void(const Window&, unsigned int, unsigned int, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_move = callback;
		}

		void Window::setCallbackWindowResize(const std::function<void(const Window&, unsigned int, unsigned int, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_resize = callback;
		}

		void Window::setCallbackWindowMinimize(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_minimize = callback;
		}

		void Window::setCallbackWindowMaximize(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_maximize = callback;
		}

		void Window::setCallbackWindowRestore(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_restore = callback;
		}

		void Window::setCallbackWindowFocusGain(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_focus_gain = callback;
		}

		void Window::setCallbackWindowFocusLose(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_focus_lose = callback;
		}

		void Window::setCallbackWindowRefresh(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_refresh = callback;
		}

		void Window::setCallbackFilesDrop(const std::function<void(const Window&, const std::vector<std::string>&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_window_filesdrop = callback;
		}

		void Window::setCallbackKeyPress(const std::function<void(const Window&, Key, bool, bool, bool, bool, const  InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_key_press = callback;
		}

		void Window::setCallbackKeyRelease(const std::function<void(const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_key_release = callback;
		}

		void Window::setCallbackKeyRepeat(const std::function<void(const Window&, Key, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_key_repeat = callback;
		}

		void Window::setCallbackUnicodeChar(const std::function<void(const Window&, unsigned int, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_unicode_char = callback;
		}

		void Window::setCallbackUnicodeCharMods(const std::function<void(const Window&, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_unicode_char_mods = callback;
		}

		void Window::setCallbackMouseMove(const std::function<void(const Window&, unsigned int, unsigned int, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_move = callback;
		}

		void Window::setCallbackMouseEnter(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_enter = callback;
		}

		void Window::setCallbackMouseLeave(const std::function<void(const Window&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_leave = callback;
		}

		void Window::setCallbackMousePress(const std::function<void(const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_press = callback;
		}

		void Window::setCallbackMouseRelease(const std::function<void(const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_release = callback;
		}

		void Window::setCallbackMouseDrag(const std::function<void(const Window&, MouseButton, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_drag = callback;
		}

		void Window::setCallbackMouseScroll(const std::function<void(const Window&, float, float, unsigned int, unsigned int, bool, bool, bool, bool, const InputState&, uint64_t)>& callback)
		{
			//reality check
			assert(window_properties.opened);
			//set callback
			callback_mouse_scroll = callback;
		}

		bool Window::isOpenGLExtensionSupported(const std::string& name) {
			int result = glfwExtensionSupported(name.c_str());
			return (result == GLFW_TRUE);
		}
		void(*Window::loadOpenGLExtensionFunction(const std::string& funcname))(void) {
			return glfwGetProcAddress(funcname.c_str());
		}
	}
}
