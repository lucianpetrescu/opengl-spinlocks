###########################################################################################################################################
# new to makefiles ? -> read https://sites.google.com/site/michaelsafyan/software-engineering/how-to-write-a-makefile  also read this http://aegis.sourceforge.net/auug97.pdf
# NOTE: 
#    this makefile will not find differences in inline headers (headers with implementation code). The headers need to be addedd as a dependency on the object build rules. 
# INLINE_HEADERS += $(wildcard *.h) $(wildcard *.hpp)
###########################################################################################################################################


###########################################################################################################################################
# 															PROJECT FILES
ifeq ($(OS),Windows_NT)
	OUTPUT_NAME := spinlocktest_win
else
	UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        OUTPUT_NAME := spinlocktest_nix
    endif
    ifeq ($(UNAME_S),Darwin)
        OUTPUT_NAME := spinlocktest_osx
    endif
endif

OUTPUT_DIR := bin
OUTPUT_NAME_DEBUG =  $(OUTPUT_NAME)_d.exe
OUTPUT_NAME_RELEASE = $(OUTPUT_NAME).exe
OUTPUT_DEBUG_DIR := makedebug
OUTPUT_RELEASE_DIR := makerelease
INLINE_HEADERS :=
SOURCES := spinlocktest.cpp lap_wic.cpp lap_gpu_timer.cpp
VPATH := dep
OBJS_DEBUG := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))
OBJS_RELEASE := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))


###########################################################################################################################################
# 													COMPILER AND LINKER FLAGS
#compiler automatically knows who CPP is and uses CXXFLAGS to create objs (note: CPPFLAGS is for C preprocessor.)
#release flags (deliverable) (all warnings, full optimization,  optimize for size, no debugging,  link time optimization, enable sse & sse2, enable sse for math
RELEASE_CPP_FLAGS=$(CXXFLAGS) -m64 -std=c++11 -Wall -O2 -Os -DNDEBUG -flto -msse -msse2 -mfpmath=sse -Wno-unused-variable -Wno-sign-compare
RELEASE_LD_FLAGS = $(LDFLAGS) -pthread -flto
#debug flags (debugging), export, variable shadowing (bad practice usually)
# debug linker can add --export-dynamic to export all symbols to the dynamic linker table (permits callstack inspection...), but this is only valid on ELF.
DEBUG_CPP_FLAGS=$(CXXFLAGS) -m64 -std=c++11 -Wall -Og -g -O0 -Wno-unused-variable -Wno-sign-compare
ifeq ($(OS),Windows_NT)
	#mingw and cygwin don't have rdynamic, also have to add gdi32, which is automatically added in Visual Studio through pragma comment.
	DEBUG_LD_FLAGS = $(LDFLAGS) -pthread -lgdi32
	RELEASE_LD_FLAGS += -lgdi32
else
	#native make
	DEBUG_LD_FLAGS = $(LDFLAGS) -pthread -rdynamic 
	#platform dependent
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		#link with OpenGL, dl, 
		DEBUG_LD_FLAGS += -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
		RELEASE_LD_FLAGS += -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	endif
	ifeq ($(UNAME_S),Darwin)
		#tell compiler that we are mixing objective c and cpp
		DEBUG_CPP_FLAGS += -x objective-c++ -std=c++11
		RELEASE_CPP_FLAGS += -x objective-c++ -std=c++11
		#link with the required frameworks
		DEBUG_LD_FLAGS += -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
		RELEASE_LD_FLAGS += -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	endif
endif







###########################################################################################################################################
#																BUILD TARGETS
#make might confuse "all" "clean" "distclean" etc targets for actual filenames, PHONY is used to prevent this confusion.
.PHONY: all pre-build-debug pre-build-release build-debug build-release debug release clean distclean help install

#the build process is the following
# 1. pre-build-(debug|release) is always called before compilation and it creates the folder structure required for building debug|release
# 2. build-(debug|release) builds the program for debug|release, and depends on pre-build-(debug|release)
# 3. debug|release is always called last (depends on build-(debug|release)),
all: debug release

pre-build-debug:
	@echo ------------------------------------ PREPARE     ------------------------------------
	mkdir -p $(OUTPUT_DIR)
	mkdir -p $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)
	@echo ------------------------------------ COMPILING ----------------------------------
pre-build-release:
	@echo ------------------------------------ PREPARE     ------------------------------------
	mkdir -p $(OUTPUT_DIR)
	mkdir -p $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)
	@echo ------------------------------------ COMPILING ----------------------------------
	
$(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)/%.o : %.cpp $(INLINE_HEADERS) pre-build-debug
	$(CXX) $(DEBUG_CPP_FLAGS) -c $< -o $@
	
$(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)/%.o : %.cpp $(INLINE_HEADERS) pre-build-release
	$(CXX) $(RELEASE_CPP_FLAGS) -c $< -o $@

build-debug: pre-build-debug $(OBJS_DEBUG)
	@echo ------------------------------------ LINKING ------------------------------------
	$(CXX) $(OBJS_DEBUG) $(DEBUG_LD_FLAGS) -o $(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG)
build-release: pre-build-release $(OBJS_RELEASE)
	@echo ------------------------------------ LINKING ------------------------------------
	$(CXX) $(OBJS_RELEASE) $(RELEASE_LD_FLAGS) -o $(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE)
debug: build-debug
	@echo ------------------------------------ FINISHED ------------------------------------
release: build-release
	@echo ------------------------------------ FINISHED ------------------------------------

clean:
	@echo ------------------------------------ CLEANING -----------------------------------
	rm -f $(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG)
	rm -f $(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE)
	rm -f $(OBJS_DEBUG)
	rm -f $(OBJS_RELEASE)
	
distclean: clean


###########################################################################################################################################
# 														HELP and INSTALL
help:
	@echo ---This makefile is used to create the test executable for lap_gl_debug_context_---
	@echo ---Since the library is source only it can be used by just including it in your build system---
	@echo use -make all- to build both debug and release executables
	@echo use -make debug- to build only the debug executable
	@echo use -make release- to build only the release executable
install:
	@echo nothing to install

dbg:
	@echo $(SOURCES)
	@echo $(OBJS_DEBUG)
	@echo $(CXX)
	@echo $(DEBUG_LD_FLAGS)
	@echo $(RELEASE_LD_FLAGS)
