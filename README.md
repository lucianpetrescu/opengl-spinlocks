# GPU SPINLOCK TESTER #

This is a spinlock performance tester for various contention cases. It is implemented with OpenGL 4.5. Useful for testing performance when developing GPU algorithms. The test uses a single spinlock, but it can easily be modified to work with more.

version: 1.00

author: Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )


### SCREENSHOTS ###

Examples of tests. The number of iterations, the number of GPU threads, the number of spinlock usages per GPU thread and the probability of a GPU thread to use the spinlock are all easily configurable. 
 ![results](img/output.png) 

###GPU Spinlocks###

Implemented in shader/spinlock.comp

	layout(std430, binding = 0) buffer BlockName0 {
		uint lockdata [];		// used by spinlock, needs a ssbo of size >=1 
								// bound on binding 0 of this compute shader
	};

	.....

	bool written = false;
	do {
		bool can_write = (atomicExchange(lockdata[0], 1) != 1);

		if (can_write) {
			//DO SYNCHRONIZED WORK HERE
			memoryBarrier();

			atomicExchange(lockdata[0], 0);
			written = true;
		}
	} while (!written);

inspired from:

[http://stackoverflow.com/questions/21538555/broken-glsl-spinlock-glsl-locks-compendium](http://stackoverflow.com/questions/21538555/broken-glsl-spinlock-glsl-locks-compendium)
[https://devtalk.nvidia.com/default/topic/526793/glsl-loop-39-break-39-instruction-not-executed/](https://devtalk.nvidia.com/default/topic/526793/glsl-loop-39-break-39-instruction-not-executed/)

###Properties###

- c++11, no exceptions
- OpenGL 4.5
- dependencies:
	-  lap_wic provides a (hidden) window and the needed OpenGL context.
	-  lap\_gpu_timer provides gpu timer queries
	- both can be freely obtained at [https://bitbucket.org/lucianpetrescu/public](https://bitbucket.org/lucianpetrescu/public)

###Compiling and linking###

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n
	

### DOCUMENTATION ###

no doxygen, just this file

### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.

###TODO LIST###

nothing


