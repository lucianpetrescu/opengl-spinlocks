﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "dep/lap_wic.hpp"
#include "dep/lap_gpu_timer.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <iomanip>

int main(int argc, char* argv[]) {

	//use LAP_WIC library to solve window+context+input
	lap::wic::WICSystem wicsystem(nullptr, false);	//generate a windowing system with no logging
	lap::wic::WindowProperties wp;	wp.visible = false;
	lap::wic::Window window = lap::wic::Window(wp, lap::wic::FramebufferProperties(), lap::wic::ContextProperties(), lap::wic::InputProperties());	//create a window with an OpenGL context

	//create a gpu timer (wtih LAP_GPU_Timer library)
	lap::gpu::GPUTimer gputimer(2);

	//init rand
	std::srand((unsigned int)reinterpret_cast<uintptr_t>(&wicsystem));

	//create test program
	GLuint glshader = glCreateShader(GL_COMPUTE_SHADER);
	std::ifstream file("../shader/spinlock.comp"); 
	std::stringstream ss; ss << file.rdbuf();
	std::string str = ss.str();
	const char* strc = str.c_str();
	const int length = (GLint)str.size();
	glShaderSource(glshader, 1, &strc, &length);
	glCompileShader(glshader);
	GLuint glprogram = glCreateProgram();
	glAttachShader(glprogram, glshader);
	glLinkProgram(glprogram);

	//simulates a per-frame spinlock using application, through a number of frames
	auto& workfunc = [&](	unsigned int iterations,	// number of sim iterations
							unsigned int threads,		// number of all gpu threads
							float usage_probability,	// probability that a gpu thread will use the shared resource
							unsigned int usage_count	// if the resource is to be used, how many times will it be used?
						) {
		//create a lock buffer
		GLuint lockbuffer, lockdata = 0;
		glCreateBuffers(1, &lockbuffer);
		glNamedBufferData(lockbuffer, sizeof(unsigned int), &lockdata, GL_STATIC_DRAW);		//initialize the lock with value 0

		//create an output buffer and resize it to match threads size
		GLuint outputbuffer;
		glCreateBuffers(1, &outputbuffer);
		glNamedBufferData(outputbuffer, sizeof(float) * threads, nullptr, GL_STATIC_DRAW);

		//locations
		GLint loc_threads = glGetUniformLocation(glprogram, "threads");
		GLint loc_randseed = glGetUniformLocation(glprogram, "randseed");
		GLint loc_usage_probability = glGetUniformLocation(glprogram, "usage_probability");
		GLint loc_usage_count = glGetUniformLocation(glprogram, "usage_count");

		double mintime = 99999, maxtime = 0, avgtime = 0;
		for (unsigned int i = 0; i < iterations; i++) {
			//set state
			glUseProgram(glprogram);
			glUniform1ui(loc_threads, threads);
			glUniform1f(loc_randseed, rand() / 1000.0f);
			glUniform1f(loc_usage_probability, usage_probability);
			glUniform1ui(loc_usage_count, usage_count);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, lockbuffer);		//lock buffer, size of 1
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, outputbuffer);		//data buffer, size of threads
			
			//perform measured work
			gputimer.insertQuery(0);
			glDispatchCompute(threads / 32 + 1, 1, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
			gputimer.insertQuery(1);

			//accumulate measurements
			auto time = gputimer.getTimeInMillisecondsAfterWaitingQueries(0, 1);
			mintime = std::min(time, mintime);
			maxtime = std::max(time, maxtime);
			avgtime = (time + i * avgtime) / (i + 1);
		}

		//output
		std::cout << std::fixed<< std::setprecision(2)<< "-------------------------------------\nOver " << iterations << " iterations, with " << threads << " threads, " << usage_count << " spinlock usages @ "<<usage_probability*100<<"% usage probability per thread :" << std::endl;
		std::cout <<"\tavgtime = " << avgtime << " ms\n\tmintime = " << mintime <<" ms\n\tmaxtime = " << maxtime <<" ms."<< std::endl;

		//cleanup
		glDeleteBuffers(1, &lockbuffer);
		glDeleteBuffers(1, &outputbuffer);
		glFinish();
	};

	//perform work, adapt this to the expected usage rate of your use case.
	workfunc(20, 128, 0.5f, 2);
	workfunc(50, 1000, 0.2f, 2);
	workfunc(50, 5000, 0.3f, 2);
	workfunc(50, 10000, 0.4f, 2);
	workfunc(50, 25000, 0.5f, 2);
	workfunc(100, 100000, 0.25f, 4);
	workfunc(100, 4400, 0.8f, 3);
	workfunc(100, 27000, 0.6f, 3);
	


	//cleanup the remaining OpenGL resources
	glUseProgram(0);
	glDeleteShader(glshader);
	glDeleteProgram(glprogram);


	//wait (some consoles close on program termination..)
	std::cout << "\n\nFinished, press any key .. " << std::endl;
	std::cin.get();

	return 0;
}